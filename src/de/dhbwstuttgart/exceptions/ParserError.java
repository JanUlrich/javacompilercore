package de.dhbwstuttgart.exceptions;

import org.antlr.v4.runtime.Token;

public class ParserError extends TypeinferenceException{

	public ParserError(Token offset){
		super("Fehler beim Parsen", offset);
	}
}
