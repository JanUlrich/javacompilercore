package de.dhbwstuttgart.exceptions;

public class DebugException extends RuntimeException {

	public DebugException(String message) {
		System.err.print(message);
	}
}
