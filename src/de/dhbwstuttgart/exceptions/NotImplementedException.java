package de.dhbwstuttgart.exceptions;

public class NotImplementedException extends RuntimeException {
	
	public NotImplementedException() {
	}

	public NotImplementedException(String string) {
		super(string);
	}
}
