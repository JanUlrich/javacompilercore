package de.dhbwstuttgart.exceptions;

import de.dhbwstuttgart.syntaxtree.SyntaxTreeNode;
import org.antlr.v4.runtime.Token;

/**
 * Eine RuntimeException, welche bei einem Fehler wÃ¤hrend des Typinferenzalgorithmus ausgelÃ¶st wird.
 * Dies wird zum Beispiel durch Programmierfehler in der Java-Eingabedatei ausgelÃ¶st.
 * @author Andreas Stadelmeier, a10023
 *
 */
//TODO: Diese Klasse muss von Exception erben
public class TypeinferenceException extends RuntimeException {
	
	/**
     * Das Offset im Quelltext bei dem das Problem aufgetaucht ist
     */
	private Token offset;
    private String message;
    
    public TypeinferenceException(String message, SyntaxTreeNode problemSource)
    {
        super(message);
        this.message=message;
        if(problemSource == null)throw new DebugException("TypinferenzException ohne Offset: "+this.message);
        this.offset=problemSource.getOffset();
    }
    
    public TypeinferenceException(String message, Token offset){
    	this.message=message;
    	this.offset = offset;
    }
    
    /**
     * 
     * @return Der Offset an dem im Quellcode der Fehler aufgetreten ist.
     */
    public Token getOffset(){
    	return offset;
    }
    
    public String getMessage(){
    	return this.message;
    }
    
}
