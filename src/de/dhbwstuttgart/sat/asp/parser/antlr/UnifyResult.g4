grammar UnifyResult;

answer : 'ANSWER' (resultSetRule '.')*;

resultSetRule :
      parameter
    | equals
    | smaller
    | typeVar
    | type
    | unifier
    | otherRule
    ;

parameterList : '(' value (',' value)*  ')';
value : NAME
        | resultSetRule ;

parameter : PARAMLIST_NAME parameterList;
equals : EQUALS_NAME parameterList;
unifier : UNIFIER_NAME parameterList;
smaller : SMALLER_NAME parameterList;
typeVar : TYPEVAR_NAME parameterList;
type : TYPE_NAME parameterList;
otherRule : NAME parameterList;

//TODO: Es sollte Regeln für das Result set geben, welche sich nicht mit den anderen überdecken, dann auch nur diese im Result ausgeben
PARAMLIST_NAME : 'param';
UNIFIER_NAME : 'unifier';
EQUALS_NAME : 'equals';
SMALLER_NAME : 'smaller';
TYPEVAR_NAME : 'typeVar';
TYPE_NAME : 'type';
NAME : [a-zA-Z0-9_]+;

WS  :  [ \t\r\n\u000C]+ -> skip
    ;
LINE_COMMENT
    :   '%' ~[\r\n]* -> skip
    ;

