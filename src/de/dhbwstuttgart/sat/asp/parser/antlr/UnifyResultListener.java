// Generated from UnifyResult.g4 by ANTLR 4.7
package de.dhbwstuttgart.sat.asp.parser.antlr;
import org.antlr.v4.runtime.tree.ParseTreeListener;

/**
 * This interface defines a complete listener for a parse tree produced by
 * {@link UnifyResultParser}.
 */
public interface UnifyResultListener extends ParseTreeListener {
	/**
	 * Enter a parse tree produced by {@link UnifyResultParser#answer}.
	 * @param ctx the parse tree
	 */
	void enterAnswer(UnifyResultParser.AnswerContext ctx);
	/**
	 * Exit a parse tree produced by {@link UnifyResultParser#answer}.
	 * @param ctx the parse tree
	 */
	void exitAnswer(UnifyResultParser.AnswerContext ctx);
	/**
	 * Enter a parse tree produced by {@link UnifyResultParser#resultSetRule}.
	 * @param ctx the parse tree
	 */
	void enterResultSetRule(UnifyResultParser.ResultSetRuleContext ctx);
	/**
	 * Exit a parse tree produced by {@link UnifyResultParser#resultSetRule}.
	 * @param ctx the parse tree
	 */
	void exitResultSetRule(UnifyResultParser.ResultSetRuleContext ctx);
	/**
	 * Enter a parse tree produced by {@link UnifyResultParser#parameterList}.
	 * @param ctx the parse tree
	 */
	void enterParameterList(UnifyResultParser.ParameterListContext ctx);
	/**
	 * Exit a parse tree produced by {@link UnifyResultParser#parameterList}.
	 * @param ctx the parse tree
	 */
	void exitParameterList(UnifyResultParser.ParameterListContext ctx);
	/**
	 * Enter a parse tree produced by {@link UnifyResultParser#value}.
	 * @param ctx the parse tree
	 */
	void enterValue(UnifyResultParser.ValueContext ctx);
	/**
	 * Exit a parse tree produced by {@link UnifyResultParser#value}.
	 * @param ctx the parse tree
	 */
	void exitValue(UnifyResultParser.ValueContext ctx);
	/**
	 * Enter a parse tree produced by {@link UnifyResultParser#parameter}.
	 * @param ctx the parse tree
	 */
	void enterParameter(UnifyResultParser.ParameterContext ctx);
	/**
	 * Exit a parse tree produced by {@link UnifyResultParser#parameter}.
	 * @param ctx the parse tree
	 */
	void exitParameter(UnifyResultParser.ParameterContext ctx);
	/**
	 * Enter a parse tree produced by {@link UnifyResultParser#equals}.
	 * @param ctx the parse tree
	 */
	void enterEquals(UnifyResultParser.EqualsContext ctx);
	/**
	 * Exit a parse tree produced by {@link UnifyResultParser#equals}.
	 * @param ctx the parse tree
	 */
	void exitEquals(UnifyResultParser.EqualsContext ctx);
	/**
	 * Enter a parse tree produced by {@link UnifyResultParser#unifier}.
	 * @param ctx the parse tree
	 */
	void enterUnifier(UnifyResultParser.UnifierContext ctx);
	/**
	 * Exit a parse tree produced by {@link UnifyResultParser#unifier}.
	 * @param ctx the parse tree
	 */
	void exitUnifier(UnifyResultParser.UnifierContext ctx);
	/**
	 * Enter a parse tree produced by {@link UnifyResultParser#smaller}.
	 * @param ctx the parse tree
	 */
	void enterSmaller(UnifyResultParser.SmallerContext ctx);
	/**
	 * Exit a parse tree produced by {@link UnifyResultParser#smaller}.
	 * @param ctx the parse tree
	 */
	void exitSmaller(UnifyResultParser.SmallerContext ctx);
	/**
	 * Enter a parse tree produced by {@link UnifyResultParser#typeVar}.
	 * @param ctx the parse tree
	 */
	void enterTypeVar(UnifyResultParser.TypeVarContext ctx);
	/**
	 * Exit a parse tree produced by {@link UnifyResultParser#typeVar}.
	 * @param ctx the parse tree
	 */
	void exitTypeVar(UnifyResultParser.TypeVarContext ctx);
	/**
	 * Enter a parse tree produced by {@link UnifyResultParser#type}.
	 * @param ctx the parse tree
	 */
	void enterType(UnifyResultParser.TypeContext ctx);
	/**
	 * Exit a parse tree produced by {@link UnifyResultParser#type}.
	 * @param ctx the parse tree
	 */
	void exitType(UnifyResultParser.TypeContext ctx);
	/**
	 * Enter a parse tree produced by {@link UnifyResultParser#otherRule}.
	 * @param ctx the parse tree
	 */
	void enterOtherRule(UnifyResultParser.OtherRuleContext ctx);
	/**
	 * Exit a parse tree produced by {@link UnifyResultParser#otherRule}.
	 * @param ctx the parse tree
	 */
	void exitOtherRule(UnifyResultParser.OtherRuleContext ctx);
}