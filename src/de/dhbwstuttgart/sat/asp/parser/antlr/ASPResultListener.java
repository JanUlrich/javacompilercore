// Generated from ASPResult.g4 by ANTLR 4.7
package de.dhbwstuttgart.sat.asp.parser.antlr;
import org.antlr.v4.runtime.tree.ParseTreeListener;

/**
 * This interface defines a complete listener for a parse tree produced by
 * {@link ASPResultParser}.
 */
public interface ASPResultListener extends ParseTreeListener {
	/**
	 * Enter a parse tree produced by {@link ASPResultParser#answer}.
	 * @param ctx the parse tree
	 */
	void enterAnswer(ASPResultParser.AnswerContext ctx);
	/**
	 * Exit a parse tree produced by {@link ASPResultParser#answer}.
	 * @param ctx the parse tree
	 */
	void exitAnswer(ASPResultParser.AnswerContext ctx);
	/**
	 * Enter a parse tree produced by {@link ASPResultParser#resultSetRule}.
	 * @param ctx the parse tree
	 */
	void enterResultSetRule(ASPResultParser.ResultSetRuleContext ctx);
	/**
	 * Exit a parse tree produced by {@link ASPResultParser#resultSetRule}.
	 * @param ctx the parse tree
	 */
	void exitResultSetRule(ASPResultParser.ResultSetRuleContext ctx);
	/**
	 * Enter a parse tree produced by {@link ASPResultParser#parameterList}.
	 * @param ctx the parse tree
	 */
	void enterParameterList(ASPResultParser.ParameterListContext ctx);
	/**
	 * Exit a parse tree produced by {@link ASPResultParser#parameterList}.
	 * @param ctx the parse tree
	 */
	void exitParameterList(ASPResultParser.ParameterListContext ctx);
	/**
	 * Enter a parse tree produced by {@link ASPResultParser#value}.
	 * @param ctx the parse tree
	 */
	void enterValue(ASPResultParser.ValueContext ctx);
	/**
	 * Exit a parse tree produced by {@link ASPResultParser#value}.
	 * @param ctx the parse tree
	 */
	void exitValue(ASPResultParser.ValueContext ctx);
}