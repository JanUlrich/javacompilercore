// Generated from UnifyResult.g4 by ANTLR 4.7
package de.dhbwstuttgart.sat.asp.parser.antlr;
import org.antlr.v4.runtime.atn.*;
import org.antlr.v4.runtime.dfa.DFA;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.misc.*;
import org.antlr.v4.runtime.tree.*;
import java.util.List;
import java.util.Iterator;
import java.util.ArrayList;

@SuppressWarnings({"all", "warnings", "unchecked", "unused", "cast"})
public class UnifyResultParser extends Parser {
	static { RuntimeMetaData.checkVersion("4.7", RuntimeMetaData.VERSION); }

	protected static final DFA[] _decisionToDFA;
	protected static final PredictionContextCache _sharedContextCache =
		new PredictionContextCache();
	public static final int
		T__0=1, T__1=2, T__2=3, T__3=4, T__4=5, PARAMLIST_NAME=6, UNIFIER_NAME=7, 
		EQUALS_NAME=8, SMALLER_NAME=9, TYPEVAR_NAME=10, TYPE_NAME=11, NAME=12, 
		WS=13, LINE_COMMENT=14;
	public static final int
		RULE_answer = 0, RULE_resultSetRule = 1, RULE_parameterList = 2, RULE_value = 3, 
		RULE_parameter = 4, RULE_equals = 5, RULE_unifier = 6, RULE_smaller = 7, 
		RULE_typeVar = 8, RULE_type = 9, RULE_otherRule = 10;
	public static final String[] ruleNames = {
		"answer", "resultSetRule", "parameterList", "value", "parameter", "equals", 
		"unifier", "smaller", "typeVar", "type", "otherRule"
	};

	private static final String[] _LITERAL_NAMES = {
		null, "'ANSWER'", "'.'", "'('", "','", "')'", "'param'", "'unifier'", 
		"'equals'", "'smaller'", "'typeVar'", "'type'"
	};
	private static final String[] _SYMBOLIC_NAMES = {
		null, null, null, null, null, null, "PARAMLIST_NAME", "UNIFIER_NAME", 
		"EQUALS_NAME", "SMALLER_NAME", "TYPEVAR_NAME", "TYPE_NAME", "NAME", "WS", 
		"LINE_COMMENT"
	};
	public static final Vocabulary VOCABULARY = new VocabularyImpl(_LITERAL_NAMES, _SYMBOLIC_NAMES);

	/**
	 * @deprecated Use {@link #VOCABULARY} instead.
	 */
	@Deprecated
	public static final String[] tokenNames;
	static {
		tokenNames = new String[_SYMBOLIC_NAMES.length];
		for (int i = 0; i < tokenNames.length; i++) {
			tokenNames[i] = VOCABULARY.getLiteralName(i);
			if (tokenNames[i] == null) {
				tokenNames[i] = VOCABULARY.getSymbolicName(i);
			}

			if (tokenNames[i] == null) {
				tokenNames[i] = "<INVALID>";
			}
		}
	}

	@Override
	@Deprecated
	public String[] getTokenNames() {
		return tokenNames;
	}

	@Override

	public Vocabulary getVocabulary() {
		return VOCABULARY;
	}

	@Override
	public String getGrammarFileName() { return "UnifyResult.g4"; }

	@Override
	public String[] getRuleNames() { return ruleNames; }

	@Override
	public String getSerializedATN() { return _serializedATN; }

	@Override
	public ATN getATN() { return _ATN; }

	public UnifyResultParser(TokenStream input) {
		super(input);
		_interp = new ParserATNSimulator(this,_ATN,_decisionToDFA,_sharedContextCache);
	}
	public static class AnswerContext extends ParserRuleContext {
		public List<ResultSetRuleContext> resultSetRule() {
			return getRuleContexts(ResultSetRuleContext.class);
		}
		public ResultSetRuleContext resultSetRule(int i) {
			return getRuleContext(ResultSetRuleContext.class,i);
		}
		public AnswerContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_answer; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof UnifyResultListener ) ((UnifyResultListener)listener).enterAnswer(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof UnifyResultListener ) ((UnifyResultListener)listener).exitAnswer(this);
		}
	}

	public final AnswerContext answer() throws RecognitionException {
		AnswerContext _localctx = new AnswerContext(_ctx, getState());
		enterRule(_localctx, 0, RULE_answer);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(22);
			match(T__0);
			setState(28);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << PARAMLIST_NAME) | (1L << UNIFIER_NAME) | (1L << EQUALS_NAME) | (1L << SMALLER_NAME) | (1L << TYPEVAR_NAME) | (1L << TYPE_NAME) | (1L << NAME))) != 0)) {
				{
				{
				setState(23);
				resultSetRule();
				setState(24);
				match(T__1);
				}
				}
				setState(30);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ResultSetRuleContext extends ParserRuleContext {
		public ParameterContext parameter() {
			return getRuleContext(ParameterContext.class,0);
		}
		public EqualsContext equals() {
			return getRuleContext(EqualsContext.class,0);
		}
		public SmallerContext smaller() {
			return getRuleContext(SmallerContext.class,0);
		}
		public TypeVarContext typeVar() {
			return getRuleContext(TypeVarContext.class,0);
		}
		public TypeContext type() {
			return getRuleContext(TypeContext.class,0);
		}
		public UnifierContext unifier() {
			return getRuleContext(UnifierContext.class,0);
		}
		public OtherRuleContext otherRule() {
			return getRuleContext(OtherRuleContext.class,0);
		}
		public ResultSetRuleContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_resultSetRule; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof UnifyResultListener ) ((UnifyResultListener)listener).enterResultSetRule(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof UnifyResultListener ) ((UnifyResultListener)listener).exitResultSetRule(this);
		}
	}

	public final ResultSetRuleContext resultSetRule() throws RecognitionException {
		ResultSetRuleContext _localctx = new ResultSetRuleContext(_ctx, getState());
		enterRule(_localctx, 2, RULE_resultSetRule);
		try {
			setState(38);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case PARAMLIST_NAME:
				enterOuterAlt(_localctx, 1);
				{
				setState(31);
				parameter();
				}
				break;
			case EQUALS_NAME:
				enterOuterAlt(_localctx, 2);
				{
				setState(32);
				equals();
				}
				break;
			case SMALLER_NAME:
				enterOuterAlt(_localctx, 3);
				{
				setState(33);
				smaller();
				}
				break;
			case TYPEVAR_NAME:
				enterOuterAlt(_localctx, 4);
				{
				setState(34);
				typeVar();
				}
				break;
			case TYPE_NAME:
				enterOuterAlt(_localctx, 5);
				{
				setState(35);
				type();
				}
				break;
			case UNIFIER_NAME:
				enterOuterAlt(_localctx, 6);
				{
				setState(36);
				unifier();
				}
				break;
			case NAME:
				enterOuterAlt(_localctx, 7);
				{
				setState(37);
				otherRule();
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ParameterListContext extends ParserRuleContext {
		public List<ValueContext> value() {
			return getRuleContexts(ValueContext.class);
		}
		public ValueContext value(int i) {
			return getRuleContext(ValueContext.class,i);
		}
		public ParameterListContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_parameterList; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof UnifyResultListener ) ((UnifyResultListener)listener).enterParameterList(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof UnifyResultListener ) ((UnifyResultListener)listener).exitParameterList(this);
		}
	}

	public final ParameterListContext parameterList() throws RecognitionException {
		ParameterListContext _localctx = new ParameterListContext(_ctx, getState());
		enterRule(_localctx, 4, RULE_parameterList);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(40);
			match(T__2);
			setState(41);
			value();
			setState(46);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==T__3) {
				{
				{
				setState(42);
				match(T__3);
				setState(43);
				value();
				}
				}
				setState(48);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(49);
			match(T__4);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ValueContext extends ParserRuleContext {
		public TerminalNode NAME() { return getToken(UnifyResultParser.NAME, 0); }
		public ResultSetRuleContext resultSetRule() {
			return getRuleContext(ResultSetRuleContext.class,0);
		}
		public ValueContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_value; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof UnifyResultListener ) ((UnifyResultListener)listener).enterValue(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof UnifyResultListener ) ((UnifyResultListener)listener).exitValue(this);
		}
	}

	public final ValueContext value() throws RecognitionException {
		ValueContext _localctx = new ValueContext(_ctx, getState());
		enterRule(_localctx, 6, RULE_value);
		try {
			setState(53);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,3,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(51);
				match(NAME);
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(52);
				resultSetRule();
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ParameterContext extends ParserRuleContext {
		public TerminalNode PARAMLIST_NAME() { return getToken(UnifyResultParser.PARAMLIST_NAME, 0); }
		public ParameterListContext parameterList() {
			return getRuleContext(ParameterListContext.class,0);
		}
		public ParameterContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_parameter; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof UnifyResultListener ) ((UnifyResultListener)listener).enterParameter(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof UnifyResultListener ) ((UnifyResultListener)listener).exitParameter(this);
		}
	}

	public final ParameterContext parameter() throws RecognitionException {
		ParameterContext _localctx = new ParameterContext(_ctx, getState());
		enterRule(_localctx, 8, RULE_parameter);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(55);
			match(PARAMLIST_NAME);
			setState(56);
			parameterList();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class EqualsContext extends ParserRuleContext {
		public TerminalNode EQUALS_NAME() { return getToken(UnifyResultParser.EQUALS_NAME, 0); }
		public ParameterListContext parameterList() {
			return getRuleContext(ParameterListContext.class,0);
		}
		public EqualsContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_equals; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof UnifyResultListener ) ((UnifyResultListener)listener).enterEquals(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof UnifyResultListener ) ((UnifyResultListener)listener).exitEquals(this);
		}
	}

	public final EqualsContext equals() throws RecognitionException {
		EqualsContext _localctx = new EqualsContext(_ctx, getState());
		enterRule(_localctx, 10, RULE_equals);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(58);
			match(EQUALS_NAME);
			setState(59);
			parameterList();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class UnifierContext extends ParserRuleContext {
		public TerminalNode UNIFIER_NAME() { return getToken(UnifyResultParser.UNIFIER_NAME, 0); }
		public ParameterListContext parameterList() {
			return getRuleContext(ParameterListContext.class,0);
		}
		public UnifierContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_unifier; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof UnifyResultListener ) ((UnifyResultListener)listener).enterUnifier(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof UnifyResultListener ) ((UnifyResultListener)listener).exitUnifier(this);
		}
	}

	public final UnifierContext unifier() throws RecognitionException {
		UnifierContext _localctx = new UnifierContext(_ctx, getState());
		enterRule(_localctx, 12, RULE_unifier);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(61);
			match(UNIFIER_NAME);
			setState(62);
			parameterList();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class SmallerContext extends ParserRuleContext {
		public TerminalNode SMALLER_NAME() { return getToken(UnifyResultParser.SMALLER_NAME, 0); }
		public ParameterListContext parameterList() {
			return getRuleContext(ParameterListContext.class,0);
		}
		public SmallerContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_smaller; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof UnifyResultListener ) ((UnifyResultListener)listener).enterSmaller(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof UnifyResultListener ) ((UnifyResultListener)listener).exitSmaller(this);
		}
	}

	public final SmallerContext smaller() throws RecognitionException {
		SmallerContext _localctx = new SmallerContext(_ctx, getState());
		enterRule(_localctx, 14, RULE_smaller);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(64);
			match(SMALLER_NAME);
			setState(65);
			parameterList();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class TypeVarContext extends ParserRuleContext {
		public TerminalNode TYPEVAR_NAME() { return getToken(UnifyResultParser.TYPEVAR_NAME, 0); }
		public ParameterListContext parameterList() {
			return getRuleContext(ParameterListContext.class,0);
		}
		public TypeVarContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_typeVar; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof UnifyResultListener ) ((UnifyResultListener)listener).enterTypeVar(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof UnifyResultListener ) ((UnifyResultListener)listener).exitTypeVar(this);
		}
	}

	public final TypeVarContext typeVar() throws RecognitionException {
		TypeVarContext _localctx = new TypeVarContext(_ctx, getState());
		enterRule(_localctx, 16, RULE_typeVar);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(67);
			match(TYPEVAR_NAME);
			setState(68);
			parameterList();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class TypeContext extends ParserRuleContext {
		public TerminalNode TYPE_NAME() { return getToken(UnifyResultParser.TYPE_NAME, 0); }
		public ParameterListContext parameterList() {
			return getRuleContext(ParameterListContext.class,0);
		}
		public TypeContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_type; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof UnifyResultListener ) ((UnifyResultListener)listener).enterType(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof UnifyResultListener ) ((UnifyResultListener)listener).exitType(this);
		}
	}

	public final TypeContext type() throws RecognitionException {
		TypeContext _localctx = new TypeContext(_ctx, getState());
		enterRule(_localctx, 18, RULE_type);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(70);
			match(TYPE_NAME);
			setState(71);
			parameterList();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class OtherRuleContext extends ParserRuleContext {
		public TerminalNode NAME() { return getToken(UnifyResultParser.NAME, 0); }
		public ParameterListContext parameterList() {
			return getRuleContext(ParameterListContext.class,0);
		}
		public OtherRuleContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_otherRule; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof UnifyResultListener ) ((UnifyResultListener)listener).enterOtherRule(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof UnifyResultListener ) ((UnifyResultListener)listener).exitOtherRule(this);
		}
	}

	public final OtherRuleContext otherRule() throws RecognitionException {
		OtherRuleContext _localctx = new OtherRuleContext(_ctx, getState());
		enterRule(_localctx, 20, RULE_otherRule);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(73);
			match(NAME);
			setState(74);
			parameterList();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static final String _serializedATN =
		"\3\u608b\ua72a\u8133\ub9ed\u417c\u3be7\u7786\u5964\3\20O\4\2\t\2\4\3\t"+
		"\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7\4\b\t\b\4\t\t\t\4\n\t\n\4\13\t\13\4"+
		"\f\t\f\3\2\3\2\3\2\3\2\7\2\35\n\2\f\2\16\2 \13\2\3\3\3\3\3\3\3\3\3\3\3"+
		"\3\3\3\5\3)\n\3\3\4\3\4\3\4\3\4\7\4/\n\4\f\4\16\4\62\13\4\3\4\3\4\3\5"+
		"\3\5\5\58\n\5\3\6\3\6\3\6\3\7\3\7\3\7\3\b\3\b\3\b\3\t\3\t\3\t\3\n\3\n"+
		"\3\n\3\13\3\13\3\13\3\f\3\f\3\f\3\f\2\2\r\2\4\6\b\n\f\16\20\22\24\26\2"+
		"\2\2L\2\30\3\2\2\2\4(\3\2\2\2\6*\3\2\2\2\b\67\3\2\2\2\n9\3\2\2\2\f<\3"+
		"\2\2\2\16?\3\2\2\2\20B\3\2\2\2\22E\3\2\2\2\24H\3\2\2\2\26K\3\2\2\2\30"+
		"\36\7\3\2\2\31\32\5\4\3\2\32\33\7\4\2\2\33\35\3\2\2\2\34\31\3\2\2\2\35"+
		" \3\2\2\2\36\34\3\2\2\2\36\37\3\2\2\2\37\3\3\2\2\2 \36\3\2\2\2!)\5\n\6"+
		"\2\")\5\f\7\2#)\5\20\t\2$)\5\22\n\2%)\5\24\13\2&)\5\16\b\2\')\5\26\f\2"+
		"(!\3\2\2\2(\"\3\2\2\2(#\3\2\2\2($\3\2\2\2(%\3\2\2\2(&\3\2\2\2(\'\3\2\2"+
		"\2)\5\3\2\2\2*+\7\5\2\2+\60\5\b\5\2,-\7\6\2\2-/\5\b\5\2.,\3\2\2\2/\62"+
		"\3\2\2\2\60.\3\2\2\2\60\61\3\2\2\2\61\63\3\2\2\2\62\60\3\2\2\2\63\64\7"+
		"\7\2\2\64\7\3\2\2\2\658\7\16\2\2\668\5\4\3\2\67\65\3\2\2\2\67\66\3\2\2"+
		"\28\t\3\2\2\29:\7\b\2\2:;\5\6\4\2;\13\3\2\2\2<=\7\n\2\2=>\5\6\4\2>\r\3"+
		"\2\2\2?@\7\t\2\2@A\5\6\4\2A\17\3\2\2\2BC\7\13\2\2CD\5\6\4\2D\21\3\2\2"+
		"\2EF\7\f\2\2FG\5\6\4\2G\23\3\2\2\2HI\7\r\2\2IJ\5\6\4\2J\25\3\2\2\2KL\7"+
		"\16\2\2LM\5\6\4\2M\27\3\2\2\2\6\36(\60\67";
	public static final ATN _ATN =
		new ATNDeserializer().deserialize(_serializedATN.toCharArray());
	static {
		_decisionToDFA = new DFA[_ATN.getNumberOfDecisions()];
		for (int i = 0; i < _ATN.getNumberOfDecisions(); i++) {
			_decisionToDFA[i] = new DFA(_ATN.getDecisionState(i), i);
		}
	}
}