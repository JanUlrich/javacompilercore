grammar ASPResult;

answer : 'ANSWER' (resultSetRule '.')*;

resultSetRule : NAME parameterList;

parameterList : '(' value (',' value)*  ')';
value : NAME
        | resultSetRule ;

NAME : [a-zA-Z0-9_']+;

WS  :  [ \t\r\n\u000C]+ -> skip
    ;
LINE_COMMENT
    :   '%' ~[\r\n]* -> skip
    ;
