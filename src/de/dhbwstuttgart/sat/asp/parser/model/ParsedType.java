package de.dhbwstuttgart.sat.asp.parser.model;

import java.util.List;

public class ParsedType {
    public final String name;
    public final String params;
    public ParsedType(String name, String params){
        this.name = name;
        this.params = params;
    }
}
