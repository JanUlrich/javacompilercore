package de.dhbwstuttgart.sat.asp.parser;

import de.dhbwstuttgart.exceptions.DebugException;
import de.dhbwstuttgart.exceptions.NotImplementedException;
import de.dhbwstuttgart.parser.NullToken;
import de.dhbwstuttgart.parser.scope.JavaClassName;
import de.dhbwstuttgart.sat.asp.ASPStringConverter;
import de.dhbwstuttgart.sat.asp.model.ASPRule;
import de.dhbwstuttgart.sat.asp.parser.antlr.*;
import de.dhbwstuttgart.sat.asp.parser.model.ParsedType;
import de.dhbwstuttgart.syntaxtree.type.*;
import de.dhbwstuttgart.typeinference.constraints.Pair;
import de.dhbwstuttgart.typeinference.result.*;
import de.dhbwstuttgart.typeinference.unify.model.PairOperator;
import org.antlr.v4.runtime.CharStreams;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.tree.ParseTreeWalker;

import java.util.*;
import java.util.stream.Collectors;

/**
 * Ablauf:
 * - Erst werden alle benötigten Statements eingelesen. Die Pointer bleiben als Strings erhalten
 * - Anschließend müssen diese wieder zu einem Unify Ergebnis zurückgewandelt werden
 * - Hier nicht die Typen aus dem unify.model packages verwenden.
 * TODO: Überlegen welche Informationen noch nach der Unifizierung gebraucht werden
 *  -> Eigentlich nur die korrekten Namen der Typen und TPHs
 */
public class ASPParser extends ASPResultBaseListener {
    private Collection<TypePlaceholder> originalTPHs;
    private ResultSet resultSet;
    private Map<String, RefTypeOrTPHOrWildcardOrGeneric> types = new HashMap();
    private Set<String> tphs = new HashSet<>();

    /**
     * Parst clingo output welcher als JSON (option --outf=2) ausgibt
     * @param toParse
     * @return
     */
    public static ResultSet parse(String toParse, Collection<TypePlaceholder> oldPlaceholders){
        return new ASPParser(toParse, oldPlaceholders).resultSet;
    }

    private static class Relation {
        public final String right;
        public final String left;

        Relation(String leftType, String rightType){
            this.left = leftType;
            this.right = rightType;
        }

        @Override
        public int hashCode() {
            return (left+right).hashCode();
        }

        @Override
        public boolean equals(Object obj) {
            if(obj instanceof Relation)
                return (right+left).equals(((Relation) obj).left+((Relation) obj).right);
            return super.equals(obj);
        }
    }

    private List<String> parseParameterList(UnifyResultParser.ParameterListContext ctx){
        return ctx.value().stream().map(v ->
                //ASPStringConverter.fromConstant(v.getText())
                v.getText()
        ).collect(Collectors.toList());
    }

    private void getTPHs(ASPResultParser.AnswerContext answer){
        for(ASPResultParser.ResultSetRuleContext e : answer.resultSetRule()){
            if(e.NAME().getText().equals(ASPRule.ASP_TYPE_VAR.toString())){
                String pointer = e.parameterList().value(0).getText();
                tphs.add(pointer);
                String tphName = ASPStringConverter.fromConstant(pointer);
                Optional<TypePlaceholder> oTPH = originalTPHs.stream().filter((a)->a.getName().equals(tphName)).findAny();
                TypePlaceholder tph;
                if(oTPH.isPresent()){
                    tph = oTPH.get();
                }else{
                    tph = TypePlaceholder.fresh(new NullToken());
                }
                types.put(pointer, tph);
            }
        }
    }
    private void getTypes(ASPResultParser.AnswerContext answer) {
        HashMap<String, String[]> rawTypes = new HashMap<>();
        for(ASPResultParser.ResultSetRuleContext e : answer.resultSetRule()){
            if(e.NAME().getText().equals(ASPRule.ASP_TYPE.toString())){
                String pointer = e.parameterList().value(0).getText();
                String name = e.parameterList().value(1).getText();
                Integer numParams = Integer.parseInt(e.parameterList().value(2).getText());
                String[] params = new String[numParams + 1];
                params[0] = name;
                rawTypes.put(pointer, params);
            }
        }
        for(ASPResultParser.ResultSetRuleContext e : answer.resultSetRule()){
            if(e.NAME().getText().equals(ASPRule.ASP_PARAMLIST_NAME.toString())){
                String typePointer = e.parameterList().value(0).getText();
                String paramPointer = e.parameterList().value(1).getText();
                Integer paramNum = Integer.parseInt(e.parameterList().value(2).getText());
                if(rawTypes.containsKey(typePointer)) {
                    String[] paramArray = rawTypes.get(typePointer);
                    paramArray[paramNum] = paramPointer;
                }
            }
        }

        for(String name : rawTypes.keySet()){
            types.put(name, createType(name, rawTypes));
        }

    }
    private RefType createType(String name, HashMap<String, String[]> rawTypes){
        String[] paramArray = rawTypes.get(name);
        List<RefTypeOrTPHOrWildcardOrGeneric> paramList = new ArrayList<>();
        for(int i = 1; i< paramArray.length ;i++){
            String paramPointer = paramArray[i];
            RefTypeOrTPHOrWildcardOrGeneric param;
            if(rawTypes.containsKey(paramPointer)){
                param = createType(paramPointer, rawTypes);
            }else if(tphs.contains(paramPointer)){
                String tphName = ASPStringConverter.fromConstant(paramPointer);
                param = types.get(paramPointer);
            }else{
                throw new DebugException("Fehler beim Einlesen der Clingo Ausgabe");
            }
            if(param == null)
                throw new NullPointerException();
            paramList.add(param);
        }
        return new RefType(new JavaClassName(ASPStringConverter.fromConstant(paramArray[0])), paramList, new NullToken());
    }

    private ASPParser(String toParse, Collection<TypePlaceholder> oldPlaceholders){
        //System.out.println(toParse);
        this.originalTPHs = oldPlaceholders;

        /*
        JsonObject jsonResult = Json.createReader(new StringReader(toParse)).readObject();
        JsonArray results = jsonResult.getJsonArray("Call").getJsonObject(0).
                getJsonArray("Witnesses").getJsonObject(0).
                getJsonArray("Value");

        //Im ersten Schritt werden alle Regeln geparst
        String completeResult = "";
        for(int i = 0; i<results.size();i++){
            String aspStatement = results.getString(i);
            completeResult += aspStatement + " ";
        }
        System.out.println(completeResult);
        */
        ASPResultLexer lexer = new ASPResultLexer(CharStreams.fromString(toParse));
        ASPResultParser.AnswerContext parseTree = new ASPResultParser(new CommonTokenStream(lexer)).answer();
        //new ParseTreeWalker().walk(this, parseTree);

        /*
        Diese Funktion läuft im folgenden mehrmals über das Result aus dem ASP Programm.
        Dabei werden Schritt für Schritt die Felder dieser Klasse befüllt die am Schluss das ResultSet ergeben
        */
        getTPHs(parseTree);
        getTypes(parseTree);

        Set<ResultPair> ret = new HashSet<>();
        for(ASPResultParser.ResultSetRuleContext e : parseTree.resultSetRule()){
            if(e.NAME().getText().equals(ASPRule.ASP_PAIR_EQUALS_NAME.toString())){
                String tp1 = e.parameterList().value(0).getText();
                String tp2 = e.parameterList().value(1).getText();
                if(tphs.contains(tp1) && tphs.contains(tp2)){
                    //Diese kann man ignorieren. Sollten eigentlich nicht auftauchen
                    //ret.add(new PairTPHEqualTPH((TypePlaceholder) types.get(tp1), (TypePlaceholder) types.get(tp2)));
                }else if(tphs.contains(tp1)){
                    if(types.containsKey(tp2))
                    ret.add(new PairTPHequalRefTypeOrWildcardType((TypePlaceholder) types.get(tp1), types.get(tp2)));
                    else System.out.println(tp2);
                }
            }
        }
        this.resultSet = new ResultSet(ret);
    }

}