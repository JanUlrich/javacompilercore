package de.dhbwstuttgart.sat.asp;


import org.apache.commons.io.IOUtils;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class ASPUnify {
    private final List<File> input;
    private static final List<File> programFiles = new ArrayList<>();

    private static final String aspDirectory = System.getProperty("user.dir")+"/asp/";
    static{
        programFiles.add(new File(aspDirectory + "fc.lp"));
        programFiles.add(new File(aspDirectory + "reduceRules.lp"));
        programFiles.add(new File(aspDirectory + "adaptRules.lp"));
        programFiles.add(new File(aspDirectory + "step4.lp"));
        programFiles.add(new File(aspDirectory + "subst.lp"));
        programFiles.add(new File(aspDirectory + "unifikation.lp"));
        programFiles.add(new File(aspDirectory + "cartesian.lp"));
        programFiles.add(new File(aspDirectory + "result.lp"));
    }

    public ASPUnify(List<File> inputFiles){
        this.input = inputFiles;
    }

    /*
    TODO: Clingo per Java Wrapper https://stackoverflow.com/questions/3356200/using-java-to-wrap-over-c
     */
    public String runClingo() throws IOException, InterruptedException {
        String pathToClingo =
                "clingo";
        List<String> commands = new ArrayList<>();
        commands.add(pathToClingo);
        //commands.add("--outf=2"); //use JSON-Output
        commands.add("--outf=1"); //use Text-Output
        commands.add("-n 0"); //Compute n models
        for(File file : input){
            commands.add(file.getPath());
        }
        commands.addAll(programFiles.stream().map(f->f.getPath()).collect(Collectors.toList()));

        //commands.stream().forEach(s -> System.out.print(s + " "));
        //System.out.println();

        Process clingo = new ProcessBuilder( commands.toArray(new String[0])).start();
        InputStream output = clingo.getInputStream();
        clingo.waitFor();
        String result = IOUtils.toString(output, StandardCharsets.UTF_8);
        System.out.println(result);
        return result;
    }
}
