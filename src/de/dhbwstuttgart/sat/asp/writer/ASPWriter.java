package de.dhbwstuttgart.sat.asp.writer;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;

public class ASPWriter {

    private HashSet<ASPStatement> content = new HashSet<>();

    public void add(ASPStatement stmt){
        content.add(stmt);
    }

    public String getASPFile(){
        String ret = "";
        for(ASPStatement statement : content){
            ret += statement.getASP() + ".\n";
        }
        return ret;
    }

    public void addAll(Collection<ASPStatement> aspStatements) {
        content.addAll(aspStatements);
    }
}
