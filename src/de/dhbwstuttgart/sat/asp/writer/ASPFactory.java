package de.dhbwstuttgart.sat.asp.writer;

import de.dhbwstuttgart.exceptions.NotImplementedException;
import de.dhbwstuttgart.parser.NullToken;
import de.dhbwstuttgart.parser.SyntaxTreeGenerator.FCGenerator;
import de.dhbwstuttgart.sat.asp.ASPStringConverter;
import de.dhbwstuttgart.sat.asp.model.ASPRule;
import de.dhbwstuttgart.syntaxtree.ClassOrInterface;
import de.dhbwstuttgart.syntaxtree.GenericTypeVar;
import de.dhbwstuttgart.syntaxtree.factory.NameGenerator;
import de.dhbwstuttgart.syntaxtree.type.*;
import de.dhbwstuttgart.syntaxtree.visual.ASTPrinter;
import de.dhbwstuttgart.syntaxtree.visual.OutputGenerator;
import de.dhbwstuttgart.typeinference.constraints.Constraint;
import de.dhbwstuttgart.typeinference.constraints.ConstraintSet;
import de.dhbwstuttgart.typeinference.constraints.Pair;
import de.dhbwstuttgart.typeinference.unify.model.PairOperator;

import javax.management.Notification;
import java.util.*;

public class ASPFactory implements TypeVisitor<String>{

    public static String generateASP(ConstraintSet<Pair> constraints, Collection<ClassOrInterface> fcClasses) throws ClassNotFoundException{
        ASPFactory factory = new ASPFactory();
        factory.convertFC(fcClasses);
        //List<Constraint<Pair>> constraints1 = constraints.cartesianProduct().iterator().next();
        for(Pair p : constraints.getUndConstraints()){
            factory.convertPair(p);
        }
        for(Set<Constraint<Pair>> oder : constraints.getOderConstraints()){
            factory.convertOderConstraint(oder);
        }

        return factory.writer.getASPFile();
    }

    /**
     * Wandelt eine Reihe von Constraints zu einem Und-Constraint - also einer Liste - um.
     * Dieser kann dann in Oder-Constraints verwendet werden.
     * @param undCons - Eine Liste von Constraints. Bsp: equals(..), smallerDot(..)
     * @return - list(equals, list(smallerDot(..., null)
     */
    protected ASPStatement convertListToUndConstraint(List<ASPStatement> undCons) {
        if(undCons.size() == 0)throw new NullPointerException();
        if(undCons.size() == 1){
            return undCons.get(0);
        }
        ASPStatement list = new ASPStatement(ASPRule.ASP_LIST_ENDPOINTER.toString());
        for(ASPStatement con : undCons){
            list = makeStatement(ASPRule.ASP_LIST_NAME.toString(), con.getASP(), list.getASP());
        }
        return list;
    }

    protected ASPStatement convertListToUndConstraint(Constraint<Pair> undCons) {
        List<ASPStatement> convert = new ArrayList<>();
        for(Pair p : undCons){
            convert.add(generatePairStmt(p));
        }
        return convertListToUndConstraint(convert);
    }

    protected void convertOderConstraint(Set<Constraint<Pair>> oder) {
        if(oder.size() < 2){//Oder-Setgröße darf nicht null sein. Sonst gibt es sowieso kein Ergebnis:
            for(Pair p : oder.iterator().next()){
                this.convertPair(p); //Einfach als und Constraints behandeln, wenn es nur einen Oder-Zweig gibt
            }
            return;
        }
        List<ASPStatement> ret = new ArrayList<>();
        Iterator<Constraint<Pair>> it = oder.iterator();
        String pointer1 = ASPStringConverter.toConstant(NameGenerator.makeNewName());
        ASPStatement stmt = makeStatement(ASPRule.ASP_CONSTRAINT.toString(), pointer1,
                convertListToUndConstraint(it.next()).getASP());
        ret.add(stmt);
        while(it.hasNext()){
            String pointer2 = ASPStringConverter.toConstant(NameGenerator.makeNewName());
            Constraint<Pair> cons = it.next();
            stmt = makeStatement(ASPRule.ASP_CONSTRAINT.toString(), pointer2,
                    convertListToUndConstraint(cons).getASP());
            ret.add(stmt);
            ASPStatement oderStmt = makeStatement(ASPRule.ASP_ODER.toString(), pointer1, pointer2);
            if(it.hasNext()){
                String oderPointer = ASPStringConverter.toConstant(NameGenerator.makeNewName());
                stmt = makeStatement(ASPRule.ASP_CONSTRAINT.toString(), oderPointer, oderStmt.getASP());
                ret.add(stmt);
                pointer1 = oderPointer;
            }else{
                ret.add(oderStmt);
            }
        }
        //Alle erstellten Constraints schreiben:
        writer.addAll(ret);
    }

    protected ASPWriter writer = new ASPWriter();

    protected void convertFC(Collection<ClassOrInterface> classes) throws ClassNotFoundException {
        Collection<Pair> fc = FCGenerator.toFC(classes);
        HashMap<String, Pair> set = new HashMap<>();
        for(Pair fcp : fc){
            StringBuilder output = new StringBuilder();
            OutputGenerator generator = new OutputGenerator(output);
            fcp.TA1.accept(generator);
            output.append("<");
            fcp.TA2.accept(generator);
            set.put(output.toString(), fcp);
        }
        for(Pair fcp : set.values()){
            convertPair(fcp);
        }
        for(ClassOrInterface cl : classes){
            RefType t = fromClassOrInterface(cl);
            convertPair(new Pair(t,t,PairOperator.SMALLER));
        }
    }

    private RefType fromClassOrInterface(ClassOrInterface cl){
        List<RefTypeOrTPHOrWildcardOrGeneric> params = new ArrayList<>();
        for(GenericTypeVar gtv : cl.getGenerics()){
            params.add(TypePlaceholder.fresh(new NullToken()));
        }
        return new RefType(cl.getClassName(), params, new NullToken());
    }

    protected void convertPair(Pair p){
        ASPStatement pairStmt = generatePairStmt(p);
        writer.add(pairStmt);
    }

    protected ASPStatement generatePairStmt(Pair p) {
        String ls = p.TA1.acceptTV(this);
        String rs = p.TA2.acceptTV(this);
        ASPStatement pairStmt = null;
        if(p.GetOperator().equals(PairOperator.SMALLERDOT)){
            pairStmt = makeStatement(ASPRule.ASP_PAIR_SMALLER_DOT_NAME.toString(), ls, rs);
        }else if(p.GetOperator().equals(PairOperator.EQUALSDOT)){
            pairStmt = makeStatement(ASPRule.ASP_PAIR_EQUALS_NAME.toString(), ls, rs);
        }else if(p.GetOperator().equals(PairOperator.SMALLER)){
            pairStmt = makeStatement(ASPRule.ASP_PAIR_SMALLER_NAME.toString(), ls, rs);
        }else throw new NotImplementedException();
        return pairStmt;
    }

    protected ASPStatement makeStatement(String rule, String... params){
        String stmt = rule + "(";
        Iterator<String> it = Arrays.asList(params).iterator();
        while(it.hasNext()){
            String param = it.next();
            stmt += param;
            if(it.hasNext())stmt+=",";
        }
        stmt += ")";
        return new ASPStatement(stmt);
    }

    protected void convertParameterlist(String pointer, List<String> pointers){
        Iterator<String> it = pointers.iterator();
        Integer i = 1;
        String ruleName = ASPRule.ASP_PARAMLIST_NAME.toString();
        while (it.hasNext()){
            ASPStatement stmt;
            String type = it.next();
            stmt = makeStatement(ruleName, pointer, type, i.toString());
            writer.add(stmt);
            i++;
        }
    }

    @Override
    public String visit(RefType refType) {
        String pointer = ASPStringConverter.toConstant(NameGenerator.makeNewName());
        List<String> params = new ArrayList<>();
        for(RefTypeOrTPHOrWildcardOrGeneric param : refType.getParaList()){
            params.add(param.acceptTV(this));
        }
        String typeName = ASPStringConverter.toConstant(refType.getName());
        String ruleName = ASPRule.ASP_TYPE.toString();
        convertParameterlist(pointer, params);
        ASPStatement stmt = makeStatement(ruleName, pointer, typeName, Integer.toString(params.size()));
        writer.add(stmt);
        return pointer;
    }

    @Override
    public String visit(SuperWildcardType superWildcardType) {
        throw new NotImplementedException();
    }

    @Override
    public String visit(TypePlaceholder typePlaceholder) {
        String name = ASPStringConverter.toConstant(typePlaceholder.getName());
        ASPStatement stmt = makeStatement(ASPRule.ASP_TYPE_VAR.toString(), name);
        writer.add(stmt);
        return name;
    }

    @Override
    public String visit(ExtendsWildcardType extendsWildcardType) {
        throw new NotImplementedException();
    }

    @Override
    public String visit(GenericRefType genericRefType) {
        //Kann eigentlich wie ein normaler RefType behandelt werden
        String pointer = ASPStringConverter.toConstant(NameGenerator.makeNewName());
        String typeName = ASPStringConverter.toConstant(genericRefType.getParsedName());
        String ruleName = ASPRule.ASP_TYPE.toString();
        ASPStatement stmt = makeStatement(ruleName, pointer, typeName, ASPRule.ASP_LIST_ENDPOINTER.toString());
        writer.add(stmt);
        return pointer;
    }
}
