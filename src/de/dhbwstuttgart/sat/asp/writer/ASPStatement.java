package de.dhbwstuttgart.sat.asp.writer;

public class ASPStatement {
    private final String stmt;
    public ASPStatement(String stmt) {
        this.stmt = stmt;
    }

    public String toString(){
        return stmt;
    }

    @Override
    public int hashCode() {
        return stmt.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if(obj instanceof ASPStatement)return stmt.equals(((ASPStatement) obj).stmt);
        return false;
    }

    public String getASP() {
        return stmt;
    }
}
