package de.dhbwstuttgart.sat.asp.model;

import de.dhbwstuttgart.bytecode.signature.Signature;

public enum ASPRule {
    ASP_PAIR_EQUALS_NAME("equals"),
    ASP_PAIR_SMALLER_NAME("smaller"),
    ASP_PAIR_SMALLER_DOT_NAME("smallerDot"),
    ASP_PARAMLIST_NAME("param"),
    ASP_TYPE("type"),
    ASP_TYPE_VAR("typeVar"), ASP_ODER("oder"),
    ASP_CONSTRAINT("constraint"),
    ASP_LIST_NAME("list"),
    ASP_LIST_ENDPOINTER("null");

    private final String text;

    private ASPRule(final String text) {
        this.text = text;
    }

    @Override
    public String toString() {
        return text;
    }
}
