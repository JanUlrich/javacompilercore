package de.dhbwstuttgart.sat.asp.model;

import de.dhbwstuttgart.typeinference.constraints.Pair;

public enum ASPGencayRule {
    ASP_PAIR_EQUALS_NAME("equals"),
    ASP_PAIR_SMALLER_NAME("sub"),
    ASP_PAIR_SMALLER_DOT_NAME("subEq"),
    ASP_PARAMLIST_NAME("paramEq"),
    ASP_FC_PARAMLIST_NAME("param"),
    ASP_PARAMLIST_END_POINTER("null"),
    ASP_PARAMLIST_END_RULE_EQ("endParamEq"),
    ASP_PARAMLIST_END_RULE_FC("endParam"),
    ASP_TYPE("typeEq"),
    ASP_FCTYPE("type"),
    ASP_TYPE_VAR("var"),
    ASP_GENERIC_VAR("pph"),
    ASP_WILDCARD("wildcard"),
    ASP_PARAMLIST_ORDER("paramOrder");

    private final String text;

    private ASPGencayRule(final String text) {
        this.text = text;
    }

    @Override
    public String toString() {
        return text;
    }
}
