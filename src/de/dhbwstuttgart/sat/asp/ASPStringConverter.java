package de.dhbwstuttgart.sat.asp;

import de.dhbwstuttgart.parser.scope.JavaClassName;

import java.util.HashMap;
import java.util.Map;

public class ASPStringConverter {
    private static final Map<String, String> replacements = new HashMap<>();
    static{
        replacements.put(".", "_DOT_");
        replacements.put("$", "_DOLLAR_");
    }

    public static String toConstant(JavaClassName name){
        return toConstant(name.toString());
    }

    public static String toConstant(String name){
        return "c" + replace(name);
    }

    public static String fromConstant(String s){
        return unReplace(s).substring(1);
    }

    private static String replace(String input){
        for(String toReplace : replacements.keySet()){
            input = input.replace(toReplace, replacements.get(toReplace));
        }
        return input;
    }

    private static String unReplace(String input){
        for(String toReplace : replacements.keySet()){
            input = input.replace(replacements.get(toReplace), toReplace);
        }
        return input;
    }
}
