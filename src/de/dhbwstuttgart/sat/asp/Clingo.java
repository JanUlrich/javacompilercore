package de.dhbwstuttgart.sat.asp;


import org.apache.commons.io.IOUtils;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class Clingo {
    private final List<File> input;
    private static final List<File> programFiles = new ArrayList<>();
    static{
        
    }

    public Clingo(List<File> inputFiles){
        this.input = inputFiles;
    }

    /*
    TODO: Clingo per Java Wrapper https://stackoverflow.com/questions/3356200/using-java-to-wrap-over-c
     */
    public String runClingo() throws IOException, InterruptedException {
        String pathToClingo =
                "clingo";
        List<String> commands = new ArrayList<>();
        commands.add(pathToClingo);
        //commands.add("--outf=2"); //use JSON-Output
        commands.add("--outf=1"); //use Text-Output
        commands.add("-n 0"); //Compute all models
        for(File file : input){
            commands.add(file.getPath());
        }
        commands.addAll(programFiles.stream().map(f->f.getPath()).collect(Collectors.toList()));

        Process clingo = new ProcessBuilder( commands.toArray(new String[0])).start();
        InputStream output = clingo.getInputStream();
        clingo.waitFor();
        String result = IOUtils.toString(output, StandardCharsets.UTF_8);
        return result;
    }
}
