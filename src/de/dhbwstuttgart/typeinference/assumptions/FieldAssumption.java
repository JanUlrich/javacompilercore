package de.dhbwstuttgart.typeinference.assumptions;

import de.dhbwstuttgart.exceptions.NotImplementedException;
import de.dhbwstuttgart.syntaxtree.ClassOrInterface;
import de.dhbwstuttgart.syntaxtree.TypeScope;
import de.dhbwstuttgart.syntaxtree.type.RefTypeOrTPHOrWildcardOrGeneric;
import de.dhbwstuttgart.typeinference.constraints.GenericsResolver;

public class FieldAssumption extends Assumption{
    private ClassOrInterface receiverClass;
    private RefTypeOrTPHOrWildcardOrGeneric type;

    public FieldAssumption(ClassOrInterface receiverType,
                           RefTypeOrTPHOrWildcardOrGeneric type, TypeScope scope){
        super(scope);
        this.type = type;
        this.receiverClass = receiverType;
    }

    public ClassOrInterface getReceiverClass() {
        return receiverClass;
    }

    public RefTypeOrTPHOrWildcardOrGeneric getType(GenericsResolver resolver) {
        return resolver.resolve(type);
    }

    public RefTypeOrTPHOrWildcardOrGeneric getReceiverType(GenericsResolver resolver) {
        //TODO
        throw new NotImplementedException();
    }
}
