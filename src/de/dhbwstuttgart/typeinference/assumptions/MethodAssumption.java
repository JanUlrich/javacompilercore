package de.dhbwstuttgart.typeinference.assumptions;

import de.dhbwstuttgart.parser.NullToken;
import de.dhbwstuttgart.syntaxtree.ClassOrInterface;
import de.dhbwstuttgart.syntaxtree.GenericTypeVar;
import de.dhbwstuttgart.syntaxtree.TypeScope;
import de.dhbwstuttgart.syntaxtree.type.FunN;
import de.dhbwstuttgart.syntaxtree.type.GenericRefType;
import de.dhbwstuttgart.syntaxtree.type.RefType;
import de.dhbwstuttgart.syntaxtree.type.RefTypeOrTPHOrWildcardOrGeneric;
import de.dhbwstuttgart.typeinference.constraints.GenericsResolver;

import java.util.ArrayList;
import java.util.List;

public class MethodAssumption extends Assumption{
    private ClassOrInterface receiver;
    private RefTypeOrTPHOrWildcardOrGeneric retType;
    List<RefTypeOrTPHOrWildcardOrGeneric> params;

    public MethodAssumption(ClassOrInterface receiver, RefTypeOrTPHOrWildcardOrGeneric retType,
                            List<RefTypeOrTPHOrWildcardOrGeneric> params, TypeScope scope){
        super(scope);
        this.receiver = receiver;
        this.retType = retType;
        this.params = params;
    }

    /*
    public RefType getReceiverType() {

        return receiver;
    }
 */

    public ClassOrInterface getReceiver(){
        return receiver;
    }

    public RefTypeOrTPHOrWildcardOrGeneric getReturnType(GenericsResolver resolver) {
        if(retType instanceof GenericRefType)return resolver.resolve(retType);
        return retType;
    }

    public List<RefTypeOrTPHOrWildcardOrGeneric> getArgTypes(GenericsResolver resolver) {
        List<RefTypeOrTPHOrWildcardOrGeneric> ret = new ArrayList<>();
        for(RefTypeOrTPHOrWildcardOrGeneric param : params){
            if(param instanceof GenericRefType){ //Generics in den Assumptions müssen umgewandelt werden.
                param = resolver.resolve((GenericRefType) param);
            }
            ret.add(param);
        }
        return ret;
    }

    /**
     *
     * @param resolver
     * @return
     */
    public RefTypeOrTPHOrWildcardOrGeneric getReceiverType(GenericsResolver resolver) {
        List<RefTypeOrTPHOrWildcardOrGeneric> params = new ArrayList<>();
        for(GenericTypeVar gtv : receiver.getGenerics()){
            //Die Generics werden alle zu TPHs umgewandelt.
            params.add(resolver.resolve(new GenericRefType(gtv.getName(), new NullToken())));
        }
        RefTypeOrTPHOrWildcardOrGeneric receiverType;
        if(receiver instanceof FunNClass){
            receiverType = new FunN(params);
        }else{
            receiverType = new RefType(receiver.getClassName(), params, new NullToken());
        }

        return receiverType;
    }
}
