package de.dhbwstuttgart.typeinference.assumptions;

import de.dhbwstuttgart.parser.NullToken;
import de.dhbwstuttgart.parser.scope.JavaClassName;
import de.dhbwstuttgart.syntaxtree.ClassOrInterface;
import de.dhbwstuttgart.syntaxtree.GenericDeclarationList;
import de.dhbwstuttgart.syntaxtree.GenericTypeVar;
import de.dhbwstuttgart.syntaxtree.Method;
import de.dhbwstuttgart.syntaxtree.factory.ASTFactory;
import de.dhbwstuttgart.syntaxtree.factory.NameGenerator;
import de.dhbwstuttgart.syntaxtree.type.RefType;
import de.dhbwstuttgart.syntaxtree.type.RefTypeOrTPHOrWildcardOrGeneric;
import org.antlr.v4.runtime.Token;

import java.util.ArrayList;
import java.util.List;

public class FunNClass extends ClassOrInterface {
    public FunNClass(List<RefTypeOrTPHOrWildcardOrGeneric> funNParams) {
        super(0, new JavaClassName("Fun"+(funNParams.size()-1)), new ArrayList<>(),
                createMethods(funNParams), new ArrayList<>(), createGenerics(funNParams),
                ASTFactory.createObjectType(), true, new ArrayList<>(), new NullToken());


        }

    private static GenericDeclarationList createGenerics(List<RefTypeOrTPHOrWildcardOrGeneric> funNParams) {
        List<GenericTypeVar> generics = new ArrayList<>();
        for(RefTypeOrTPHOrWildcardOrGeneric param : funNParams){
            generics.add(new GenericTypeVar(NameGenerator.makeNewName(),
                    new ArrayList<>(), new NullToken(), new NullToken()));
        }
        return new GenericDeclarationList(generics, new NullToken());
    }

    private static List<Method> createMethods(List<RefTypeOrTPHOrWildcardOrGeneric> funNParams) {
        return null;
    }
}
