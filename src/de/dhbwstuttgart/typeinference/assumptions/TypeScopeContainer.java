package de.dhbwstuttgart.typeinference.assumptions;

import com.google.common.collect.Iterables;
import de.dhbwstuttgart.syntaxtree.GenericTypeVar;
import de.dhbwstuttgart.syntaxtree.TypeScope;
import de.dhbwstuttgart.syntaxtree.type.RefTypeOrTPHOrWildcardOrGeneric;

import java.util.ArrayList;
import java.util.Stack;
import java.util.stream.Collectors;

public class TypeScopeContainer implements TypeScope {
    ArrayList<TypeScope> scopes = new ArrayList<>();
    Stack<RefTypeOrTPHOrWildcardOrGeneric> types = new Stack<>();
    public TypeScopeContainer(TypeScope scope1, TypeScope scope2){
        scopes.add(scope1);
        scopes.add(scope2);
        types.push(scope1.getReturnType());
        types.push(scope2.getReturnType());
    }

    @Override
    public Iterable<? extends GenericTypeVar> getGenerics() {
        return Iterables.concat(scopes.stream().
                map(TypeScope::getGenerics).collect(Collectors.toList()).toArray(new Iterable[0]));
    }

    @Override
    public RefTypeOrTPHOrWildcardOrGeneric getReturnType() {
        return types.peek();
    }
}