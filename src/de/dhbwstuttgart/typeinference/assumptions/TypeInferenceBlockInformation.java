package de.dhbwstuttgart.typeinference.assumptions;

import com.google.common.collect.Iterables;
import com.google.common.collect.Iterators;
import de.dhbwstuttgart.syntaxtree.ClassOrInterface;
import de.dhbwstuttgart.syntaxtree.GenericTypeVar;
import de.dhbwstuttgart.syntaxtree.Method;
import de.dhbwstuttgart.syntaxtree.TypeScope;
import de.dhbwstuttgart.syntaxtree.type.RefTypeOrTPHOrWildcardOrGeneric;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Set;
import java.util.Stack;
import java.util.stream.Collectors;

public class TypeInferenceBlockInformation extends TypeInferenceInformation {
    private TypeScope methodContext;
    private ClassOrInterface currentClass;

    public TypeInferenceBlockInformation(Collection<ClassOrInterface> availableClasses,
                                         ClassOrInterface currentClass, TypeScope methodContext) {
        super(availableClasses);
        this.methodContext = new TypeScopeContainer(currentClass, methodContext);
        this.currentClass = currentClass;
    }
    public TypeInferenceBlockInformation(TypeInferenceBlockInformation oldScope, TypeScope newScope) {
        this(oldScope.getAvailableClasses(), oldScope.currentClass, new TypeScopeContainer(oldScope.methodContext, newScope));
    }
    public ClassOrInterface getCurrentClass() {
        return currentClass;
    }
    public TypeScope getCurrentTypeScope() {
        return methodContext;
    }
}
