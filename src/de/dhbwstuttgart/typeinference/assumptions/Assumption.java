package de.dhbwstuttgart.typeinference.assumptions;

import de.dhbwstuttgart.syntaxtree.TypeScope;

public class Assumption {
    private final TypeScope typeScope;

    public Assumption(TypeScope typeScope) {
        this.typeScope = typeScope;
    }

    public TypeScope getTypeScope() {
        return typeScope;
    }
}
