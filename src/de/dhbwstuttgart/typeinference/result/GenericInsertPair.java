package de.dhbwstuttgart.typeinference.result;

import de.dhbwstuttgart.syntaxtree.type.RefTypeOrTPHOrWildcardOrGeneric;
import de.dhbwstuttgart.syntaxtree.type.TypePlaceholder;
import de.dhbwstuttgart.typeinference.constraints.Pair;

public class GenericInsertPair  {
    public TypePlaceholder TA1;
    public TypePlaceholder TA2;

    public GenericInsertPair(TypePlaceholder additionalTPH, TypePlaceholder superType) {
        TA1 = additionalTPH;
        TA2 = superType;
    }

    public GenericInsertPair(Pair pair) {
        TA1 = (TypePlaceholder) pair.TA1;
        TA2 = (TypePlaceholder) pair.TA2;
    }

    public boolean contains(TypePlaceholder additionalTPH) {
        if(TA1.equals(additionalTPH))return true;
        if(TA2.equals(additionalTPH))return true;
        return false;
    }
}
