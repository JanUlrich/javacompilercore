package de.dhbwstuttgart.typeinference.result;

public interface ResultPairVisitor {
    void visit(PairTPHsmallerTPH p);
    void visit(PairTPHequalRefTypeOrWildcardType p);
    void visit(PairTPHEqualTPH p);
}
