package de.dhbwstuttgart.typeinference.result;

import de.dhbwstuttgart.syntaxtree.type.RefTypeOrTPHOrWildcardOrGeneric;
import de.dhbwstuttgart.syntaxtree.type.TypePlaceholder;

/**
 * Steht für: A <. B
 */
public class PairTPHsmallerTPH extends ResultPair{
    public final TypePlaceholder left;
    public final TypePlaceholder right;

    public PairTPHsmallerTPH(TypePlaceholder left, TypePlaceholder right){
        super(left, right);
        this.left = left;
        this.right = right;
    }

    @Override
    public void accept(ResultPairVisitor visitor) {
        visitor.visit(this);
    }
}
