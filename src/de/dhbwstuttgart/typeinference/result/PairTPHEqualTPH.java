package de.dhbwstuttgart.typeinference.result;

import de.dhbwstuttgart.syntaxtree.type.RefTypeOrTPHOrWildcardOrGeneric;
import de.dhbwstuttgart.syntaxtree.type.TypePlaceholder;

public class PairTPHEqualTPH extends ResultPair<TypePlaceholder, TypePlaceholder> {
    public PairTPHEqualTPH(TypePlaceholder tl, TypePlaceholder tr) {
        super(tl, tr);
    }

    @Override
    public void accept(ResultPairVisitor visitor) {
        visitor.visit(this);
    }
}
