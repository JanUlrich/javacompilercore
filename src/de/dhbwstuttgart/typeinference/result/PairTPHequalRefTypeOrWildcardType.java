package de.dhbwstuttgart.typeinference.result;

import de.dhbwstuttgart.syntaxtree.type.RefType;
import de.dhbwstuttgart.syntaxtree.type.RefTypeOrTPHOrWildcardOrGeneric;
import de.dhbwstuttgart.syntaxtree.type.TypePlaceholder;

/**
 * Steht für A =. RefType
 */
public class PairTPHequalRefTypeOrWildcardType extends ResultPair{
    public final TypePlaceholder left;
    public final RefTypeOrTPHOrWildcardOrGeneric right;

    public PairTPHequalRefTypeOrWildcardType(TypePlaceholder left, RefTypeOrTPHOrWildcardOrGeneric right){
        super(left, right);
        this.left = left;
        this.right = right;
    }

    @Override
    public void accept(ResultPairVisitor visitor) {
        visitor.visit(this);
    }
}
