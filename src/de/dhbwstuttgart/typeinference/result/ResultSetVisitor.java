package de.dhbwstuttgart.typeinference.result;

import de.dhbwstuttgart.syntaxtree.type.*;

public interface ResultSetVisitor extends ResultPairVisitor{

    void visit(RefType refType);

    void visit(GenericRefType genericRefType);

    void visit(SuperWildcardType superWildcardType);

    void visit(TypePlaceholder typePlaceholder);

    void visit(ExtendsWildcardType extendsWildcardType);

}
