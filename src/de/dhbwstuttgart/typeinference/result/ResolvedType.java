package de.dhbwstuttgart.typeinference.result;

import de.dhbwstuttgart.syntaxtree.type.GenericRefType;
import de.dhbwstuttgart.syntaxtree.type.RefTypeOrTPHOrWildcardOrGeneric;
import de.dhbwstuttgart.syntaxtree.type.TypePlaceholder;

import java.util.Set;

public class ResolvedType{
    public final RefTypeOrTPHOrWildcardOrGeneric resolvedType;
    public final Set<GenericInsertPair> additionalGenerics;

    public ResolvedType(RefTypeOrTPHOrWildcardOrGeneric resolvedType, Set<GenericInsertPair> additionalGenerics){
        this.resolvedType = resolvedType;
        this.additionalGenerics = additionalGenerics;
    }
}
