package de.dhbwstuttgart.typeinference.result;

import de.dhbwstuttgart.syntaxtree.type.RefTypeOrTPHOrWildcardOrGeneric;

/**
 * Paare, welche das Unifikationsergebnis darstellen
 */
public abstract class ResultPair<A extends RefTypeOrTPHOrWildcardOrGeneric,B extends RefTypeOrTPHOrWildcardOrGeneric> {
    private final A left;
    private final B right;

    public abstract void accept(ResultPairVisitor visitor);

    public ResultPair(A left, B right){
        this.left = left;
        this.right = right;
    }

    public A getLeft() {
        return left;
    }

    public B getRight() {
        return right;
    }
}
