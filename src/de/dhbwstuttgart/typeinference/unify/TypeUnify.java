package de.dhbwstuttgart.typeinference.unify;

import java.util.Set;
import java.util.concurrent.ForkJoinPool;

import de.dhbwstuttgart.typeinference.unify.interfaces.IFiniteClosure;
import de.dhbwstuttgart.typeinference.unify.model.UnifyPair;

public class TypeUnify {
	public Set<Set<UnifyPair>> unify(Set<UnifyPair> eq, IFiniteClosure fc) {
		TypeUnifyTask unifyTask = new TypeUnifyTask(eq, fc, true);
		ForkJoinPool pool = new ForkJoinPool();
		pool.invoke(unifyTask);
		Set<Set<UnifyPair>> res =  unifyTask.join();
		return res;
	}
	
	public Set<Set<UnifyPair>> unifySequential(Set<UnifyPair> eq, IFiniteClosure fc) {
		TypeUnifyTask unifyTask = new TypeUnifyTask(eq, fc, false);
		Set<Set<UnifyPair>> res = unifyTask.compute();
		return res;
	}

}
