package de.dhbwstuttgart.typeinference.unify.interfaces;

import java.util.List;
import java.util.Set;

/**
 * Contains operations on sets.
 * @author Florian Steurer
 */
public interface ISetOperations {
	/**
	 * Calculates the cartesian product of the sets.
	 * @return The cartesian product
	 */
	<B> Set<List<B>> cartesianProduct(List<? extends Set<? extends B>> sets);
}
