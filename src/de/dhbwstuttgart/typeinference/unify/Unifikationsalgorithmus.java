package de.dhbwstuttgart.typeinference.unify;

import java.util.Set;

import de.dhbwstuttgart.typeinference.unify.model.UnifyPair;

public interface Unifikationsalgorithmus {

    public Set<Set<UnifyPair>> apply (Set<UnifyPair> E);

}
