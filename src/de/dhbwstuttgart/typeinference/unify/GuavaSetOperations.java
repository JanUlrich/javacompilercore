package de.dhbwstuttgart.typeinference.unify;

import java.util.List;
import java.util.Set;

import com.google.common.collect.Sets;

import de.dhbwstuttgart.typeinference.unify.interfaces.ISetOperations;

/**
 * Implements set operations using google guava.
 * @author DH10STF
 *
 */
public class GuavaSetOperations implements ISetOperations {

	@Override
	public <B> Set<List<B>> cartesianProduct(List<? extends Set<? extends B>> sets) {
		// Wraps the call to google guava
		return Sets.cartesianProduct(sets);
	}

}
