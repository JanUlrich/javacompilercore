package de.dhbwstuttgart.typeinference.unify.model;

/**
 * Operators of pairs of the unification.
 * @author Florian Steurer
 */
public enum PairOperator {
	/**
	 * The smaller operator (T < P) is used to express a subtyping relation between
	 * T and P for example in the finite closure. It is necessarily true.
	 */
	SMALLER,
	
	/**
	 * The smallerdot operator (T <. P) is used to express a subtyping relation between
	 * of T and P in a CONSTRAINT during the unification. It is not necessarily true. 
	 */
	SMALLERDOT,
	
	/**
	 * The smallerdot operator for arguments (T <.? P) is used to express that 
	 * T is an element of smArg(P) (or P is an element of grArg(T)) in a CONSTRAINT 
	 * during the unification. It is not necessarily true.
	 */
	SMALLERDOTWC,
	
	/**
	 * The equalsdot operator (T =. P) is used to express that two types during the unification
	 * should be equal. It is not necessarily true.
	 */
	EQUALSDOT;
	
	@Override
	public String toString() {
		switch (this) {
			case SMALLER: return "<";
			case SMALLERDOT: return "<.";
			case SMALLERDOTWC: return "<.?";
			default: return "=."; // EQUALSDOT
		}
	}
}