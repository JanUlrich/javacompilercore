package de.dhbwstuttgart.bytecode;

import java.util.HashMap;

import de.dhbwstuttgart.bytecode.descriptor.DescriptorVisitor;
import de.dhbwstuttgart.syntaxtree.statement.ArgumentList;
import de.dhbwstuttgart.syntaxtree.type.RefTypeOrTPHOrWildcardOrGeneric;

public class MethodFromMethodCall {
	private ArgumentList argList;
	private RefTypeOrTPHOrWildcardOrGeneric returnType;
	private HashMap<String, String> genericsAndBoundsMethod;
	private HashMap<String,String> genericsAndBounds;
	
	public MethodFromMethodCall(ArgumentList argList,RefTypeOrTPHOrWildcardOrGeneric returnType,
			HashMap<String, String> genericsAndBoundsMethod,HashMap<String,String> genericsAndBounds) {
		this.argList = argList;
		this.returnType = returnType;
		this.genericsAndBoundsMethod = genericsAndBoundsMethod;
		this.genericsAndBounds = genericsAndBounds;
	}
	
	public ArgumentList getArgList() {
		return argList;
	}

	public RefTypeOrTPHOrWildcardOrGeneric getReturnType() {
		return returnType;
	}
	
	public HashMap<String, String> getGenericsAndBoundsMethod(){
		return genericsAndBoundsMethod;
	}
	
	public HashMap<String,String> getGenericsAndBounds(){
		return genericsAndBounds;
	}
	
	public String accept(DescriptorVisitor descVisitor) {
		return descVisitor.visit(this);
	}
}
