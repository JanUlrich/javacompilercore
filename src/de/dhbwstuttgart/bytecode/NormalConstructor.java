package de.dhbwstuttgart.bytecode;

import java.util.HashMap;

import de.dhbwstuttgart.bytecode.descriptor.DescriptorVisitor;
import de.dhbwstuttgart.syntaxtree.Constructor;
import de.dhbwstuttgart.syntaxtree.ParameterList;

public class NormalConstructor {
	private Constructor constructor;
	private HashMap<String, String> genericsAndBounds;
	private boolean hasGenerics;
	
	public NormalConstructor(Constructor constructor, boolean hasGenerics) {
		this.constructor = constructor;
		this.hasGenerics = hasGenerics;
	}
	
	public NormalConstructor(Constructor constructor, HashMap<String, String> genericsAndBounds, boolean hasGenerics) {
		this.constructor = constructor;
		this.genericsAndBounds = genericsAndBounds;
		this.hasGenerics = hasGenerics;
	}

	public HashMap<String, String> getGenericsAndBounds() {
		return genericsAndBounds;
	}

	public boolean hasGen() {
		return hasGenerics;
	}

	public ParameterList getParameterList() {
		return constructor.getParameterList();
	}
	
	public String accept(DescriptorVisitor descVisitor) {
		return descVisitor.visit(this);
	}
}
