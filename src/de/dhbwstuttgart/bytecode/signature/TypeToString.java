package de.dhbwstuttgart.bytecode.signature;

import de.dhbwstuttgart.exceptions.NotImplementedException;
import de.dhbwstuttgart.syntaxtree.type.ExtendsWildcardType;
import de.dhbwstuttgart.syntaxtree.type.GenericRefType;
import de.dhbwstuttgart.syntaxtree.type.RefType;
import de.dhbwstuttgart.syntaxtree.type.SuperWildcardType;
import de.dhbwstuttgart.syntaxtree.type.TypePlaceholder;
import de.dhbwstuttgart.syntaxtree.type.TypeVisitor;

public class TypeToString implements TypeVisitor<String>{

	@Override
	public String visit(RefType refType) {
		return "RT";
	}

	@Override
	public String visit(SuperWildcardType superWildcardType) {
		throw new NotImplementedException();
	}

	@Override
	public String visit(TypePlaceholder typePlaceholder) {
		return "TPH";
	}

	@Override
	public String visit(ExtendsWildcardType extendsWildcardType) {
		throw new NotImplementedException();
	}

	@Override
	public String visit(GenericRefType genericRefType) {
		return "GRT";
	}

}
