package de.dhbwstuttgart.bytecode.signature;

import java.util.HashMap;
import java.util.Iterator;

import org.objectweb.asm.Type;
import org.objectweb.asm.signature.SignatureVisitor;
import org.objectweb.asm.signature.SignatureWriter;

import de.dhbwstuttgart.bytecode.descriptor.TypeToDescriptor;
import de.dhbwstuttgart.syntaxtree.ClassOrInterface;
import de.dhbwstuttgart.syntaxtree.Constructor;
import de.dhbwstuttgart.syntaxtree.GenericTypeVar;
import de.dhbwstuttgart.syntaxtree.Method;
import de.dhbwstuttgart.syntaxtree.statement.LambdaExpression;
import de.dhbwstuttgart.syntaxtree.type.GenericRefType;
import de.dhbwstuttgart.syntaxtree.type.RefTypeOrTPHOrWildcardOrGeneric;
import de.dhbwstuttgart.typeinference.result.ResultSet;

public class Signature {
	private ClassOrInterface classOrInterface;
	private HashMap<String, String> genericsAndBounds;
	private HashMap<String, String> genericsAndBoundsMethod;
	private SignatureWriter sw;
	private Constructor constructor;
	private Method method;
	private HashMap<String,RefTypeOrTPHOrWildcardOrGeneric> methodParamsAndTypes;
	private ResultSet resultSet;
	
	public Signature(ClassOrInterface classOrInterface, HashMap<String, String> genericsAndBounds) {
		this.classOrInterface = classOrInterface;
		this.genericsAndBounds = genericsAndBounds;
		sw = new SignatureWriter();
		createSignatureForClassOrInterface();
	}

	public Signature(Constructor constructor, HashMap<String, String> genericsAndBounds, HashMap<String,RefTypeOrTPHOrWildcardOrGeneric> methodParamsAndTypes) {
		this.constructor = constructor;
		this.genericsAndBounds = genericsAndBounds;
		this.methodParamsAndTypes = methodParamsAndTypes;
		sw = new SignatureWriter();
		createSignatureForConsOrMethod(this.constructor,true);
	}

	public Signature(Method method, HashMap<String, String> genericsAndBoundsMethod,
			HashMap<String, RefTypeOrTPHOrWildcardOrGeneric> methodParamsAndTypes, ResultSet resultSet) {
		this.method = method;
		this.genericsAndBoundsMethod = genericsAndBoundsMethod;
		this.methodParamsAndTypes = methodParamsAndTypes;
		this.resultSet = resultSet;
		sw = new SignatureWriter();
		createSignatureForConsOrMethod(this.method,false);
	}
	
	public Signature(LambdaExpression lambdaExpression,int numberOfParams) {
		sw = new SignatureWriter();
		createSignatureForFunN(lambdaExpression, numberOfParams);
	}

	private void createSignatureForFunN(LambdaExpression lambdaExpression, int numberOfParams) {
		
		sw.visitFormalTypeParameter("R");
		// getBounds vom Return-Type
		sw.visitClassBound().visitClassType(Type.getInternalName(Object.class));
		sw.visitClassBound().visitEnd();
		for(int i = 0;i<numberOfParams;i++) {
			int j = i+1;
			sw.visitFormalTypeParameter("T"+ j);
			// getBounds von Params
			sw.visitClassBound().visitClassType(Type.getInternalName(Object.class));
			sw.visitClassBound().visitEnd();
		}
		// TODO: prüfe ob Return-Type = void,
		sw.visitSuperclass().visitClassType(Type.getInternalName(Object.class));;
		sw.visitEnd();
	}

	/**
	 * Creates signature for a method or constructor with @see {@link SignatureWriter}
	 * Signature looks like: 
	 * <typevaliables (K:Ljava/lang/Object "Bounds")>(params L.. OR T.. Or basistape)ReturnType
	 * 
	 * @param method method or constructor
	 * @param isConstructor true if constructor
	 */
	private void createSignatureForConsOrMethod(Method method, boolean isConstructor) {
		Iterator<? extends GenericTypeVar> itr = method.getGenerics().iterator();
		// visits all formal type parameter and visits their bounds <T:...;B:...;...>
		while(itr.hasNext()) {
			GenericTypeVar g = itr.next();
			getBoundsOfTypeVar(g,genericsAndBoundsMethod);
		}
		// visits each method-parameter to create the signature 
		for(String paramName : methodParamsAndTypes.keySet()) {
			RefTypeOrTPHOrWildcardOrGeneric t = methodParamsAndTypes.get(paramName);
			// parameter type deswegen ist true
			doVisitParamsOrReturn(t,true);
		}
		if(isConstructor) {
			sw.visitReturnType().visitBaseType('V');
		}else {
			RefTypeOrTPHOrWildcardOrGeneric returnType = method.getReturnType();
			// return type deswegen ist false
			doVisitParamsOrReturn(returnType, false);
		}
//		sw.visitEnd();
	}
	/**
	 * Visits parameter type or return type with {@link SignatureVisitor} to create
	 * the method signature
	 * @param t type of parameter or return type
	 * @param isParameterType true if t is type of parameter
	 */
	private void doVisitParamsOrReturn(RefTypeOrTPHOrWildcardOrGeneric t, boolean isParameterType) {
		String type = t.acceptTV(new TypeToString());
		SignatureVisitor sv;
		if(isParameterType) {
			 sv = sw.visitParameterType();
		}
		else {
			sv = sw.visitReturnType();
		}
		switch (type) {
		case "RT":
			sv.visitClassType(t.acceptTV(new TypeToSignature()));
			break;
		case "GRT":
			GenericRefType g = (GenericRefType) t;
			sv.visitTypeVariable(g.acceptTV(new TypeToSignature()));
			break;
		case "TPH":
			RefTypeOrTPHOrWildcardOrGeneric r = resultSet.resolveType(t).resolvedType;
			if(!r.acceptTV(new TypeToSignature()).substring(0, 4).equals("TPH "))
				sv.visitInterface().visitClassType(r.acceptTV(new TypeToSignature()));
//			sv.visitClassType(r.acceptTV(new TypeToSignature()));
			System.out.println(r.getClass()+" Signature TPH: "+r.acceptTV(new TypeToSignature()));
			break;
		default:
			if(!isParameterType)
				sv.visitBaseType('V');
			break;
		}
	}
	/**
	 * Creates signature for class or interface with {@link SignatureWriter}
	 * Signature looks like: 
	 * <typevaliables (K:Ljava/lang/Object "Bounds")>superclass
	 */
	private void createSignatureForClassOrInterface() {
		Iterator<GenericTypeVar> itr = classOrInterface.getGenerics().iterator();

		while(itr.hasNext()) {
			GenericTypeVar g = itr.next();
			getBoundsOfTypeVar(g,genericsAndBounds);
		}
		
		sw.visitSuperclass().visitClassType(classOrInterface.getSuperClass().acceptTV(new TypeToDescriptor()));;
		sw.visitEnd();
	}
	/**
	 * Get bounds of type variable
	 * @param g type variable
	 * @param genAndBounds 
	 */
	private void getBoundsOfTypeVar(GenericTypeVar g, HashMap<String, String> genAndBounds) {
		sw.visitFormalTypeParameter(g.getName());
		
		Iterator<? extends RefTypeOrTPHOrWildcardOrGeneric> bItr = g.getBounds().iterator(); 
		while(bItr.hasNext()) {
			RefTypeOrTPHOrWildcardOrGeneric b =bItr.next();
			String boundDesc = b.acceptTV(new TypeToDescriptor());
//			System.out.println("GetBounds: " + boundDesc);
			// Ensure that <...> extends java.lang.Object OR ...
			sw.visitClassBound().visitClassType(boundDesc);
			genAndBounds.put(g.getName(), boundDesc);
		}
		sw.visitClassBound().visitEnd();
	}
	
	public String toString() {
		return sw.toString();
	}
	
}
