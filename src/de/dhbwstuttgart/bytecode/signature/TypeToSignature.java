package de.dhbwstuttgart.bytecode.signature;

import java.util.Iterator;

import de.dhbwstuttgart.exceptions.NotImplementedException;
import de.dhbwstuttgart.syntaxtree.type.ExtendsWildcardType;
import de.dhbwstuttgart.syntaxtree.type.GenericRefType;
import de.dhbwstuttgart.syntaxtree.type.RefType;
import de.dhbwstuttgart.syntaxtree.type.RefTypeOrTPHOrWildcardOrGeneric;
import de.dhbwstuttgart.syntaxtree.type.SuperWildcardType;
import de.dhbwstuttgart.syntaxtree.type.TypePlaceholder;
import de.dhbwstuttgart.syntaxtree.type.TypeVisitor;

public class TypeToSignature implements TypeVisitor<String> {

	@Override
	public String visit(RefType refType) {
//		return refType.toString().replace(".", "/");
		String params = "";
        if(refType.getParaList().size()>0){
            params += "<";
            Iterator<RefTypeOrTPHOrWildcardOrGeneric> it = refType.getParaList().iterator();
            while(it.hasNext()){
                RefTypeOrTPHOrWildcardOrGeneric param = it.next();
                params += "L"+param.toString().replace(".", "/");
                if(it.hasNext())params += ";";
            }
            params += ";>";
        }
        return refType.getName().toString().replace(".", "/") + params+";";
	}

	@Override
	public String visit(SuperWildcardType superWildcardType) {
		throw new NotImplementedException();
	}

	@Override
	public String visit(TypePlaceholder typePlaceholder) {
		return typePlaceholder.toString().replace(".", "/");
	}

	@Override
	public String visit(ExtendsWildcardType extendsWildcardType) {
		throw new NotImplementedException();
	}

	@Override
	public String visit(GenericRefType genericRefType) {
		return genericRefType.getParsedName().replace(".", "/");
	}
	
}
