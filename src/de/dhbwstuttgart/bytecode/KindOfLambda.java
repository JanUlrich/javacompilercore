package de.dhbwstuttgart.bytecode;

import de.dhbwstuttgart.exceptions.NotImplementedException;
import de.dhbwstuttgart.syntaxtree.statement.*;

import java.util.ArrayList;
import java.util.List;

import de.dhbwstuttgart.parser.SyntaxTreeGenerator.AssignToLocal;
import de.dhbwstuttgart.syntaxtree.StatementVisitor;
import de.dhbwstuttgart.syntaxtree.statement.Literal;
import de.dhbwstuttgart.syntaxtree.type.RefTypeOrTPHOrWildcardOrGeneric;

public class KindOfLambda implements StatementVisitor{
	private boolean isInstanceCapturingLambda = false;
	private List<RefTypeOrTPHOrWildcardOrGeneric> argumentList = new ArrayList<>();
	
	public KindOfLambda(LambdaExpression lambdaExpression) {
		lambdaExpression.methodBody.accept(this);
		
	}
	
	public boolean isInstanceCapturingLambda() {
		return this.isInstanceCapturingLambda;
	}
	
	public List<RefTypeOrTPHOrWildcardOrGeneric> getArgumentList() {
		return argumentList;
	}
	
	@Override
	public void visit(ArgumentList argumentList) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void visit(LambdaExpression lambdaExpression) {
		
	}

	@Override
	public void visit(Assign assign) {
		assign.rightSide.accept(this);
	}

	@Override
	public void visit(BinaryExpr binary) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void visit(Block block) {
		for(Statement stmt : block.getStatements()) {
			stmt.accept(this);
		}
	}

	@Override
	public void visit(CastExpr castExpr) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void visit(EmptyStmt emptyStmt) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void visit(FieldVar fieldVar) {
		fieldVar.receiver.accept(this);
	}

	@Override
	public void visit(ForStmt forStmt) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void visit(IfStmt ifStmt) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void visit(InstanceOf instanceOf) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void visit(LocalVar localVar) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void visit(LocalVarDecl localVarDecl) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void visit(MethodCall methodCall) {
		methodCall.receiver.accept(this);
	}

	@Override
	public void visit(NewClass methodCall) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void visit(NewArray newArray) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void visit(ExpressionReceiver receiver) {
		receiver.expr.accept(this);
	}

	@Override
	public void visit(UnaryExpr unaryExpr) {
		throw new NotImplementedException();
	}

	@Override
	public void visit(Return aReturn) {
		aReturn.retexpr.accept(this);
	}

	@Override
	public void visit(ReturnVoid aReturn) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void visit(StaticClassName staticClassName) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void visit(Super aSuper) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void visit(This aThis) {
		this.isInstanceCapturingLambda = true;
		this.argumentList.add(aThis.getType());
	}

	@Override
	public void visit(WhileStmt whileStmt) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void visit(DoStmt whileStmt) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void visit(Literal literal) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void visit(AssignToField assignLeftSide) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void visit(AssignToLocal assignLeftSide) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void visit(SuperCall superCall) {
		// TODO Auto-generated method stub
		
	}
}
