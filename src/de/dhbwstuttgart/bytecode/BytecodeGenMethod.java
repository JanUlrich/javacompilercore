package de.dhbwstuttgart.bytecode;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.lang.invoke.CallSite;
import java.lang.invoke.MethodHandle;
import java.lang.invoke.MethodHandles;
import java.lang.invoke.MethodType;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

import de.dhbwstuttgart.exceptions.NotImplementedException;
import de.dhbwstuttgart.syntaxtree.statement.*;
import org.objectweb.asm.ClassWriter;
import org.objectweb.asm.Handle;
import org.objectweb.asm.MethodVisitor;
import org.objectweb.asm.Opcodes;
import org.objectweb.asm.Type;
import org.objectweb.asm.signature.SignatureVisitor;
import org.objectweb.asm.signature.SignatureWriter;

import de.dhbwstuttgart.bytecode.descriptor.DescriptorToString;
import de.dhbwstuttgart.bytecode.descriptor.TypeToDescriptor;
import de.dhbwstuttgart.bytecode.signature.Signature;
import de.dhbwstuttgart.bytecode.signature.TypeToSignature;
import de.dhbwstuttgart.bytecode.signature.TypeToString;
import de.dhbwstuttgart.parser.SyntaxTreeGenerator.AssignToLocal;
import de.dhbwstuttgart.syntaxtree.FormalParameter;
import de.dhbwstuttgart.syntaxtree.Method;
import de.dhbwstuttgart.syntaxtree.StatementVisitor;

import de.dhbwstuttgart.syntaxtree.statement.Literal;

import de.dhbwstuttgart.syntaxtree.type.RefTypeOrTPHOrWildcardOrGeneric;
import de.dhbwstuttgart.typeinference.result.ResultSet;

public class BytecodeGenMethod implements StatementVisitor{
	
	private Method m;
	private MethodVisitor mv;
	private HashMap<String, Integer> paramsAndLocals = new HashMap<>(); 
	private String className;
	private int lamCounter = -1;
	private ClassWriter cw;
	private ResultSet resultSet;
	private boolean isInterface;
	HashMap<String, String> genericsAndBoundsMethod;
	private HashMap<String,String> genericsAndBounds;
	private boolean isBinaryExp = false;
	
	//for tests **
	private String fieldName;
	private String fieldDesc;
	private Expression rightSideTemp;
	private boolean isRightSideALambda = false;
	private KindOfLambda kindOfLambda;
	private HashMap<String, byte[]> classFiles;
	
	private ArrayList<RefTypeOrTPHOrWildcardOrGeneric> varsFunInterface = new ArrayList<>();;
	
	public BytecodeGenMethod(String className,ResultSet resultSet, Method m, MethodVisitor mv, 
			HashMap<String, Integer> paramsAndLocals, ClassWriter cw, HashMap<String, String> genericsAndBoundsMethod, 
			HashMap<String,String> genericsAndBounds, boolean isInterface, HashMap<String, byte[]> classFiles) {
		
		this.className = className;
		this.resultSet = resultSet;
		this.m = m;
		this.mv = mv;
		this.paramsAndLocals = paramsAndLocals;
		this.cw = cw;
		this.genericsAndBoundsMethod = genericsAndBoundsMethod;
		this.genericsAndBounds = genericsAndBounds;
		this.isInterface = isInterface;
		this.classFiles = classFiles;
		
		if(!isInterface)
			this.m.block.accept(this);
		
	}
	
	public BytecodeGenMethod(LambdaExpression lambdaExpression,ResultSet resultSet ,MethodVisitor mv, 
			 int indexOfFirstParamLam, boolean isInterface, HashMap<String, byte[]> classFiles) {
		
		this.resultSet = resultSet;
		this.mv = mv;
		this.isInterface = isInterface;
		this.classFiles = classFiles;

		Iterator<FormalParameter> itr = lambdaExpression.params.iterator();
		int i = indexOfFirstParamLam;
		while(itr.hasNext()) {
			FormalParameter fp = itr.next();
			this.paramsAndLocals.put(fp.getName(), i);
			i++;
		}
		lambdaExpression.methodBody.accept(this);
	}
	
	private String getResolvedType(RefTypeOrTPHOrWildcardOrGeneric type) {
		return resultSet.resolveType(type).resolvedType.acceptTV(new TypeToDescriptor());
	}
	
	
	@Override
	public void visit(Block block) {
		for(Statement stmt : block.getStatements()) {
//			System.out.println(where);
			stmt.accept(this);
		}
	}
	
	@Override
	public void visit(SuperCall superCall) {
		superCall.receiver.accept(this);
		superCall.arglist.accept(this);
		mv.visitMethodInsn(Opcodes.INVOKESPECIAL, Type.getInternalName(Object.class), superCall.name, "()V",isInterface);
	}
	
	// ??
	@Override
	public void visit(LocalVar localVar) {
		mv.visitVarInsn(Opcodes.ALOAD, paramsAndLocals.get(localVar.name));
		if(isBinaryExp) {
			getVlaue(getResolvedType(localVar.getType()));
		}
	}
	// ??
	@Override
	public void visit(LocalVarDecl localVarDecl) {

	}

	@Override
	public void visit(Assign assign) {
		// if the right side is a lambda => the left side must be a functional interface
		if(assign.rightSide instanceof LambdaExpression) {
			isRightSideALambda = true;
		}else {
			isRightSideALambda = false;
		}
		
		if(assign.rightSide instanceof BinaryExpr)
			isBinaryExp = true;
		
		if(assign.lefSide instanceof AssignToField) {
			// load_0, ldc or .. then putfield
			this.rightSideTemp = assign.rightSide;
		}else {
			assign.rightSide.accept(this);
		}
		if(isBinaryExp) {
			doAssign(getResolvedType(assign.lefSide.getType()));
			isBinaryExp = false;
		}
		assign.lefSide.accept(this);
	}
	
	@Override
	public void visit(BinaryExpr binary) {
		binary.lexpr.accept(this);
		binary.rexpr.accept(this);
		switch (binary.operation.toString()) {
		case "ADD":
			mv.visitInsn(Opcodes.IADD);
			break;

		default:
			break;
		}
		
	}
	
	@Override
	public void visit(LambdaExpression lambdaExpression) {
		this.lamCounter++;
		
		Lambda lam = new Lambda(lambdaExpression);
		String lamDesc = lam.accept(new DescriptorToString(resultSet));
		//Call site, which, when invoked, returns an instance of the functional interface to which
		//the lambda is being converted
		MethodType mt = MethodType.methodType(CallSite.class, MethodHandles.Lookup.class, String.class,
						MethodType.class, MethodType.class, MethodHandle.class, MethodType.class);
				

		Handle bootstrap = new Handle(Opcodes.H_INVOKESTATIC, "java/lang/invoke/LambdaMetafactory", 
				"metafactory", mt.toMethodDescriptorString(), false);
		String methodName = "lambda$new$" + this.lamCounter;
		
		// Für die Parameter-Typen und Return-Typ braucht man die Bounds (für die Typlöschung)
		
		String typeErasure = "(";
		Iterator<FormalParameter> itr = lambdaExpression.params.iterator();
		while(itr.hasNext()) {
			itr.next();
			typeErasure += "L"+Type.getInternalName(Object.class) + ";";
		}
		
		typeErasure += ")L"+Type.getInternalName(Object.class) + ";";
		// Type erasure
		Type arg1 = Type.getMethodType(typeErasure);
//		Type arg1 = Type.getMethodType(lamDesc);
		// real Type
		Type arg3 = Type.getMethodType(lamDesc);
		
		int staticOrSpecial=0;
		int staticOrInstance=0;
		int indexOfFirstParamLam = 0;
		this.kindOfLambda = new KindOfLambda(lambdaExpression);

		if(kindOfLambda.isInstanceCapturingLambda()) {
			mv.visitVarInsn(Opcodes.ALOAD, 0);
			staticOrSpecial = Opcodes.H_INVOKESPECIAL;
			indexOfFirstParamLam = 1;
		}else {
			staticOrSpecial = Opcodes.H_INVOKESTATIC;
			staticOrInstance = Opcodes.ACC_STATIC;
		}
		
		// first check if capturing lambda then invokestatic or invokespecial
		Handle arg2 = new Handle(staticOrSpecial, this.className, methodName, 
				arg3.toString(),false);
		// Descriptor of functional interface methode
		SamMethod samMethod = new SamMethod(kindOfLambda.getArgumentList(), lambdaExpression.getType());
		// Desc: (this/nothing)TargetType
		String fiMethodDesc = samMethod.accept(new DescriptorToString(resultSet));
		mv.visitInvokeDynamicInsn("apply", fiMethodDesc, bootstrap, arg1, arg2,arg3);
		
		MethodVisitor mvLambdaBody = cw.visitMethod(Opcodes.ACC_PRIVATE+ staticOrInstance + Opcodes.ACC_SYNTHETIC, 
				methodName, arg3.toString(), null, null);

		new BytecodeGenMethod(lambdaExpression,this.resultSet,mvLambdaBody,indexOfFirstParamLam,isInterface,
				classFiles);
		
		mvLambdaBody.visitMaxs(0, 0);
		mvLambdaBody.visitEnd();
		cw.visitInnerClass("java/lang/invoke/MethodHandles$Lookup", "java/lang/invoke/MethodHandles", "Lookup",
				Opcodes.ACC_PUBLIC + Opcodes.ACC_STATIC + Opcodes.ACC_FINAL);
		
//		generateBCForFunN(lambdaExpression,typeErasure);
	}

	private void generateBCForFunN(LambdaExpression lambdaExpression, String methDesc) {
		ClassWriter classWriter =new ClassWriter(ClassWriter.COMPUTE_FRAMES|ClassWriter.COMPUTE_MAXS);
		
		SignatureWriter methSig = new SignatureWriter();
		
		int numberOfParams = 0;
		SignatureVisitor paramVisitor = methSig.visitParameterType();
		Iterator<FormalParameter> itr = lambdaExpression.params.iterator();
		while(itr.hasNext()) {
			numberOfParams++;
			// getBounds
			paramVisitor.visitTypeVariable("T"+numberOfParams);
			itr.next();
		}
		methSig.visitReturnType().visitTypeVariable("R");
		// ")"+lam.getReturn.getBounds
		Signature sig = new Signature(lambdaExpression,numberOfParams);
		String name = "Fun"+numberOfParams;
		classWriter.visit(Opcodes.V1_8, Opcodes.ACC_INTERFACE+Opcodes.ACC_ABSTRACT, name, 
				sig.toString(), Type.getInternalName(Object.class), null);
		MethodVisitor mvApply = classWriter.visitMethod(Opcodes.ACC_PUBLIC+Opcodes.ACC_ABSTRACT, "apply", 
				methDesc, methSig.toString(), null);
		mvApply.visitEnd();
		writeClassFile(classWriter.toByteArray(),name);
	}
	
	public void writeClassFile(byte[] bytecode, String name) {
		FileOutputStream output;
		try {
			System.out.println("generating "+name+ ".class file...");
			output = new FileOutputStream(new File(System.getProperty("user.dir") + "/testBytecode/generatedBC/examples/" +name+".class"));				
			output.write(bytecode);
			output.close();
			System.out.println(name+".class file generated");
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
}

	@Override
	public void visit(CastExpr castExpr) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void visit(EmptyStmt emptyStmt) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void visit(FieldVar fieldVar) {
		
		fieldName = fieldVar.fieldVarName;
		fieldDesc = "L"+getResolvedType(fieldVar.getType())+";";
		
		fieldVar.receiver.accept(this);
		// test (if)
		if(!fieldVar.receiver.getClass().equals(StaticClassName.class)) {
			mv.visitFieldInsn(Opcodes.GETFIELD,getResolvedType(fieldVar.receiver.getType()),
					fieldName ,fieldDesc);
		}
		
//		mv.visitFieldInsn(Opcodes.GETSTATIC, fieldVar.receiver.getType().toString().replace(".", "/"), 
//				fieldVar.fieldVarName, fieldVar.getType().toString());
	}

	@Override
	public void visit(ForStmt forStmt) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void visit(IfStmt ifStmt) {
		System.out.println("If");
	}

	@Override
	public void visit(InstanceOf instanceOf) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void visit(MethodCall methodCall) {
		methodCall.receiver.accept(this);
		methodCall.arglist.accept(this);
		
		MethodFromMethodCall method = new MethodFromMethodCall(methodCall.arglist, methodCall.getType(),
				genericsAndBoundsMethod,genericsAndBounds);
		String mDesc = method.accept(new DescriptorToString(resultSet));
		
		// is methodCall.receiver functional Interface)?
		if(varsFunInterface.contains(methodCall.receiver.getType())) {
			mv.visitMethodInsn(Opcodes.INVOKEINTERFACE, getResolvedType(methodCall.receiver.getType()), 
					methodCall.name, mDesc, false);
		}else {
			mv.visitMethodInsn(Opcodes.INVOKEVIRTUAL, getResolvedType(methodCall.receiver.getType()),
					methodCall.name, mDesc, isInterface);
		}
		// test
//		if(!methodCall.getType().toString().equals("V")) {
//			mv.visitInsn(Opcodes.POP);
//		}
	}

	@Override
	public void visit(NewClass methodCall) {
		
		mv.visitTypeInsn(Opcodes.NEW, methodCall.name.replace(".", "/"));
		mv.visitInsn(Opcodes.DUP);
		// creates Descriptor
		methodCall.arglist.accept(this);
		String d = "(";
		for(Expression e : methodCall.arglist.getArguments()) {
			d = d + "L"+getResolvedType(e.getType()) + ";";
		}
		d += ")V";
		
		mv.visitMethodInsn(Opcodes.INVOKESPECIAL, methodCall.name.replace(".", "/"), "<init>", d, isInterface);
	}

	@Override
	public void visit(NewArray newArray) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void visit(ExpressionReceiver receiver) {
		receiver.expr.accept(this);
	}

	@Override
	public void visit(UnaryExpr unaryExpr) {
		System.out.println(unaryExpr.operation.toString());
	}

	@Override
	public void visit(Return aReturn) {
		aReturn.retexpr.accept(this);
		mv.visitInsn(Opcodes.ARETURN);
	}

	@Override
	public void visit(ReturnVoid aReturn) {
		mv.visitInsn(Opcodes.RETURN);
	}

	@Override
	public void visit(StaticClassName staticClassName) {
//		mv.visitMethodInsn(Opcodes.INVOKESTATIC, staticClassName.getType().toString().replace(".", "/"), 
//				staticClassName.toString(), staticClassName.getType().toString(), false);
		mv.visitFieldInsn(Opcodes.GETSTATIC, getResolvedType(staticClassName.getType()), 
				fieldName, fieldDesc);
	}

	@Override
	public void visit(Super aSuper) {

	}

	@Override
	public void visit(This aThis) {
		mv.visitVarInsn(Opcodes.ALOAD, 0);
	}

	@Override
	public void visit(WhileStmt whileStmt) {
		whileStmt.expr.accept(this);
		whileStmt.loopBlock.accept(this);
	}

	@Override
	public void visit(DoStmt whileStmt) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void visit(Literal literal) {
		Object value = literal.value;
		String typeOfLiteral = resultSet.resolveType(literal.getType()).resolvedType.acceptTV(new TypeToDescriptor());
		if(this.isBinaryExp) {
			getVlaue(typeOfLiteral);
		}else {
			doAssign(typeOfLiteral, value);
		}
		
		
	}
	
	private void getVlaue(String typeOfLiteral) {
		switch (typeOfLiteral) {
		case "java/lang/String":
			break;
		case "java/lang/Boolean":
			break;
		case "java/lang/Byte":
			mv.visitMethodInsn(Opcodes.INVOKESTATIC, "java/lang/Byte", "valueOf", 
					"(B)Ljava/lang/Byte;", false);
			break;
		case "java/lang/Short":
			mv.visitMethodInsn(Opcodes.INVOKESTATIC, "java/lang/Short", "valueOf", 
					"(S)Ljava/lang/Short;", false);
			break;
		case "java/lang/Integer":
			mv.visitMethodInsn(Opcodes.INVOKEVIRTUAL, "java/lang/Integer", "intValue", 
					"()I", false);
			break;
		case "java/lang/Long":
			mv.visitMethodInsn(Opcodes.INVOKESTATIC, "java/lang/Long", "valueOf", 
					"(J)Ljava/lang/Long;", false);
			break;
		case "java/lang/Float":
			break;
		case "java/lang/Double":
			break;
		case "java/lang/Character":
			break;
		default:
			break;
		}
	}

	private void doAssign(String type, Object value) {
		switch (type) {
		case "java/lang/String":
			mv.visitLdcInsn(String.valueOf(value));
			break;
		case "java/lang/Boolean":
			visitBooleanLiteral((Boolean) value);
			break;
		case "java/lang/Byte":
			visitByteLiteral(((Double) value).byteValue(),false);
			mv.visitMethodInsn(Opcodes.INVOKESTATIC, "java/lang/Byte", "valueOf", 
					"(B)Ljava/lang/Byte;", false);
			break;
		case "java/lang/Short":
			visitShortLiteral(((Double) value).shortValue(),false);
			mv.visitMethodInsn(Opcodes.INVOKESTATIC, "java/lang/Short", "valueOf", 
					"(S)Ljava/lang/Short;", false);
			break;
		case "java/lang/Integer":
			//zweite Argument isLong
			visitIntegerLiteral(((Double) value).intValue(), false);
			mv.visitMethodInsn(Opcodes.INVOKESTATIC, "java/lang/Integer", "valueOf", 
					"(I)Ljava/lang/Integer;", false);
			break;
		case "java/lang/Long":
			visitLongLiteral(((Double) value).longValue(), true);
			mv.visitMethodInsn(Opcodes.INVOKESTATIC, "java/lang/Long", "valueOf", 
					"(J)Ljava/lang/Long;", false);
			break;
		case "java/lang/Float":
			visitFloatLiteral(((Double) value).floatValue());
			break;
		case "java/lang/Double":
			visitDoubleLiteral((Double) value);
			break;
		case "java/lang/Character":
			visitCharLiteral((Character) value);
			break;
		default:
			break;
		}
	}
	
	private void doAssign(String type) {
		switch (type) {
		case "java/lang/String":
			break;
		case "java/lang/Boolean":
			break;
		case "java/lang/Byte":
			mv.visitMethodInsn(Opcodes.INVOKESTATIC, "java/lang/Byte", "valueOf", 
					"(B)Ljava/lang/Byte;", false);
			break;
		case "java/lang/Short":
			mv.visitMethodInsn(Opcodes.INVOKESTATIC, "java/lang/Short", "valueOf", 
					"(S)Ljava/lang/Short;", false);
			break;
		case "java/lang/Integer":
			mv.visitMethodInsn(Opcodes.INVOKESTATIC, "java/lang/Integer", "valueOf", 
					"(I)Ljava/lang/Integer;", false);
			break;
		case "java/lang/Long":
			mv.visitMethodInsn(Opcodes.INVOKESTATIC, "java/lang/Long", "valueOf", 
					"(J)Ljava/lang/Long;", false);
			break;
		case "java/lang/Float":
			break;
		case "java/lang/Double":
			break;
		case "java/lang/Character":
			break;
		default:
			break;
		}
	}

	private void visitCharLiteral(Character value) {
		mv.visitIntInsn(Opcodes.BIPUSH, (int) value);
		mv.visitMethodInsn(Opcodes.INVOKESTATIC, "java/lang/Character", "valueOf", 
				"(C)Ljava/lang/Character;", false);
	}

	private void visitDoubleLiteral(Double value) {
		if(value == 0) {
			mv.visitInsn(Opcodes.DCONST_0);
		}else if(value == 1) {
			mv.visitInsn(Opcodes.DCONST_1);
		}else {
			mv.visitLdcInsn(value);
		}
		mv.visitMethodInsn(Opcodes.INVOKESTATIC, "java/lang/Double", "valueOf", 
				"(D)Ljava/lang/Double;", false);
	}

	private void visitFloatLiteral(Float value) {
		if(value.intValue()>-1 && value.intValue() < 3) {
			//Opcodes.FCONST_0 = 11, Opcodes.FCONST_1 = 12, usw
			mv.visitInsn(value.intValue()+11);
		}else {
			mv.visitLdcInsn(value);
		}
		mv.visitMethodInsn(Opcodes.INVOKESTATIC, "java/lang/Float", "valueOf", 
				"(F)Ljava/lang/Float;", false);
	}

	private void visitLongLiteral(Long value, boolean isLong) {
		if(value<Math.pow(2, 15) || (value>=-Math.pow(2, 15))&&value<-128) {
			visitShortLiteral(value.shortValue(),isLong);
		}else {
			mv.visitLdcInsn(value);
		}
	}

	private void visitShortLiteral(Short value,boolean isLong) {
		if(value< 128 || (value>-129 && value<-1)) {
			visitByteLiteral(value.byteValue(), isLong);
		}else if(value<Math.pow(2, 15) || (value>=-Math.pow(2, 15))&&value<-128) {
			mv.visitIntInsn(Opcodes.SIPUSH, value);
		}
	}

	private void visitByteLiteral(Byte value, boolean isLong) {
		
		if(!isLong && value<6 && value>-1) {
			//Opcodes.ICONST_0 = 3, Opcodes.ICONST_1 = 4, usw
			mv.visitInsn(value+3);
		}else if(isLong && value>-1 && value<2){
			//Opcodes.LCONST_0 = 9, Opcodes.LCONST_1 = 10
			mv.visitInsn(value+9);
		}else {
			mv.visitIntInsn(Opcodes.BIPUSH, value);
		}
		
	}

	private void visitIntegerLiteral(Integer value, boolean isLong) {
		
		if(value<Math.pow(2, 15) || (value>=-Math.pow(2, 15))&&value<-128) {
			visitShortLiteral(value.shortValue(),isLong);
		}else {
			mv.visitLdcInsn(value);
		}
	}

	private void visitBooleanLiteral(Boolean b) {
		if(b) {
			mv.visitInsn(Opcodes.ICONST_1);
		}else {
			mv.visitInsn(Opcodes.ICONST_0);
		}
		mv.visitMethodInsn(Opcodes.INVOKESTATIC, "java/lang/Boolean", "valueOf", 
				"(Z)Ljava/lang/Boolean;", false);	
	}

	@Override
	public void visit(ArgumentList argumentList) {
		for(Expression al : argumentList.getArguments()) {
			al.accept(this);
		}
	}

	@Override
	public void visit(AssignToField assignLeftSide) {
//		temporäre Lösung für testen, bis ich weiss wie man funktionale
//		interfaces erkennt
		if(isRightSideALambda)
			varsFunInterface.add(assignLeftSide.field.getType());
		// Loads the an object reference from the local variable 
		//			array slot onto the top of the operand stack.
		assignLeftSide.field.receiver.accept(this);
		this.rightSideTemp.accept(this);
		mv.visitFieldInsn(Opcodes.PUTFIELD, getResolvedType(assignLeftSide.field.receiver.getType()), 
				assignLeftSide.field.fieldVarName, getResolvedType(assignLeftSide.field.getType()));
	}

	@Override
	public void visit(AssignToLocal assignLeftSide) {
		if(isRightSideALambda)
			varsFunInterface.add(assignLeftSide.localVar.getType());
		paramsAndLocals.put(assignLeftSide.localVar.name, paramsAndLocals.size()+1);
		mv.visitVarInsn(Opcodes.ASTORE, paramsAndLocals.size());
		// Debug:::
		
	}

}
