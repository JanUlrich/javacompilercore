package de.dhbwstuttgart.bytecode;

import java.util.HashMap;
import java.util.Iterator;

import de.dhbwstuttgart.exceptions.NotImplementedException;
import de.dhbwstuttgart.syntaxtree.statement.*;
import org.objectweb.asm.ClassWriter;
import org.objectweb.asm.FieldVisitor;
import org.objectweb.asm.MethodVisitor;
import org.objectweb.asm.Opcodes;

import de.dhbwstuttgart.bytecode.descriptor.DescriptorToString;
import de.dhbwstuttgart.bytecode.descriptor.TypeToDescriptor;
import de.dhbwstuttgart.bytecode.signature.Signature;
import de.dhbwstuttgart.bytecode.signature.TypeToString;
import de.dhbwstuttgart.parser.SyntaxTreeGenerator.AssignToLocal;
import de.dhbwstuttgart.syntaxtree.*;
import de.dhbwstuttgart.syntaxtree.statement.Literal;
import de.dhbwstuttgart.syntaxtree.type.ExtendsWildcardType;
import de.dhbwstuttgart.syntaxtree.type.GenericRefType;
import de.dhbwstuttgart.syntaxtree.type.RefType;
import de.dhbwstuttgart.syntaxtree.type.RefTypeOrTPHOrWildcardOrGeneric;
import de.dhbwstuttgart.syntaxtree.type.SuperWildcardType;
import de.dhbwstuttgart.syntaxtree.type.TypePlaceholder;
import de.dhbwstuttgart.typeinference.result.ResultSet;

public class BytecodeGen implements ASTVisitor {
	
	ClassWriter cw =new ClassWriter(ClassWriter.COMPUTE_FRAMES|ClassWriter.COMPUTE_MAXS);
	
	String type;
	
	String className;
	private boolean isInterface;
	private ResultSet resultSet;
	private int indexOfFirstParam = 0;
	
	// stores parameter, local vars and the next index on the local variable table, which use for aload_i, astore_i,...
	HashMap<String, Integer> paramsAndLocals = new HashMap<>();
	// stores generics and their bounds of class
	HashMap<String, String> genericsAndBounds = new HashMap<>();
	// stores generics and their bounds of method
	HashMap<String, String> genericsAndBoundsMethod = new HashMap<>();
	
	HashMap<String,RefTypeOrTPHOrWildcardOrGeneric> methodParamsAndTypes = new HashMap<>();
	byte[] bytecode;
	HashMap<String,byte[]> classFiles;
	
	public BytecodeGen(HashMap<String,byte[]> classFiles, ResultSet resultSet) {
		this.classFiles = classFiles;
		this.resultSet = resultSet;
	}
	
	@Override
	public void visit(SourceFile sourceFile) {
		for(ClassOrInterface cl : sourceFile.getClasses()) {
			System.out.println("in Class: " + cl.getClassName().toString());
			BytecodeGen classGen = new BytecodeGen(classFiles, resultSet);
			cl.accept(classGen);
			classGen.writeClass(cl.getClassName().toString());
		}
	}
	
	/**
	 * Associates the bytecode of the class that was build with the classWriter {@link #cw}
	 * with the class name in the map {@link #classFiles}
	 * 
	 * @param name name of the class with which the the bytecode is to be associated
	 */
	private void writeClass(String name) {
		bytecode = cw.toByteArray();
		classFiles.put(name, bytecode);
		
	}
	
	public HashMap<String,byte[]> getClassFiles() {
		return classFiles;
	}
	
	
	@Override
	public void visit(ClassOrInterface classOrInterface) {
		className = classOrInterface.getClassName().toString();
		
		cw.visitSource(className +".jav", null);
		
		isInterface = (classOrInterface.getModifiers()&512)==512;

		int acc = isInterface?classOrInterface.getModifiers()+Opcodes.ACC_ABSTRACT:classOrInterface.getModifiers()+Opcodes.ACC_SUPER;
		String sig = null;
		/* if class has generics then creates signature
		 * Signature looks like:
		 * <E:Ljava/...>Superclass 
		 */
		if(classOrInterface.getGenerics().iterator().hasNext()) {
			Signature signature = new Signature(classOrInterface, genericsAndBounds);
			sig = signature.toString();
		}
		// needs implemented Interfaces?
		cw.visit(Opcodes.V1_8, acc, classOrInterface.getClassName().toString()
				, sig, classOrInterface.getSuperClass().acceptTV(new TypeToDescriptor()), null);
		
		// for each field in the class
		for(Field f : classOrInterface.getFieldDecl()) {
			f.accept(this);
		}
		
		for(Constructor c : classOrInterface.getConstructors()) {
			c.accept(this);
		}
		
		for(Method m : classOrInterface.getMethods()) {
			m.accept(this);
		}
	}

	@Override
	public void visit(Constructor field) {
		field.getParameterList().accept(this);
		
		String desc = null;
		boolean hasGen = false;
		
		for(String paramName : methodParamsAndTypes.keySet()) {
			String typeOfParam = methodParamsAndTypes.get(paramName).acceptTV(new TypeToDescriptor());
			if(genericsAndBounds.containsKey(typeOfParam)) {
				hasGen = true;
				break;
			}
		}
		String sig = null;
		if(hasGen) {
			Signature signature = new Signature(field, genericsAndBounds,methodParamsAndTypes);
			sig = signature.toString();
		}
		NormalConstructor constructor = new NormalConstructor(field,genericsAndBounds,hasGen);
		desc = constructor.accept(new DescriptorToString(resultSet));
		MethodVisitor mv = cw.visitMethod(Opcodes.ACC_PUBLIC, "<init>", desc, sig, null);
		mv.visitCode();		
		BytecodeGenMethod gen = new BytecodeGenMethod(className,resultSet,field, mv,paramsAndLocals,cw,
				genericsAndBoundsMethod,genericsAndBounds,isInterface,classFiles);
		if(!field.getParameterList().iterator().hasNext()) {
			mv.visitInsn(Opcodes.RETURN);
		}
		mv.visitMaxs(0, 0);
		mv.visitEnd();
	}

	@Override
	public void visit(Method method) {
		// TODO: check if the method is static => if static then the first param will be stored in pos 0
		// else it will be stored in pos 1 and this will be stored in pos 0
		method.getParameterList().accept(this);

		String methDesc = null;
		
		// Method getModifiers() ?
		int acc = isInterface?Opcodes.ACC_ABSTRACT:method.modifier;
		
		boolean hasGenInParameterList = genericsAndBounds.containsKey(resultSet.resolveType(method.getReturnType()).resolvedType.acceptTV(new TypeToDescriptor()));
		if(!hasGenInParameterList) {
			for(String paramName : methodParamsAndTypes.keySet()) {
				String typeOfParam = methodParamsAndTypes.get(paramName).acceptTV(new TypeToDescriptor());
				if(genericsAndBounds.containsKey(typeOfParam)) {
					hasGenInParameterList = true;
					break;
				}
			}
		}
		
		//TODO: Test if the return-type or any of the parameter is a parameterized type. (VP)
		//than create the descriptor with the new syntax.
		
		String sig = null;
		boolean hasGen = method.getGenerics().iterator().hasNext() || hasGenInParameterList;
		
		/* if method has generics or return type is TPH, create signature */
		if(hasGen||method.getReturnType().acceptTV(new TypeToString()).equals("TPH")) {
			// resultset hier zum testen 
			Signature signature = new Signature(method, genericsAndBoundsMethod, methodParamsAndTypes,resultSet);
			sig = signature.toString();
		}
		System.out.println(sig);
		NormalMethod meth = new NormalMethod(method,genericsAndBounds,genericsAndBoundsMethod,hasGen);
		methDesc = meth.accept(new DescriptorToString(resultSet));
		System.out.println(methDesc);
		MethodVisitor mv = cw.visitMethod(Opcodes.ACC_PUBLIC+acc, method.getName(), methDesc, sig, null);

		mv.visitCode();		
		
		BytecodeGenMethod gen = new BytecodeGenMethod(className,resultSet,method, mv,paramsAndLocals,cw,
				genericsAndBounds,genericsAndBounds,isInterface,classFiles);
		mv.visitMaxs(0, 0);
		mv.visitEnd();
	}
	
	@Override
	public void visit(ParameterList formalParameters) {
		paramsAndLocals = new HashMap<>();
		methodParamsAndTypes = new HashMap<>();
		Iterator<FormalParameter> itr = formalParameters.iterator();
		int i = 1;
		while(itr.hasNext()) {
			FormalParameter fp = itr.next();
			paramsAndLocals.put(fp.getName(), i);
			methodParamsAndTypes.put(fp.getName(), resultSet.resolveType(fp.getType()).resolvedType);
			fp.accept(this);
			i++;
		}
	}
	
	@Override
	public void visit(FormalParameter formalParameter) {
		formalParameter.getType().accept(this);
	}
	
	@Override
	public void visit(RefType refType) {
		type = "L"+refType.toString()+";";
	}

	@Override
	public void visit(SuperWildcardType superWildcardType) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void visit(TypePlaceholder typePlaceholder) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void visit(ExtendsWildcardType extendsWildcardType) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void visit(GenericRefType genericRefType) {
		// TODO Auto-generated method stub
		
	}

	// ??
	@Override
	public void visit(FieldVar fieldVar) {
//		cw.newField(fieldVar.receiver.toString(), fieldVar.fieldVarName.toString(), fieldVar.getType().toString());
		FieldVisitor fv = cw.visitField(Opcodes.ACC_PRIVATE, fieldVar.fieldVarName, "L"+fieldVar.getType()+";", null, null);
		fv.visitEnd();
	}
	
	// access flages?? modifiers
	@Override
	public void visit(Field field) {
		FieldVisitor fv = cw.visitField(Opcodes.ACC_PRIVATE, field.getName(), "L"+field.getType().toString().replace(".", "/")+";", null, null);
		fv.visitEnd();
	}

	@Override
	public void visit(LambdaExpression lambdaExpression) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void visit(Assign assign) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void visit(BinaryExpr binary) {

	}

	@Override
	public void visit(Block block) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void visit(CastExpr castExpr) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void visit(EmptyStmt emptyStmt) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void visit(ForStmt forStmt) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void visit(IfStmt ifStmt) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void visit(InstanceOf instanceOf) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void visit(LocalVar localVar) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void visit(LocalVarDecl localVarDecl) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void visit(MethodCall methodCall) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void visit(NewClass methodCall) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void visit(NewArray newArray) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void visit(Return aReturn) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void visit(ReturnVoid aReturn) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void visit(StaticClassName staticClassName) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void visit(Super aSuper) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void visit(This aThis) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void visit(WhileStmt whileStmt) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void visit(DoStmt whileStmt) {
		// TODO Auto-generated method stub
		
	}

	// ???
	@Override
	public void visit(Literal literal) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void visit(ArgumentList argumentList) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void visit(GenericTypeVar genericTypeVar) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void visit(GenericDeclarationList genericTypeVars) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void visit(AssignToField assignLeftSide) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void visit(AssignToLocal assignLeftSide) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void visit(SuperCall superCall) {
		
	}

	@Override
	public void visit(ExpressionReceiver expressionReceiver) {
		// TODO Auto-generated method stub

	}

	@Override
	public void visit(UnaryExpr unaryExpr) {
		throw new NotImplementedException();
	}

}
