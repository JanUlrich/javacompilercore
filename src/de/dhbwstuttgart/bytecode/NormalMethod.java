package de.dhbwstuttgart.bytecode;

import java.util.HashMap;

import de.dhbwstuttgart.bytecode.descriptor.DescriptorVisitor;
import de.dhbwstuttgart.syntaxtree.Method;
import de.dhbwstuttgart.syntaxtree.ParameterList;
import de.dhbwstuttgart.syntaxtree.type.RefTypeOrTPHOrWildcardOrGeneric;

public class NormalMethod {
	private Method method;
	private HashMap<String, String> genericsAndBounds;
	private HashMap<String, String> genericsAndBoundsMethod;
	private boolean hasGenerics;
	
	public NormalMethod(Method method, boolean hasGenerics) {
		this.method = method;
		this.hasGenerics = hasGenerics;
	}

	public NormalMethod(Method method, HashMap<String, String> genericsAndBounds,
			HashMap<String, String> genericsAndBoundsMethod,boolean hasGenerics) {
		this.method = method;
		this.genericsAndBounds = genericsAndBounds;
		this.genericsAndBoundsMethod = genericsAndBoundsMethod;
		this.hasGenerics = hasGenerics;
	}

	public Method getMethod() {
		return method;
	}
	
	public ParameterList getParameterList() {
		return method.getParameterList();
	}
	
	public HashMap<String, String> getGenericsAndBounds(){
		return genericsAndBounds;
	}
	
	public HashMap<String, String> getGenericsAndBoundsMethod(){
		return genericsAndBoundsMethod;
	}
	
	public RefTypeOrTPHOrWildcardOrGeneric getReturnType() {
		return method.getReturnType();
	}
	
	public boolean hasGen() {
		return this.hasGenerics;
	}
	
	public String accept(DescriptorVisitor descVisitor) {
		return descVisitor.visit(this);
	}
}
