package de.dhbwstuttgart.bytecode;

import de.dhbwstuttgart.bytecode.descriptor.DescriptorVisitor;
import de.dhbwstuttgart.syntaxtree.ParameterList;
import de.dhbwstuttgart.syntaxtree.statement.LambdaExpression;
import de.dhbwstuttgart.syntaxtree.type.RefTypeOrTPHOrWildcardOrGeneric;

public class Lambda {
	private LambdaExpression lambdaExpression;
	
	public Lambda(LambdaExpression lambdaExpression) {
		this.lambdaExpression = lambdaExpression;
	}
	
	public ParameterList getParams() {
		return lambdaExpression.params;
	}
	
	public RefTypeOrTPHOrWildcardOrGeneric getReturnType() {
		return lambdaExpression.getReturnType();
	}
	
	public String accept(DescriptorVisitor descVisitor) {
		return descVisitor.visit(this);
	}
}
