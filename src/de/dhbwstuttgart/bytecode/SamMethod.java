package de.dhbwstuttgart.bytecode;

import java.util.List;

import de.dhbwstuttgart.bytecode.descriptor.DescriptorVisitor;
import de.dhbwstuttgart.syntaxtree.type.RefTypeOrTPHOrWildcardOrGeneric;

public class SamMethod {
	private List<RefTypeOrTPHOrWildcardOrGeneric> argumentList;
	private RefTypeOrTPHOrWildcardOrGeneric returnType;
	
	public SamMethod(List<RefTypeOrTPHOrWildcardOrGeneric> argumentList, RefTypeOrTPHOrWildcardOrGeneric returnType) {
		this.argumentList = argumentList;
		this.returnType = returnType;
	}
	
	public List<RefTypeOrTPHOrWildcardOrGeneric> getArgumentList() {
		return argumentList;
	}



	public RefTypeOrTPHOrWildcardOrGeneric getReturnType() {
		return returnType;
	}



	public String accept(DescriptorVisitor descVisitor) {
		return descVisitor.visit(this);
	}
}
