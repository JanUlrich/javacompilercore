package de.dhbwstuttgart.bytecode.descriptor;

import java.util.Iterator;

import de.dhbwstuttgart.bytecode.Lambda;
import de.dhbwstuttgart.bytecode.MethodFromMethodCall;
import de.dhbwstuttgart.bytecode.NormalConstructor;
import de.dhbwstuttgart.bytecode.NormalMethod;
import de.dhbwstuttgart.bytecode.SamMethod;
import de.dhbwstuttgart.bytecode.signature.TypeToSignature;
import de.dhbwstuttgart.syntaxtree.FormalParameter;
import de.dhbwstuttgart.syntaxtree.statement.Expression;
import de.dhbwstuttgart.syntaxtree.type.RefType;
import de.dhbwstuttgart.syntaxtree.type.RefTypeOrTPHOrWildcardOrGeneric;
import de.dhbwstuttgart.typeinference.result.ResultSet;

public class DescriptorToString implements DescriptorVisitor{
	ResultSet resultSet;
	
	public DescriptorToString(ResultSet resultSet) {
		this.resultSet = resultSet;
	}
	
	private String addReturnType(String desc, RefTypeOrTPHOrWildcardOrGeneric returnType, ResultSet resultSet) {
		if(resultSet.resolveType(returnType).resolvedType.toString().equals("void")){
			desc = desc + ")V";
		}else {
			desc = desc + ")" + "L"+resultSet.resolveType(returnType).resolvedType.acceptTV(new TypeToDescriptor())+ ";";
		}
		return desc;
	}
	
	@Override
	public String visit(NormalMethod method) {
		
		String desc = "(";
		Iterator<FormalParameter> itr = method.getParameterList().iterator();
		while(itr.hasNext()) {
			FormalParameter fp = itr.next();
//			System.out.println(resultSet.resolveType(fp.getType()).resolvedType.acceptTV(new TypeToSignature()));
//			System.out.println("Parmetrisierte typ ? "+ ((RefType) fp.getType()).getParaList().size());
			if(method.hasGen()) {
				String fpDesc = fp.getType().acceptTV(new TypeToDescriptor());
				if(method.getGenericsAndBoundsMethod().containsKey(fpDesc)) {
					desc += "L"+method.getGenericsAndBoundsMethod().get(fpDesc)+ ";";
				}else if(method.getGenericsAndBounds().containsKey(fpDesc)){
					desc += "L"+method.getGenericsAndBounds().get(fpDesc)+ ";";
				}else {
					desc += "L"+resultSet.resolveType(fp.getType()).resolvedType.acceptTV(new TypeToDescriptor())+ ";";
				}
			}
//			else if(((RefType) fp.getType()).getParaList().size() > 0){
//				desc += "L"+resultSet.resolveType(fp.getType()).resolvedType.toString().replace(".", "%").replace("<", "%%").replace(">", "%%")+ ";";
//			}
			else {
				desc += "L"+resultSet.resolveType(fp.getType()).resolvedType.acceptTV(new TypeToDescriptor())+ ";";
			}
		}
		
		if(resultSet.resolveType(method.getReturnType()).resolvedType.toString().equals("void")) {
			desc += ")V";
		}else {
			if(method.hasGen()) {
				String ret = method.getReturnType().acceptTV(new TypeToDescriptor());
				if(method.getGenericsAndBoundsMethod().containsKey(ret)) {
					desc += ")L"+method.getGenericsAndBoundsMethod().get(ret)+ ";";
				}else if(method.getGenericsAndBounds().containsKey(ret)){
					desc += ")L"+method.getGenericsAndBounds().get(ret)+ ";";
				}else {
					desc += ")" + "L"+resultSet.resolveType(method.getReturnType()).resolvedType.acceptTV(new TypeToDescriptor())+ ";";
				}
			}else {
				desc += ")" + "L"+resultSet.resolveType(method.getReturnType()).resolvedType.acceptTV(new TypeToDescriptor())+ ";";
			}
		}
//		desc = addReturnType(desc,method.getReturnType(), resultSet);
		return desc;
	}

	@Override
	public String visit(NormalConstructor constructor) {
		String desc = "(";
		Iterator<FormalParameter> itr = constructor.getParameterList().iterator();
		while(itr.hasNext()) {
			FormalParameter fp = itr.next();
			if(constructor.hasGen()) {
//				System.out.println("Cons has Gens");
				String fpDesc = fp.getType().acceptTV(new TypeToDescriptor());
//				System.out.println(fpDesc);
				if(constructor.getGenericsAndBounds().containsKey(fpDesc)){
					desc += "L"+constructor.getGenericsAndBounds().get(fpDesc)+ ";";
				}else {
					desc += "L"+resultSet.resolveType(fp.getType()).resolvedType.acceptTV(new TypeToDescriptor())+ ";";
				}
			}else {
//				System.out.println("Cons has NOT Gens");
				desc += "L"+resultSet.resolveType(fp.getType()).resolvedType.acceptTV(new TypeToDescriptor())+ ";";
			}
		}
		desc = desc + ")V";
		return desc;
	}

	@Override
	public String visit(Lambda lambdaExpression) {
		String desc = "(";
		Iterator<FormalParameter> itr = lambdaExpression.getParams().iterator();
		while(itr.hasNext()) {
			FormalParameter fp = itr.next();
			desc = desc + "L"+resultSet.resolveType(fp.getType()).resolvedType.acceptTV(new TypeToDescriptor()) + ";";
		}
		desc = addReturnType(desc, lambdaExpression.getReturnType(), resultSet);
		return desc;
	}

	@Override
	public String visit(SamMethod samMethod) {
		String desc = "(";
		Iterator<RefTypeOrTPHOrWildcardOrGeneric> itr = samMethod.getArgumentList().iterator();
		while(itr.hasNext()) {
			RefTypeOrTPHOrWildcardOrGeneric rt = itr.next();
			desc = desc + "L"+resultSet.resolveType(rt).resolvedType.acceptTV(new TypeToDescriptor())+";";
		}
		desc = desc + ")"+"L"+resultSet.resolveType(samMethod.getReturnType()).resolvedType.acceptTV(new TypeToDescriptor())+";";
		return desc;
	}

	@Override
	public String visit(MethodFromMethodCall methodFromMethodCall) {
		String desc = "(";
		for(Expression e : methodFromMethodCall.getArgList().getArguments()) {
			String d = e.getType().acceptTV(new TypeToDescriptor());
			if(methodFromMethodCall.getGenericsAndBoundsMethod().containsKey(d)) {
				desc += "L"+methodFromMethodCall.getGenericsAndBoundsMethod().get(d)+ ";";
			}else if(methodFromMethodCall.getGenericsAndBounds().containsKey(d)) {
				desc += "L"+methodFromMethodCall.getGenericsAndBounds().get(d)+ ";";
			}else {
				desc += "L"+resultSet.resolveType(e.getType()).resolvedType.acceptTV(new TypeToDescriptor())+ ";";
			}
		}
		
		if(resultSet.resolveType(methodFromMethodCall.getReturnType()).resolvedType.toString().equals("void")) {
			desc += ")V";
		}else {
			String ret = resultSet.resolveType(methodFromMethodCall.getReturnType()).resolvedType.acceptTV(new TypeToDescriptor());
			if(methodFromMethodCall.getGenericsAndBoundsMethod().containsKey(ret)) {
				desc += ")L"+methodFromMethodCall.getGenericsAndBoundsMethod().get(ret)+ ";";
			}else if(methodFromMethodCall.getGenericsAndBounds().containsKey(ret)){
				desc += ")L"+methodFromMethodCall.getGenericsAndBounds().get(ret)+ ";";
			}else {
				desc += ")" + "L"+resultSet.resolveType(methodFromMethodCall.getReturnType()).resolvedType.acceptTV(new TypeToDescriptor())+ ";";
			}
		}
//		desc = addReturnType(desc, methodFromMethodCall.getReturnType(), resultSet);
		return desc;
	}
	
}
