package de.dhbwstuttgart.bytecode.descriptor;

import de.dhbwstuttgart.bytecode.Lambda;
import de.dhbwstuttgart.bytecode.MethodFromMethodCall;
import de.dhbwstuttgart.bytecode.NormalConstructor;
import de.dhbwstuttgart.bytecode.NormalMethod;
import de.dhbwstuttgart.bytecode.SamMethod;

public interface DescriptorVisitor {
	public String visit(NormalMethod method);
	public String visit(NormalConstructor constructor);
	public String visit(Lambda lambdaExpression);
	public String visit(SamMethod samMethod);
	public String visit(MethodFromMethodCall methodFromMethodCall);
}
