package de.dhbwstuttgart.bytecode.descriptor;

import de.dhbwstuttgart.exceptions.NotImplementedException;
import de.dhbwstuttgart.syntaxtree.type.ExtendsWildcardType;
import de.dhbwstuttgart.syntaxtree.type.GenericRefType;
import de.dhbwstuttgart.syntaxtree.type.RefType;
import de.dhbwstuttgart.syntaxtree.type.SuperWildcardType;
import de.dhbwstuttgart.syntaxtree.type.TypePlaceholder;
import de.dhbwstuttgart.syntaxtree.type.TypeVisitor;

public class TypeToDescriptor implements TypeVisitor<String>{

	@Override
	public String visit(RefType refType) {
		return refType.getName().toString().replace(".", "/");
	}

	@Override
	public String visit(SuperWildcardType superWildcardType) {
		throw new NotImplementedException();
	}

	@Override
	public String visit(TypePlaceholder typePlaceholder) {
		return typePlaceholder.toString().replace(".", "/");
	}

	@Override
	public String visit(ExtendsWildcardType extendsWildcardType) {
		throw new NotImplementedException();
	}

	@Override
	public String visit(GenericRefType genericRefType) {
		return genericRefType.getParsedName().replace(".", "/");
	}
}