package de.dhbwstuttgart.syntaxtree;



import org.antlr.v4.runtime.Token;

import java.util.Iterator;
import java.util.List;


public class ParameterList extends SyntaxTreeNode implements Iterable<FormalParameter>
{
    private List<FormalParameter> formalparameter;
    
    public ParameterList(List<FormalParameter> params, Token offset){
        super(offset);
        this.formalparameter = params;
    }

    public FormalParameter getParameterAt(int i)
    {
        if (i >= formalparameter.size() ) return null;
        
        return formalparameter.get(i);
    }
    
    
    public List<FormalParameter> getFormalparalist()
    {
        return formalparameter;
    }

	@Override
	public Iterator<FormalParameter> iterator() {
		return formalparameter.iterator();
	}

    @Override
    public void accept(ASTVisitor visitor) {
        visitor.visit(this);
    }
}
