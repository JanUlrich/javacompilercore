package de.dhbwstuttgart.syntaxtree;

import de.dhbwstuttgart.syntaxtree.type.*;

public interface ASTVisitor extends StatementVisitor{

    void visit(SourceFile sourceFile);

    void visit(GenericTypeVar genericTypeVar);

    void visit(FormalParameter formalParameter);

    void visit(GenericDeclarationList genericTypeVars);

    void visit(Field field);

    void visit(Method field);

    void visit(Constructor field);

    void visit(ParameterList formalParameters);

    void visit(ClassOrInterface classOrInterface);

    void visit(RefType refType);

    void visit(SuperWildcardType superWildcardType);

    void visit(TypePlaceholder typePlaceholder);

    void visit(ExtendsWildcardType extendsWildcardType);

    void visit(GenericRefType genericRefType);
}
