package de.dhbwstuttgart.syntaxtree;

import de.dhbwstuttgart.parser.SyntaxTreeGenerator.AssignToLocal;
import de.dhbwstuttgart.syntaxtree.statement.*;

public interface StatementVisitor {

    void visit(ArgumentList argumentList);

    void visit(LambdaExpression lambdaExpression);

    void visit(Assign assign);

    void visit(BinaryExpr binary);

    void visit(Block block);

    void visit(CastExpr castExpr);

    void visit(EmptyStmt emptyStmt);

    void visit(FieldVar fieldVar);

    void visit(ForStmt forStmt);

    void visit(IfStmt ifStmt);

    void visit(InstanceOf instanceOf);

    void visit(LocalVar localVar);

    void visit(LocalVarDecl localVarDecl);

    void visit(MethodCall methodCall);

    void visit(NewClass methodCall);

    void visit(NewArray newArray);

    void visit(Return aReturn);

    void visit(ReturnVoid aReturn);

    void visit(StaticClassName staticClassName);

    void visit(Super aSuper);

    void visit(This aThis);

    void visit(WhileStmt whileStmt);

    void visit(DoStmt whileStmt);

    void visit(AssignToField assignLeftSide);

    void visit(AssignToLocal assignLeftSide);

    void visit(SuperCall superCall);

    void visit(ExpressionReceiver expressionReceiver);

    void visit(UnaryExpr unaryExpr);

    void visit(Literal literal);
}
