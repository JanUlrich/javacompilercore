package de.dhbwstuttgart.syntaxtree;
import java.io.File;
import java.util.*;

import de.dhbwstuttgart.parser.NullToken;
import de.dhbwstuttgart.parser.scope.JavaClassName;
import de.dhbwstuttgart.typeinference.constraints.ConstraintSet;
import de.dhbwstuttgart.typeinference.assumptions.TypeInferenceInformation;
//import sun.security.x509.X509CertInfo;


public class SourceFile extends SyntaxTreeNode{
  private String pkgName;

  public final List<ClassOrInterface> KlassenVektor;
  public final Set<JavaClassName> imports;

  /**
     * Die SourceFile reprÃ¤sntiert eine zu einem Syntaxbaum eingelesene Java-Datei.
     * SourceFile stellt dabei den Wurzelknoten des Syntaxbaumes dar.
     */
  public SourceFile(String pkgName, List<ClassOrInterface> classDefinitions, Set<JavaClassName> imports){
    super(new NullToken());
    this.KlassenVektor = classDefinitions;
    this.pkgName = pkgName;
    this.imports = imports;
  }
  
  public String getPkgName(){
    return this.pkgName;
  }

  // Get imports (to test implementation)
  public Set<JavaClassName> getImports(){
    return this.imports;
  }

  public List<ClassOrInterface> getClasses() {
    return KlassenVektor;
  }

  @Override
  public void accept(ASTVisitor visitor) {
    visitor.visit(this);
  }
}
