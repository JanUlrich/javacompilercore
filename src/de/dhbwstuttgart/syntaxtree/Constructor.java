package de.dhbwstuttgart.syntaxtree;

import de.dhbwstuttgart.syntaxtree.statement.Statement;
import de.dhbwstuttgart.syntaxtree.statement.SuperCall;
import de.dhbwstuttgart.syntaxtree.type.RefType;
import de.dhbwstuttgart.syntaxtree.type.RefTypeOrTPHOrWildcardOrGeneric;
import org.antlr.v4.runtime.Token;

import de.dhbwstuttgart.syntaxtree.statement.Block;

import java.util.List;

public class Constructor extends Method {


	//TODO: Constructor braucht ein super-Statement
	public Constructor(int modifier, String name, RefTypeOrTPHOrWildcardOrGeneric returnType, ParameterList parameterList, Block codeInsideConstructor,
					   GenericDeclarationList gtvDeclarations, Token offset, List<Statement> fieldInitializations) {
		super(modifier, name, returnType, parameterList, prepareBlock(codeInsideConstructor,fieldInitializations), gtvDeclarations, offset);

	}

	/**
	 * @param fieldInitializations - Das sind die Statements,
	 *                                welche die Felder der zugehörigen Klasse dieses
	 *                                Konstruktor initialisieren
	 */
	protected static Block prepareBlock(Block constructorBlock, List<Statement> fieldInitializations){
		List<Statement> statements = constructorBlock.getStatements();
		statements.add(0, new SuperCall(constructorBlock.getOffset()));
		return new Block(statements, constructorBlock.getOffset());
	}

	@Override
	public void accept(ASTVisitor visitor) {
		visitor.visit(this);
	}
}
