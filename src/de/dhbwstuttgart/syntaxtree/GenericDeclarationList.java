package de.dhbwstuttgart.syntaxtree;

import org.antlr.v4.runtime.Token;

import java.util.*;


/**
 * Stellt eine Deklarations-Liste von Generischen Variablen dar.
 * Kann vor Methoden und Klassen auftauchen. (<....>)
 * @author janulrich
 * 
 */
public class GenericDeclarationList extends SyntaxTreeNode implements Iterable<GenericTypeVar>{

	private Token offsetOfLastElement;
	private List<GenericTypeVar> gtvs = new ArrayList<>();
	
	public GenericDeclarationList(List<GenericTypeVar> values, Token endOffset) {
		super(endOffset);
		gtvs = values;
		this.offsetOfLastElement = endOffset;
	}

	@Override
	public Iterator<GenericTypeVar> iterator() {
		return gtvs.iterator();
	}

	@Override
	public void accept(ASTVisitor visitor) {
		visitor.visit(this);
	}
}
