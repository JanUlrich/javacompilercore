package de.dhbwstuttgart.syntaxtree;

import java.util.ArrayList;
import java.util.List;

import de.dhbwstuttgart.core.IItemWithOffset;
import de.dhbwstuttgart.typeinference.assumptions.TypeInferenceInformation;
import org.antlr.v4.runtime.Token;
//import org.antlr.v4.runtime.misc.Pair;

public abstract class SyntaxTreeNode implements IItemWithOffset{
    private final Token offset;

    public SyntaxTreeNode(Token offset){
        this.offset = offset;
    }

    public Token getOffset(){
        return offset;
    }

    public abstract void accept(ASTVisitor visitor);
}
