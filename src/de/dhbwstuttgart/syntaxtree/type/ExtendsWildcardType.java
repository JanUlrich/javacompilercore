package de.dhbwstuttgart.syntaxtree.type;


import de.dhbwstuttgart.syntaxtree.ASTVisitor;
import de.dhbwstuttgart.typeinference.result.ResultSetVisitor;
import org.antlr.v4.runtime.Token;

/**
 *  Stellt eine Wildcard mit oberer Grenze dar.
 *  z.B. void test(? extends Number var){..}
 *   ... 
 * @author luar 2.12.06
 * 
 */

public class ExtendsWildcardType extends WildcardType{
	
	/**
	* Author: Arne LÃ¼dtke<br/>
	* Standard Konstruktor fÃ¼r eine ExtendsWildcard
	*/
	public ExtendsWildcardType (RefTypeOrTPHOrWildcardOrGeneric extendsType, Token offset)
	{
		super(extendsType, offset);
	}

	@Override
	public boolean isExtends() {
		return true;
	}

	@Override
	public boolean isSuper() {
		return false;
	}


	@Override
	public void accept(ASTVisitor visitor) {
		visitor.visit(this);
	}

	@Override
	public <A> A acceptTV(TypeVisitor<A> visitor) {
		return visitor.visit(this);
	}

	@Override
	public void accept(ResultSetVisitor visitor) {
		visitor.visit(this);
	}
}
