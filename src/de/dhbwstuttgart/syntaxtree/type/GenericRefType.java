package de.dhbwstuttgart.syntaxtree.type;

import de.dhbwstuttgart.syntaxtree.ASTVisitor;
import de.dhbwstuttgart.typeinference.result.ResultSetVisitor;
import org.antlr.v4.runtime.Token;

public class GenericRefType extends RefTypeOrTPHOrWildcardOrGeneric
{
    private String name;

    public GenericRefType(String name, Token offset)
    {
        super(offset);
        this.name = name;
    }

    public String getParsedName(){
        return name.toString();
    }

    @Override
    public void accept(ASTVisitor visitor) {
        visitor.visit(this);
    }

    @Override
    public <A> A acceptTV(TypeVisitor<A> visitor) {
        return visitor.visit(this);
    }

    @Override
    public void accept(ResultSetVisitor visitor) {
        visitor.visit(this);
    }
}

