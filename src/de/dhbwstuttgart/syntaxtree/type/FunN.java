package de.dhbwstuttgart.syntaxtree.type;


import de.dhbwstuttgart.parser.NullToken;
import de.dhbwstuttgart.parser.scope.JavaClassName;

import java.util.List;

/**
 * @see Spezifikation "Complete Typeinference in Java 8" von Martin PlÃ¼micke
 * "interface FunN<R,T1, T2, ... ,TN> { R apply(T1 arg1, T2 arg2, ... , TN argN); }"
 * @author A10023 - Andreas Stadelmeier
 * 
 * Bemerkung:
 * FunN ist ein RefType. Der RefType ist nicht mit einem FunNInterface verbunden.
 * 
 */
public class FunN extends RefType {
	/**
	 * @author Andreas Stadelmeier,  a10023
	 * BenÃ¶tigt fÃ¼r den Typinferenzalgorithmus fÃ¼r Java 8
	 * Generiert einen RefType auf eine FunN<R,T1,...,TN> - Klasse.
	 * @param params
	 * @return
	 */
	public FunN(List<RefTypeOrTPHOrWildcardOrGeneric> params) {
		super(new JavaClassName("Fun"+params.size()), params, new NullToken());
	}

	/**
	 * Spezieller Konstruktor um eine FunN ohne Returntype zu generieren
	 
	protected FunN(List<? extends Type> list){
		super("",0);
		if(list==null)throw new NullPointerException();
		setT(list);
		this.name = new JavaClassName("Fun"+list.size());//getName();
	}
	*/

}
