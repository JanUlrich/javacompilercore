package de.dhbwstuttgart.syntaxtree.type;

import de.dhbwstuttgart.syntaxtree.ASTVisitor;
import de.dhbwstuttgart.syntaxtree.SyntaxTreeNode;
import de.dhbwstuttgart.typeinference.result.ResultSetVisitor;
import org.antlr.v4.runtime.Token;

public abstract class RefTypeOrTPHOrWildcardOrGeneric extends SyntaxTreeNode{
    public RefTypeOrTPHOrWildcardOrGeneric(Token offset) {
        super(offset);
    }

    @Override
    public abstract void accept(ASTVisitor visitor);

    public abstract <A> A acceptTV(TypeVisitor<A> visitor);
    public abstract void accept(ResultSetVisitor visitor);
}
