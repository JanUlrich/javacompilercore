package de.dhbwstuttgart.syntaxtree.type;

public interface TypeVisitor<A> {
    A visit(RefType refType);

    A visit(SuperWildcardType superWildcardType);

    A visit(TypePlaceholder typePlaceholder);

    A visit(ExtendsWildcardType extendsWildcardType);

    A visit(GenericRefType genericRefType);
}
