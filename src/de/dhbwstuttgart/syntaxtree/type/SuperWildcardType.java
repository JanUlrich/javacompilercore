package de.dhbwstuttgart.syntaxtree.type;


import de.dhbwstuttgart.syntaxtree.ASTVisitor;
import de.dhbwstuttgart.typeinference.result.ResultSetVisitor;
import org.antlr.v4.runtime.Token;

/**
 *  Stellt eine Wildcard mit unterer Grenze dar.
 *  z.B. void test(? super Integer var){..}
 *   ... 
 * @author luar 2.12.06
 * 
 */

public class SuperWildcardType extends WildcardType{

	/**
	* Author: Arne LÃ¼dtke<br/>
	* Standard Konstruktor fÃ¼r eine SuperWildcard
	*/
	public SuperWildcardType( RefTypeOrTPHOrWildcardOrGeneric innerType, Token offset)
	{
		super(innerType, offset);
	}
	
	/**
	* Author: Arne LÃ¼dtke<br/>
	* Gibt den Typen in der Wildcard zurÃ¼ck.
	* Beispiel: ? super Integer.
	* Integer wird zurÃ¼ckgegeben.
	*/
	public RefTypeOrTPHOrWildcardOrGeneric getInnerType()
	{
		return this.innerType;
	}

	@Override
	public boolean isExtends() {
		return false;
	}

	@Override
	public boolean isSuper() {
		return true;
	}

	@Override
	public void accept(ASTVisitor visitor) {
		visitor.visit(this);
	}

	@Override
	public <A> A acceptTV(TypeVisitor<A> visitor) {
		return visitor.visit(this);
	}

	@Override
	public void accept(ResultSetVisitor visitor) {
		visitor.visit(this);
	}
}
