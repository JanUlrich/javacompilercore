package de.dhbwstuttgart.syntaxtree.type;

import org.antlr.v4.runtime.Token;

import de.dhbwstuttgart.parser.scope.JavaClassName;


public class Void extends RefType
{
    public Void(Token offset) {
        super(JavaClassName.Void, offset);
    }
}

