package de.dhbwstuttgart.syntaxtree.type;

import org.antlr.v4.runtime.Token;

/**
 *  Stellt eine Wildcard in Java dar.
 *  z.B. void Test(? var){..}
 * @author luar 2.12.06
 * 
 */

public abstract class WildcardType extends RefTypeOrTPHOrWildcardOrGeneric {
	
	protected RefTypeOrTPHOrWildcardOrGeneric innerType = null;
	
	/**
	* Author: Arne LÃ¼dtke<br/>
	* Standard Konstruktor fÃ¼r eine Wildcard
	*/
	public WildcardType(RefTypeOrTPHOrWildcardOrGeneric innerType, Token offset)
	{
		super(offset);
		this.innerType = innerType;
	}

	public RefTypeOrTPHOrWildcardOrGeneric getInnerType(){
		return innerType;
	}
	
	/**
	* Author: Arne LÃ¼dtke<br/>
	* Gibt String Entsprechung zurÃ¼ck.
	*/
	public String toString()
	{
		return "?";
	}


	public abstract boolean isExtends();
	public abstract boolean isSuper();
}
