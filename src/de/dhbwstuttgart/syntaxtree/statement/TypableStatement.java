package de.dhbwstuttgart.syntaxtree.statement;

import de.dhbwstuttgart.syntaxtree.ASTVisitor;
import de.dhbwstuttgart.syntaxtree.StatementVisitor;
import de.dhbwstuttgart.syntaxtree.SyntaxTreeNode;
import de.dhbwstuttgart.syntaxtree.type.RefTypeOrTPHOrWildcardOrGeneric;
import org.antlr.v4.runtime.Token;

public abstract class TypableStatement extends SyntaxTreeNode{
    private RefTypeOrTPHOrWildcardOrGeneric type;

    public TypableStatement(RefTypeOrTPHOrWildcardOrGeneric type, Token offset){
        super(offset);
        if(type == null)throw new NullPointerException();
        this.type = type;
    }

    public RefTypeOrTPHOrWildcardOrGeneric getType(){
        return type;
    }

    public abstract void accept(StatementVisitor visitor);

    @Override
    public void accept(ASTVisitor visitor) {
        this.accept((StatementVisitor)visitor);
    }
}
