package de.dhbwstuttgart.syntaxtree.statement;

import de.dhbwstuttgart.syntaxtree.StatementVisitor;
import de.dhbwstuttgart.syntaxtree.type.RefType;
import de.dhbwstuttgart.syntaxtree.type.Void;
import org.antlr.v4.runtime.Token;

import de.dhbwstuttgart.syntaxtree.SyntaxTreeNode;

import java.util.ArrayList;


public class SuperCall extends MethodCall
{
    public SuperCall(Token offset){
        this(new ArgumentList(new ArrayList<Expression>(), offset),offset);
    }

    public SuperCall(ArgumentList argumentList, Token offset){
        super(new Void(offset), new ExpressionReceiver(new This(offset)), "<init>", argumentList, offset);
    }


    @Override
    public void accept(StatementVisitor visitor) {
        visitor.visit(this);
    }
}
