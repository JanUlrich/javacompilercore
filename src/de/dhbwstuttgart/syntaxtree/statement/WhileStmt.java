package de.dhbwstuttgart.syntaxtree.statement;


import de.dhbwstuttgart.syntaxtree.StatementVisitor;
import de.dhbwstuttgart.syntaxtree.type.TypePlaceholder;
import de.dhbwstuttgart.typeinference.assumptions.TypeInferenceBlockInformation;
import de.dhbwstuttgart.typeinference.constraints.ConstraintSet;
import de.dhbwstuttgart.typeinference.assumptions.TypeInferenceInformation;
import de.dhbwstuttgart.exceptions.NotImplementedException;
import org.antlr.v4.runtime.Token;

public class WhileStmt extends Statement
{
    public final Expression expr;
    public final Statement loopBlock;

    public WhileStmt(Expression expr, Statement loopBlock, Token offset)
    {
        super(TypePlaceholder.fresh(offset), offset);
        this.expr = expr;
        this.loopBlock = loopBlock;
    }

    @Override
    public void accept(StatementVisitor visitor) {
        visitor.visit(this);
    }
}
