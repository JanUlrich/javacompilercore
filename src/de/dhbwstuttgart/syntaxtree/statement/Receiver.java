package de.dhbwstuttgart.syntaxtree.statement;

import de.dhbwstuttgart.syntaxtree.type.RefTypeOrTPHOrWildcardOrGeneric;
import org.antlr.v4.runtime.Token;

public abstract class Receiver extends Expression
{
    public Receiver(RefTypeOrTPHOrWildcardOrGeneric type, Token offset)
    {
        super(type, offset);
    }
}
