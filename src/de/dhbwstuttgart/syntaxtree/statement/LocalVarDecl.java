package de.dhbwstuttgart.syntaxtree.statement;


import de.dhbwstuttgart.exceptions.NotImplementedException;
import de.dhbwstuttgart.syntaxtree.StatementVisitor;
import de.dhbwstuttgart.syntaxtree.type.RefTypeOrTPHOrWildcardOrGeneric;
import de.dhbwstuttgart.typeinference.assumptions.TypeInferenceBlockInformation;
import de.dhbwstuttgart.typeinference.constraints.ConstraintSet;
import org.antlr.v4.runtime.Token;


public class LocalVarDecl extends Statement
{

    private String name;

    public LocalVarDecl(String name, RefTypeOrTPHOrWildcardOrGeneric type, Token offset)
    {
        super(type, offset);
        this.name = name;
    }

    @Override
    public void accept(StatementVisitor visitor) {
        visitor.visit(this);
    }

    public String getName() {
        return name;
    }
}
