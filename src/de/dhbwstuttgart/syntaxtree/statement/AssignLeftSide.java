package de.dhbwstuttgart.syntaxtree.statement;

import de.dhbwstuttgart.syntaxtree.StatementVisitor;
import de.dhbwstuttgart.syntaxtree.type.RefTypeOrTPHOrWildcardOrGeneric;
import org.antlr.v4.runtime.Token;

public abstract class AssignLeftSide extends TypableStatement{

    public AssignLeftSide(RefTypeOrTPHOrWildcardOrGeneric type, Token offset) {
        super(type, offset);
    }
}
