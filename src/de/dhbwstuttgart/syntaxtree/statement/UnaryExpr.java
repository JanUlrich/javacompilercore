package de.dhbwstuttgart.syntaxtree.statement;


import de.dhbwstuttgart.syntaxtree.StatementVisitor;
import de.dhbwstuttgart.syntaxtree.statement.Expression;
import de.dhbwstuttgart.syntaxtree.statement.JavaInternalExpression;
import de.dhbwstuttgart.syntaxtree.type.RefTypeOrTPHOrWildcardOrGeneric;
import org.antlr.v4.runtime.Token;

public class UnaryExpr extends JavaInternalExpression
{
    public enum Operation{
        NOT,
        MINUS,
        PREINCREMENT,
        PREDECREMENT,
        POSTINCREMENT,
        PLUS, POSTDECREMENT
    }


    public final Operation operation;
    public Expression expr;

    public UnaryExpr(Operation operation, Expression argument, RefTypeOrTPHOrWildcardOrGeneric retType, Token offset)
    {
        super(retType, offset);
        this.expr = argument;
        this.operation = operation;
    }
    @Override
    public void accept(StatementVisitor visitor) {
        visitor.visit(this);
    }


}
