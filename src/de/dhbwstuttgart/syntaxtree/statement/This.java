package de.dhbwstuttgart.syntaxtree.statement;

import de.dhbwstuttgart.syntaxtree.StatementVisitor;
import de.dhbwstuttgart.syntaxtree.type.RefType;
import de.dhbwstuttgart.syntaxtree.type.TypePlaceholder;
import org.antlr.v4.runtime.Token;
import de.dhbwstuttgart.exceptions.NotImplementedException;

public class This extends Expression
{
    public This(Token offset)
    {
        super(TypePlaceholder.fresh(offset),offset);
    }
    
    public ArgumentList arglist;

    @Override
    public void accept(StatementVisitor visitor) {
        visitor.visit(this);
    }
}
