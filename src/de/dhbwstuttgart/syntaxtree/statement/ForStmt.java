
package de.dhbwstuttgart.syntaxtree.statement;

import de.dhbwstuttgart.exceptions.NotImplementedException;
import de.dhbwstuttgart.syntaxtree.StatementVisitor;
import de.dhbwstuttgart.typeinference.assumptions.TypeInferenceBlockInformation;
import de.dhbwstuttgart.typeinference.constraints.ConstraintSet;

public class ForStmt extends Statement
{
    
    private Expression head_Initializer_1;
	private Expression head_Condition_1;
	private Expression head_Loop_expr_1;
	private Expression head_Initializer;
	private Expression head_Condition;
	private Expression head_Loop_expr;
	public Block body_Loop_block;

	public ForStmt(int offset, int variableLength)
    {
        super(null,null);
    }

    @Override
    public void accept(StatementVisitor visitor) {
        visitor.visit(this);
    }
}