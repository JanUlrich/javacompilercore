package de.dhbwstuttgart.syntaxtree.statement;

import de.dhbwstuttgart.syntaxtree.StatementVisitor;
import de.dhbwstuttgart.syntaxtree.type.TypePlaceholder;
import org.antlr.v4.runtime.Token;

public class DoStmt extends WhileStmt
{
    public DoStmt(Expression expr, Statement loopBlock, Token offset)
    {
        super(expr, loopBlock, offset);
    }

    @Override
    public void accept(StatementVisitor visitor) {
        visitor.visit(this);
    }
}
