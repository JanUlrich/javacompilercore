package de.dhbwstuttgart.syntaxtree.statement;

import de.dhbwstuttgart.parser.scope.JavaClassName;
import de.dhbwstuttgart.syntaxtree.StatementVisitor;
import de.dhbwstuttgart.syntaxtree.type.RefType;
import de.dhbwstuttgart.typeinference.assumptions.TypeInferenceBlockInformation;
import de.dhbwstuttgart.typeinference.constraints.ConstraintSet;
import de.dhbwstuttgart.typeinference.assumptions.TypeInferenceInformation;
import org.antlr.v4.runtime.Token;

public class StaticClassName extends Receiver {
    public StaticClassName(JavaClassName className, Token offset) {
        super(new RefType(className, offset), offset);
    }

    @Override
    public void accept(StatementVisitor visitor) {
        visitor.visit(this);
    }
}
