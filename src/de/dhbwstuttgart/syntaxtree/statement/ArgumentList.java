package de.dhbwstuttgart.syntaxtree.statement;


import de.dhbwstuttgart.syntaxtree.ASTVisitor;
import de.dhbwstuttgart.syntaxtree.StatementVisitor;
import de.dhbwstuttgart.syntaxtree.SyntaxTreeNode;
import org.antlr.v4.runtime.Token;

import java.util.List;

public class ArgumentList extends SyntaxTreeNode
{
    public ArgumentList(List<Expression> expr, Token offset) {
		super(offset);
		this.expr = expr;
	}

	private List<Expression> expr;

	public List<Expression> getArguments(){
		return expr;
	}

	
	@Override
	public void accept(ASTVisitor visitor) {
		visitor.visit(this);
	}

	public void accept(StatementVisitor visitor) {
		visitor.visit(this);
	}
}
