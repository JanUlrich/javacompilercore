package de.dhbwstuttgart.syntaxtree.statement;

import de.dhbwstuttgart.syntaxtree.*;
import de.dhbwstuttgart.syntaxtree.type.RefType;
import de.dhbwstuttgart.syntaxtree.type.RefTypeOrTPHOrWildcardOrGeneric;
import de.dhbwstuttgart.typeinference.assumptions.TypeInferenceBlockInformation;
import de.dhbwstuttgart.typeinference.constraints.Constraint;
import de.dhbwstuttgart.typeinference.constraints.ConstraintSet;
import de.dhbwstuttgart.typeinference.constraints.Pair;
import org.antlr.v4.runtime.Token;
//import sun.reflect.generics.reflectiveObjects.NotImplementedException;

import java.util.ArrayList;

public class LambdaExpression extends Expression implements TypeScope {
	public final Block methodBody;
	public final ParameterList params;
	
	public LambdaExpression(RefTypeOrTPHOrWildcardOrGeneric type, ParameterList params, Block methodBody, Token offset) {
		super(type,offset);
		this.methodBody = methodBody;
		this.params = params;
	}

	@Override
	public void accept(StatementVisitor visitor) {
		visitor.visit(this);
	}

	@Override
	public Iterable<? extends GenericTypeVar> getGenerics() {
		//Lambda-Ausdrücke haben keine Generics
		return new ArrayList<>();
	}

	@Override
	public RefTypeOrTPHOrWildcardOrGeneric getReturnType() {
		//RefType type = (RefType) this.getType();
		//return type.getParaList().get(0);
		return methodBody.getType();
	}
}
