package de.dhbwstuttgart.syntaxtree.statement;

import de.dhbwstuttgart.syntaxtree.StatementVisitor;
import org.antlr.v4.runtime.Token;

public class ReturnVoid extends Return{
    public ReturnVoid(Token offset) {
        super(new EmptyStmt(offset), offset);
    }

    @Override
    public void accept(StatementVisitor visitor) {
        visitor.visit(this);
    }
}
