package de.dhbwstuttgart.syntaxtree.statement;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import de.dhbwstuttgart.exceptions.TypeinferenceException;
import de.dhbwstuttgart.parser.NullToken;
import de.dhbwstuttgart.syntaxtree.ClassOrInterface;
import de.dhbwstuttgart.syntaxtree.Method;
import de.dhbwstuttgart.syntaxtree.StatementVisitor;
import de.dhbwstuttgart.syntaxtree.type.RefType;
import de.dhbwstuttgart.syntaxtree.type.RefTypeOrTPHOrWildcardOrGeneric;
import de.dhbwstuttgart.typeinference.assumptions.MethodAssumption;
import de.dhbwstuttgart.typeinference.assumptions.TypeInferenceBlockInformation;
import de.dhbwstuttgart.typeinference.constraints.Constraint;
import de.dhbwstuttgart.typeinference.constraints.ConstraintSet;
import de.dhbwstuttgart.typeinference.constraints.Pair;
import de.dhbwstuttgart.typeinference.unify.model.PairOperator;
import org.antlr.v4.runtime.Token;
import de.dhbwstuttgart.exceptions.NotImplementedException;


public class NewClass extends MethodCall
{
    /**
     *
     * @param newClass Typ der Instanzierten Klasse
     * @param args Argumente mit denen der New-Call aufgerufen wurde
     * @param start
     */
    public NewClass(RefType newClass, ArgumentList args, Token start) {
        super(newClass, new ExpressionReceiver(new EmptyStmt(start)), newClass.getName().toString(), args, start);
    }

    @Override
    public void accept(StatementVisitor visitor) {
        visitor.visit(this);
    }
}
