package de.dhbwstuttgart.syntaxtree.statement;

import de.dhbwstuttgart.syntaxtree.StatementVisitor;
import de.dhbwstuttgart.syntaxtree.type.Void;
import de.dhbwstuttgart.typeinference.assumptions.TypeInferenceBlockInformation;
import de.dhbwstuttgart.typeinference.constraints.ConstraintSet;
import de.dhbwstuttgart.typeinference.assumptions.TypeInferenceInformation;
import org.antlr.v4.runtime.Token;


public class EmptyStmt extends Statement
{
    public EmptyStmt(Token offset)
    {
        super(new Void(offset),offset);
    }

    @Override
    public void accept(StatementVisitor visitor) {
        visitor.visit(this);
    }
}
