
package de.dhbwstuttgart.syntaxtree.statement;

import de.dhbwstuttgart.syntaxtree.ASTVisitor;
import de.dhbwstuttgart.syntaxtree.StatementVisitor;
import de.dhbwstuttgart.syntaxtree.SyntaxTreeNode;
import de.dhbwstuttgart.syntaxtree.type.RefTypeOrTPHOrWildcardOrGeneric;
import de.dhbwstuttgart.typeinference.assumptions.TypeInferenceBlockInformation;
import de.dhbwstuttgart.typeinference.constraints.ConstraintSet;
import org.antlr.v4.runtime.Token;

public abstract class Expression extends TypableStatement
{
    public Expression(RefTypeOrTPHOrWildcardOrGeneric type, Token offset){
        super(type, offset);
    }
}
