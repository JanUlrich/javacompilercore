package de.dhbwstuttgart.syntaxtree.statement;

import de.dhbwstuttgart.exceptions.NotImplementedException;
import de.dhbwstuttgart.syntaxtree.StatementVisitor;
import de.dhbwstuttgart.syntaxtree.type.RefTypeOrTPHOrWildcardOrGeneric;


public class InstanceOf extends BinaryExpr
{
    public Expression expr;
    private RefTypeOrTPHOrWildcardOrGeneric reftype;
    
    public InstanceOf(int offset,int variableLength)
    {
        super(null, null, null, null, null);
        throw new NotImplementedException();
        // #JB# 20.04.2005
    }

    @Override
    public void accept(StatementVisitor visitor) {
        visitor.visit(this);
    }
}
