package de.dhbwstuttgart.syntaxtree.statement;

import de.dhbwstuttgart.syntaxtree.StatementVisitor;

public class AssignToField extends AssignLeftSide{
    public final FieldVar field;
    public AssignToField(FieldVar fieldVar) {
        super(fieldVar.getType(), fieldVar.getOffset());
        field = fieldVar;
    }

    @Override
    public void accept(StatementVisitor visitor) {
        visitor.visit(this);
    }
}
