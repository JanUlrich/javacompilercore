package de.dhbwstuttgart.syntaxtree.statement;

import de.dhbwstuttgart.syntaxtree.StatementVisitor;
import de.dhbwstuttgart.syntaxtree.type.RefTypeOrTPHOrWildcardOrGeneric;
import org.antlr.v4.runtime.Token;

public class ExpressionReceiver extends Receiver
{
    public final Expression expr;

    public ExpressionReceiver(Expression expr)
    {
        super(expr.getType(), expr.getOffset());
        this.expr = expr;
    }

    @Override
    public void accept(StatementVisitor visitor) {
        visitor.visit(this);
    }
}
