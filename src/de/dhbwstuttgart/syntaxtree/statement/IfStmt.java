package de.dhbwstuttgart.syntaxtree.statement;

import de.dhbwstuttgart.syntaxtree.StatementVisitor;
import de.dhbwstuttgart.syntaxtree.type.RefTypeOrTPHOrWildcardOrGeneric;
import de.dhbwstuttgart.typeinference.assumptions.TypeInferenceBlockInformation;
import de.dhbwstuttgart.typeinference.constraints.ConstraintSet;
import de.dhbwstuttgart.typeinference.assumptions.TypeInferenceInformation;
import org.antlr.v4.runtime.Token;


public class IfStmt extends Statement
{
    public final Expression expr;
    public final Statement then_block;
    public final Statement else_block;

    public IfStmt(RefTypeOrTPHOrWildcardOrGeneric type,
                  Expression expr, Statement thenBlock, Statement elseBlock, Token offset)
    {
        super(type,offset);
        this.expr = expr;
        this.then_block = thenBlock;
        this.else_block = elseBlock;
    }

    @Override
    public void accept(StatementVisitor visitor) {
        visitor.visit(this);
    }
}
