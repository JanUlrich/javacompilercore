package de.dhbwstuttgart.syntaxtree.statement;
import java.util.List;

import de.dhbwstuttgart.syntaxtree.StatementVisitor;
import de.dhbwstuttgart.syntaxtree.type.RefTypeOrTPHOrWildcardOrGeneric;
import de.dhbwstuttgart.typeinference.assumptions.TypeInferenceBlockInformation;
import de.dhbwstuttgart.typeinference.constraints.ConstraintSet;
import de.dhbwstuttgart.exceptions.NotImplementedException;


public class NewArray extends Expression
{
    public NewArray(int offset,int variableLength)
    {
        super(null,null);
    }
    private RefTypeOrTPHOrWildcardOrGeneric type;
    public List<Expression> expr;

    @Override
    public void accept(StatementVisitor visitor) {
        visitor.visit(this);
    }
}
