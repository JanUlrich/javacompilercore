package de.dhbwstuttgart.syntaxtree.statement;

import de.dhbwstuttgart.exceptions.TypeinferenceException;
import de.dhbwstuttgart.syntaxtree.*;
import de.dhbwstuttgart.syntaxtree.type.RefType;
import de.dhbwstuttgart.syntaxtree.type.RefTypeOrTPHOrWildcardOrGeneric;
import de.dhbwstuttgart.typeinference.assumptions.TypeInferenceBlockInformation;
import de.dhbwstuttgart.typeinference.constraints.Constraint;
import de.dhbwstuttgart.typeinference.constraints.ConstraintSet;
import de.dhbwstuttgart.typeinference.assumptions.MethodAssumption;
import de.dhbwstuttgart.typeinference.constraints.Pair;
import de.dhbwstuttgart.typeinference.unify.model.PairOperator;
import org.antlr.v4.runtime.Token;

import de.dhbwstuttgart.syntaxtree.type.TypePlaceholder;

import java.util.*;


public class MethodCall extends Statement
{
    public final String name;
    public final Receiver receiver;
    public final ArgumentList arglist;

    public MethodCall(RefTypeOrTPHOrWildcardOrGeneric retType, Receiver receiver, String methodName, ArgumentList argumentList, Token offset){
		super(retType,offset);
		this.arglist = argumentList;
        this.name = methodName;
        this.receiver = receiver;
	}

    @Override
    public void accept(StatementVisitor visitor) {
        visitor.visit(this);
    }

    public ArgumentList getArgumentList() {
        return arglist;
    }
}
