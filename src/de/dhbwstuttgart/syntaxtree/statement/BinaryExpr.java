package de.dhbwstuttgart.syntaxtree.statement;


import de.dhbwstuttgart.syntaxtree.StatementVisitor;
import de.dhbwstuttgart.syntaxtree.type.RefTypeOrTPHOrWildcardOrGeneric;
import org.antlr.v4.runtime.Token;

public class BinaryExpr extends Expression
{
    @Override
    public void accept(StatementVisitor visitor) {
        visitor.visit(this);
    }

    public enum Operator{
        ADD, // +
        SUB, // -
        MUL, // *
        MOD, // Modulo Operator %
        AND, // &&
        OR,  // ||
        DIV, // /
        LESSTHAN, // <
        BIGGERTHAN, // >
        LESSEQUAL, // <=
        BIGGEREQUAL // >=
    }

    public final Operator operation;
    public final Expression lexpr;
    public final Expression rexpr;

    public BinaryExpr(Operator operation, RefTypeOrTPHOrWildcardOrGeneric type, Expression lexpr, Expression rexpr, Token offset)
    {
        super(type,offset);

        this.operation = operation;
        this.lexpr = lexpr;
        this.rexpr = rexpr;
    }

}
