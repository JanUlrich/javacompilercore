package de.dhbwstuttgart.syntaxtree.statement;

import de.dhbwstuttgart.exceptions.NotImplementedException;
import de.dhbwstuttgart.syntaxtree.StatementVisitor;
import de.dhbwstuttgart.syntaxtree.type.RefTypeOrTPHOrWildcardOrGeneric;
import de.dhbwstuttgart.typeinference.assumptions.TypeInferenceBlockInformation;
import de.dhbwstuttgart.typeinference.constraints.ConstraintSet;
import org.antlr.v4.runtime.Token;


public class CastExpr extends Expression
{
    public CastExpr(RefTypeOrTPHOrWildcardOrGeneric castType, Expression expr, Token offset)
    {
        super(castType, offset);
        this.expr = expr;
    }

    public Expression expr;

    @Override
    public void accept(StatementVisitor visitor) {
        visitor.visit(this);
    }
}
