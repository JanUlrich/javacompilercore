package de.dhbwstuttgart.syntaxtree.statement;

import de.dhbwstuttgart.syntaxtree.StatementVisitor;
import de.dhbwstuttgart.syntaxtree.type.RefTypeOrTPHOrWildcardOrGeneric;
import org.antlr.v4.runtime.Token;

public class LocalVar extends Statement{

    public final String name;

    public LocalVar(String n, RefTypeOrTPHOrWildcardOrGeneric type, Token offset)
    {
        super(type,offset);
        this.name = n;
    }

    public LocalVar(Expression e1, RefTypeOrTPHOrWildcardOrGeneric type, String access)
    {
        super(type,e1.getOffset());
        this.name = access;
    }

    @Override
    public void accept(StatementVisitor visitor) {
        visitor.visit(this);
    }
}
