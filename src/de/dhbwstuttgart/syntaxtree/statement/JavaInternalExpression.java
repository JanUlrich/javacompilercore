package de.dhbwstuttgart.syntaxtree.statement;

import de.dhbwstuttgart.syntaxtree.type.RefTypeOrTPHOrWildcardOrGeneric;
import org.antlr.v4.runtime.Token;

import java.nio.charset.StandardCharsets;

public abstract class JavaInternalExpression extends Statement{
    public JavaInternalExpression(RefTypeOrTPHOrWildcardOrGeneric retType, Token offset){
        super(retType, offset);
    }
}
