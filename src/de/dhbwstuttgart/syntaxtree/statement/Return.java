package de.dhbwstuttgart.syntaxtree.statement;

import de.dhbwstuttgart.syntaxtree.StatementVisitor;
import org.antlr.v4.runtime.Token;


public class Return extends Statement
{
    public final Expression retexpr;

    public Return(Expression retExpr, Token offset)
    {
        super(retExpr.getType(),offset);
        this.retexpr = retExpr;
    }

    @Override
    public void accept(StatementVisitor visitor) {
        visitor.visit(this);
    }
}
