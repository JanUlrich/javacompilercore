
package de.dhbwstuttgart.syntaxtree.statement;

import de.dhbwstuttgart.syntaxtree.StatementVisitor;
import de.dhbwstuttgart.typeinference.assumptions.TypeInferenceBlockInformation;
import de.dhbwstuttgart.typeinference.constraints.ConstraintSet;
import de.dhbwstuttgart.typeinference.unify.model.PairOperator;
import org.antlr.v4.runtime.Token;

/*
Aufbau:
rightSide = leftSide
 */
public class Assign extends Statement
{
    public final Expression rightSide;
    public final AssignLeftSide lefSide;

    public Assign(AssignLeftSide leftHandSide, Expression value, Token offset) {
		super(leftHandSide.getType(), offset);
        this.rightSide = value;
        this.lefSide = leftHandSide;
	}

    @Override
    public void accept(StatementVisitor visitor) {
        visitor.visit(this);
    }
}
