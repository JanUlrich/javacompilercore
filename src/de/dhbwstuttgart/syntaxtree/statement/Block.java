package de.dhbwstuttgart.syntaxtree.statement;
import java.util.*;

import de.dhbwstuttgart.syntaxtree.StatementVisitor;
import de.dhbwstuttgart.syntaxtree.type.TypePlaceholder;
import de.dhbwstuttgart.typeinference.assumptions.TypeInferenceBlockInformation;
import de.dhbwstuttgart.typeinference.constraints.ConstraintSet;
import org.antlr.v4.runtime.Token;


public class Block extends Statement
{
    public Block(List<Statement> statements, Token offset) {
		super(TypePlaceholder.fresh(offset), offset);
		this.statements = statements;
	}


    
    public List<Statement> statements = new ArrayList<>();

    public List<Statement> getStatements()
    {
        return statements;
    }

    @Override
    public void accept(StatementVisitor visitor) {
        visitor.visit(this);
    }
}


