package de.dhbwstuttgart.syntaxtree;

import de.dhbwstuttgart.syntaxtree.type.RefTypeOrTPHOrWildcardOrGeneric;
import org.antlr.v4.runtime.Token;

public class FormalParameter extends SyntaxTreeNode
{
    private RefTypeOrTPHOrWildcardOrGeneric type;
    private String name;
    
    public FormalParameter(String name, RefTypeOrTPHOrWildcardOrGeneric type, Token offset){
        super(offset);
        this.name = name;
    	this.type = type;
    }

    public RefTypeOrTPHOrWildcardOrGeneric getType() {
        return type;
    }

    public String getName() {
        return name;
    }

    @Override
    public void accept(ASTVisitor visitor) {
        visitor.visit(this);
    }
}
