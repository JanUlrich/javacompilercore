package de.dhbwstuttgart.syntaxtree;

import de.dhbwstuttgart.syntaxtree.statement.Expression;
import org.antlr.v4.runtime.Token;

import de.dhbwstuttgart.syntaxtree.type.RefTypeOrTPHOrWildcardOrGeneric;

/**
 * Eine Feldinitialisation steht fÃ¼r eine Felddeklaration mit gleichzeitiger Wertzuweisung
 * Beispiel: 'public Feld FeldVar = FeldWert;'
 * @author janulrich
 *
 */
public class FieldDeclaration extends Field{

    private Expression wert;
	
    /**
     * Dieser Konstruktor der FieldDeclaration erstellt den Syntaxknoten vollstÃ¤ndig.
     * Kein nachtrÃ¤gliches hinzfÃ¼gen von Informationen oder aufrufen von parserPostProcessing ist notwendig.
     */
    public FieldDeclaration(String name, RefTypeOrTPHOrWildcardOrGeneric typ, int modifier, Expression value, Token offset){
    	super(name, typ, modifier, offset);//Dieser Deklarator wird nicht vom Parser aufgerufen. Dadurch gibt es auch keinen Offset
    	this.wert = value;
    }


}
