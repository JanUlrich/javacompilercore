package de.dhbwstuttgart.syntaxtree;

import de.dhbwstuttgart.syntaxtree.type.RefTypeOrTPHOrWildcardOrGeneric;

import java.util.List;

public class ExceptionList
{
    private List<RefTypeOrTPHOrWildcardOrGeneric> exceptions;

    public ExceptionList(List<RefTypeOrTPHOrWildcardOrGeneric> exceptions){
        this.exceptions = exceptions;
    }
}
