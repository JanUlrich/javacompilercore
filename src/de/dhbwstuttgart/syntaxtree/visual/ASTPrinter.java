package de.dhbwstuttgart.syntaxtree.visual;

import de.dhbwstuttgart.syntaxtree.*;

public class ASTPrinter {

    public static String print(SourceFile toPrint){
        StringBuilder output = new StringBuilder();
        new OutputGenerator(output).visit(toPrint);
        return output.toString();
    }

}
