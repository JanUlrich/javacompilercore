package de.dhbwstuttgart.syntaxtree.visual;

import de.dhbwstuttgart.syntaxtree.type.*;
import de.dhbwstuttgart.typeinference.result.*;

public class ResultSetOutputGenerator extends OutputGenerator implements ResultSetVisitor{

    public ResultSetOutputGenerator(StringBuilder out) {
        super(out);
    }

    @Override
    public void visit(PairTPHsmallerTPH p) {
        print(p, "<");
    }

    @Override
    public void visit(PairTPHequalRefTypeOrWildcardType p) {
        print(p, "=.");
    }

    @Override
    public void visit(PairTPHEqualTPH p) {
        print(p, "=.");
    }

    private void print(ResultPair p , String operator){
        out.append("(");
        p.getLeft().accept((ResultSetVisitor) this);
        out.append(" "+operator+" ");
        p.getRight().accept((ResultSetVisitor) this);
        out.append(")");
    }
}
