package de.dhbwstuttgart.syntaxtree.visual;

import de.dhbwstuttgart.syntaxtree.*;

public class ASTTypePrinter extends ASTPrinter{

    public static String print(SourceFile toPrint){
        StringBuilder output = new StringBuilder();
        new TypeOutputGenerator(output).visit(toPrint);
        return output.toString();
    }

}
