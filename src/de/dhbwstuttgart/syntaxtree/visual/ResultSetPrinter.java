package de.dhbwstuttgart.syntaxtree.visual;

import de.dhbwstuttgart.syntaxtree.SourceFile;
import de.dhbwstuttgart.typeinference.result.ResultPair;
import de.dhbwstuttgart.typeinference.result.ResultSet;

import java.util.Set;

public class ResultSetPrinter {

    public static String print(ResultSet toPrint){
        StringBuilder output = new StringBuilder();
        for(ResultPair p : toPrint.results){
            p.accept(new ResultSetOutputGenerator(output));
            output.append("\n");
        }
        return output.toString();
    }

}
