package de.dhbwstuttgart.syntaxtree.visual;

import de.dhbwstuttgart.syntaxtree.*;
import de.dhbwstuttgart.syntaxtree.statement.*;
import de.dhbwstuttgart.syntaxtree.statement.Literal;
import de.dhbwstuttgart.syntaxtree.type.*;

public class TypeOutputGenerator extends OutputGenerator {

    TypeOutputGenerator(StringBuilder out){
        super(out);
    }

    @Override
    public void visit(SourceFile sourceFile) {
        super.visit(sourceFile);
    }

    @Override
    public void visit(ArgumentList argumentList) {
        super.visit(argumentList);
    }

    @Override
    public void visit(GenericTypeVar genericTypeVar) {
        super.visit(genericTypeVar);
    }

    @Override
    public void visit(FormalParameter formalParameter) {
        super.visit(formalParameter);
    }

    @Override
    public void visit(GenericDeclarationList genericTypeVars) {
        super.visit(genericTypeVars);
    }

    @Override
    public void visit(Field field) {
        super.visit(field);
    }

    @Override
    public void visit(Method method) {
        super.visit(method);
    }

    @Override
    public void visit(ParameterList formalParameters) {
        super.visit(formalParameters);
    }

    @Override
    public void visit(ClassOrInterface classOrInterface) {
        super.visit(classOrInterface);
    }

    @Override
    public void visit(RefType refType) {
        super.visit(refType);
    }

    @Override
    public void visit(SuperWildcardType superWildcardType) {
        super.visit(superWildcardType);
    }

    @Override
    public void visit(TypePlaceholder typePlaceholder) {
        super.visit(typePlaceholder);
    }

    @Override
    public void visit(ExtendsWildcardType extendsWildcardType) {
        super.visit(extendsWildcardType);
    }

    @Override
    public void visit(GenericRefType genericRefType) {
        super.visit(genericRefType);
    }

    @Override
    public void visit(LambdaExpression lambdaExpression) {
        out.append("(");
        super.visit(lambdaExpression);
        out.append(")");
        this.out.append("::");
        lambdaExpression.getType().accept(this);
    }

    @Override
    public void visit(Assign assign) {
        super.visit(assign);
    }

    @Override
    public void visit(BinaryExpr binary) {
        binary.lexpr.accept(this);
        out.append(" | ");
        binary.rexpr.accept(this);
    }

    @Override
    public void visit(Block block) {
        out.append("(");
        super.visit(block);
        out.append(")");
        this.out.append("::");
        block.getType().accept(this);
    }

    @Override
    public void visit(CastExpr castExpr) {
        super.visit(castExpr);
    }

    @Override
    public void visit(EmptyStmt emptyStmt) {
        super.visit(emptyStmt);
    }

    @Override
    public void visit(FieldVar fieldVar) {
        out.append("(");
        super.visit(fieldVar);
        out.append(")");
        this.out.append("::");
        fieldVar.getType().accept(this);
    }

    @Override
    public void visit(ForStmt forStmt) {
        super.visit(forStmt);
    }

    @Override
    public void visit(IfStmt ifStmt) {
        super.visit(ifStmt);
    }

    @Override
    public void visit(InstanceOf instanceOf) {
        super.visit(instanceOf);
    }

    @Override
    public void visit(LocalVar localVar) {
        out.append("(");
        super.visit(localVar);
        out.append(")");
        this.out.append("::");
        localVar.getType().accept(this);
    }

    @Override
    public void visit(LocalVarDecl localVarDecl) {
        super.visit(localVarDecl);
    }

    @Override
    public void visit(MethodCall methodCall) {
        out.append("(");
        super.visit(methodCall);
        out.append(")");
        this.out.append("::");
        methodCall.getType().accept(this);
    }

    @Override
    public void visit(NewClass methodCall) {
        super.visit(methodCall);
    }

    @Override
    public void visit(NewArray newArray) {
        super.visit(newArray);
    }

    @Override
    public void visit(ExpressionReceiver receiver) {
        super.visit(receiver);
    }

    @Override
    public void visit(Return aReturn) {
        super.visit(aReturn);
    }

    @Override
    public void visit(ReturnVoid aReturn) {
        super.visit(aReturn);
    }

    @Override
    public void visit(StaticClassName staticClassName) {
        super.visit(staticClassName);
    }

    @Override
    public void visit(Super aSuper) {
        super.visit(aSuper);
    }

    @Override
    public void visit(This aThis) {
        out.append("(");
        super.visit(aThis);
        out.append(")");
        this.out.append("::");
        aThis.getType().accept(this);
    }

    @Override
    public void visit(WhileStmt whileStmt) {
        super.visit(whileStmt);
    }

    @Override
    public void visit(Literal literal) {
        super.visit(literal);
    }
}