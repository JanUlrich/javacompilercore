package de.dhbwstuttgart.syntaxtree.visual;

import de.dhbwstuttgart.syntaxtree.type.*;

public class TypePrinter implements TypeVisitor<String> {
    @Override
    public String visit(RefType refType) {
        return refType.toString();
    }

    @Override
    public String visit(SuperWildcardType superWildcardType) {
        return "? super " + superWildcardType.getInnerType().acceptTV(this);
    }

    @Override
    public String visit(TypePlaceholder typePlaceholder) {
        return "TPH " + typePlaceholder.getName();
    }

    @Override
    public String visit(ExtendsWildcardType extendsWildcardType) {
        return "? extends " + extendsWildcardType.getInnerType().acceptTV(this);
    }

    @Override
    public String visit(GenericRefType genericRefType) {
        return genericRefType.getParsedName();
    }
}
