package de.dhbwstuttgart.syntaxtree;

import de.dhbwstuttgart.syntaxtree.type.RefTypeOrTPHOrWildcardOrGeneric;

import java.util.Collection;


public interface TypeScope {
    Iterable<? extends GenericTypeVar> getGenerics();

    RefTypeOrTPHOrWildcardOrGeneric getReturnType();
}
