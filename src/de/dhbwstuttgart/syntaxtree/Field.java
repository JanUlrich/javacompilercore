package de.dhbwstuttgart.syntaxtree;

import de.dhbwstuttgart.syntaxtree.type.RefTypeOrTPHOrWildcardOrGeneric;
import org.antlr.v4.runtime.Token;

import java.util.ArrayList;

public class Field extends SyntaxTreeNode implements TypeScope{

	public final int modifier;
	private String name;
	private RefTypeOrTPHOrWildcardOrGeneric type;
    
	public Field(String name, RefTypeOrTPHOrWildcardOrGeneric type, int modifier, Token offset){
		super(offset);
		this.name = name;
		this.type = type;
		this.modifier = modifier;
	}

  	public String getName(){
    return this.name;
  }

	public RefTypeOrTPHOrWildcardOrGeneric getType() {
		return type;
	}

	@Override
	public void accept(ASTVisitor visitor) {
		visitor.visit(this);
	}

	@Override
	public Iterable<? extends GenericTypeVar> getGenerics() {
		return new ArrayList<>();
	}

	@Override
	public RefTypeOrTPHOrWildcardOrGeneric getReturnType() {
		return type;
	}
}

