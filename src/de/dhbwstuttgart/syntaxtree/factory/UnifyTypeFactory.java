package de.dhbwstuttgart.syntaxtree.factory;

import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import de.dhbwstuttgart.exceptions.DebugException;
import de.dhbwstuttgart.exceptions.NotImplementedException;
import de.dhbwstuttgart.parser.NullToken;
import de.dhbwstuttgart.parser.SyntaxTreeGenerator.FCGenerator;
import de.dhbwstuttgart.parser.scope.JavaClassName;
import de.dhbwstuttgart.sat.asp.model.ASPRule;
import de.dhbwstuttgart.sat.asp.parser.ASPParser;
import de.dhbwstuttgart.syntaxtree.ClassOrInterface;
import de.dhbwstuttgart.syntaxtree.type.*;
import de.dhbwstuttgart.syntaxtree.type.Void;
import de.dhbwstuttgart.syntaxtree.type.WildcardType;
import de.dhbwstuttgart.typeinference.constraints.Constraint;
import de.dhbwstuttgart.typeinference.constraints.ConstraintSet;
import de.dhbwstuttgart.typeinference.constraints.Pair;
import de.dhbwstuttgart.typeinference.result.PairTPHEqualTPH;
import de.dhbwstuttgart.typeinference.result.PairTPHequalRefTypeOrWildcardType;
import de.dhbwstuttgart.typeinference.result.PairTPHsmallerTPH;
import de.dhbwstuttgart.typeinference.result.ResultPair;
import de.dhbwstuttgart.typeinference.unify.model.*;

public class UnifyTypeFactory {

    public static FiniteClosure generateFC(List<ClassOrInterface> fromClasses) throws ClassNotFoundException {
        /*
        Die transitive Hülle muss funktionieren.
        Man darf schreiben List<A> extends AL<A>
        und Vector<B> extends List<B>
        hier muss dann aber dennoch die Vererbung V < L < AL
        hergestellt werden.
        In einem solchen Vererbungsbaum dürfen die TPH auch die gleichen Namen haben.
        Generell dürfen sie immer die gleichen Namen haben.
        TODO: die transitive Hülle bilden
         */
        return new FiniteClosure(FCGenerator.toUnifyFC(fromClasses));
    }

    public static UnifyPair generateSmallerPair(UnifyType tl, UnifyType tr){
        return new UnifyPair(tl, tr, PairOperator.SMALLER);
    }

    public static UnifyPair generateSmallerDotPair(UnifyType tl, UnifyType tr){
        return new UnifyPair(tl, tr, PairOperator.SMALLERDOT);
    }

    public static UnifyPair generateEqualDotPair(UnifyType tl, UnifyType tr){
        return new UnifyPair(tl, tr, PairOperator.EQUALSDOT);
    }

    /**
     * Convert from
     * ASTType -> UnifyType
     */
    public static UnifyType convert(RefTypeOrTPHOrWildcardOrGeneric t){
        if(t instanceof GenericRefType){
            return UnifyTypeFactory.convert((GenericRefType)t);
        }else
        if(t instanceof FunN){
            return UnifyTypeFactory.convert((FunN)t);
        }else if(t instanceof TypePlaceholder){
            return UnifyTypeFactory.convert((TypePlaceholder)t);
        }else if(t instanceof ExtendsWildcardType){
            return UnifyTypeFactory.convert((ExtendsWildcardType)t);
        }else if(t instanceof SuperWildcardType){
            return UnifyTypeFactory.convert((SuperWildcardType)t);
        }else if(t instanceof RefType){
            return UnifyTypeFactory.convert((RefType)t);
        }
        //Es wurde versucht ein Typ umzuwandeln, welcher noch nicht von der Factory abgedeckt ist
        throw new NotImplementedException("Der Typ "+t+" kann nicht umgewandelt werden");
    }

    public static UnifyType convert(RefType t){
        //Check if it is a FunN Type:
        Pattern p = Pattern.compile("Fun(\\d+)");
        Matcher m = p.matcher(t.getName().toString());
        boolean b = m.matches();
        if(b){
            Integer N = Integer.valueOf(m.group(1));
            if((N + 1) == t.getParaList().size()){
                return convert(new FunN(t.getParaList()));
            }
        }
        UnifyType ret;
        if(t.getParaList() != null && t.getParaList().size() > 0){
            List<UnifyType> params = new ArrayList<>();
            for(RefTypeOrTPHOrWildcardOrGeneric pT : t.getParaList()){
                params.add(UnifyTypeFactory.convert(pT));
            }
            ret = new ReferenceType(t.getName().toString(),new TypeParams(params));
        }else{
            ret = new ReferenceType(t.getName().toString());
        }
        return ret;
    }

    public static UnifyType convert(FunN t){
        UnifyType ret;
        List<UnifyType> params = new ArrayList<>();
        if(t.getParaList() != null && t.getParaList().size() > 0){
            for(RefTypeOrTPHOrWildcardOrGeneric pT : t.getParaList()){
                params.add(UnifyTypeFactory.convert(pT));
            }
        }
        ret = FunNType.getFunNType(new TypeParams(params));
        return ret;
    }

    public static UnifyType convert(TypePlaceholder tph){
        return new PlaceholderType(tph.getName());
    }

    public static UnifyType convert(GenericRefType t){
        return new ReferenceType(t.getParsedName());
    }

    public static UnifyType convert(WildcardType t){
        if(t.isExtends())
            return new ExtendsType(UnifyTypeFactory.convert(t.getInnerType()));
        else if(t.isSuper())
            return new SuperType(UnifyTypeFactory.convert(t.getInnerType()));
        else throw new NotImplementedException();
    }


    public static ConstraintSet<UnifyPair> convert(ConstraintSet<Pair> constraints) {
        return constraints.map(UnifyTypeFactory::convert);
    }

    public static Constraint<UnifyPair> convert(Constraint<Pair> constraint){
        return constraint.stream().map(UnifyTypeFactory::convert).collect(Collectors.toCollection(Constraint::new));
    }

    public static UnifyPair convert(Pair p) {
        if(p.GetOperator().equals(PairOperator.SMALLERDOT)) {
            UnifyPair ret = generateSmallerDotPair(UnifyTypeFactory.convert(p.TA1)
                    , UnifyTypeFactory.convert(p.TA2));
            return ret;
        }else if(p.GetOperator().equals(PairOperator.EQUALSDOT)) {
            UnifyPair ret = generateEqualDotPair(UnifyTypeFactory.convert(p.TA1)
                    , UnifyTypeFactory.convert(p.TA2));
            return ret;
        }else if(p.GetOperator().equals(PairOperator.SMALLER)){
            return generateSmallerPair(UnifyTypeFactory.convert(p.TA1),
                    UnifyTypeFactory.convert(p.TA2));
        }else            throw new NotImplementedException();
    }

    /**
     * Convert from
     * UnifyType -> ASTType
     */
    public static Set<ResultPair> convert(Set<UnifyPair> unifyPairSet, Map<String,TypePlaceholder> tphs) {
        return unifyPairSet.stream().map(
                unifyPair -> convert(unifyPair, tphs))
                        .collect(Collectors.toSet());
    }

    public static ResultPair convert(UnifyPair mp, Map<String,TypePlaceholder> tphs) {
        RefTypeOrTPHOrWildcardOrGeneric tl = UnifyTypeFactory.convert(mp.getLhsType(), tphs);
        RefTypeOrTPHOrWildcardOrGeneric tr = UnifyTypeFactory.convert(mp.getRhsType(), tphs);
        if(tl instanceof TypePlaceholder){
            if(tr instanceof TypePlaceholder) {

                if(mp.getPairOp().equals(PairOperator.EQUALSDOT)) {
                    return new PairTPHEqualTPH((TypePlaceholder)tl, (TypePlaceholder)tr);
                    //Einfach ignorieren TODO: Das hier muss ausgebessert werden:
                    //return new PairTPHequalRefTypeOrWildcardType((TypePlaceholder)tl, ASTFactory.createObjectType());
                }else{
                    return new PairTPHsmallerTPH((TypePlaceholder)tl, (TypePlaceholder)tr);
                }
            }else if(tr instanceof RefType){
                return new PairTPHequalRefTypeOrWildcardType((TypePlaceholder)tl, (RefType) tr);
            }else if(tr instanceof WildcardType){
                return new PairTPHequalRefTypeOrWildcardType((TypePlaceholder)tl, (WildcardType) tr);
            }else throw new NotImplementedException();
        }else throw new NotImplementedException();
    }

    public static RefTypeOrTPHOrWildcardOrGeneric convert(ReferenceType t, Map<String,TypePlaceholder> tphs) {
        if(JavaClassName.Void.equals(t.getName()))return new Void(new NullToken());
        RefType ret = new RefType(new JavaClassName(t.getName()),convert(t.getTypeParams(), tphs),new NullToken());
        return ret;
    }

    public static RefTypeOrTPHOrWildcardOrGeneric convert(FunNType t, Map<String,TypePlaceholder> tphs) {
        RefType ret = new RefType(new JavaClassName(t.getName()), convert(t.getTypeParams(), tphs), new NullToken());
        return ret;
    }

    public static RefTypeOrTPHOrWildcardOrGeneric convert(SuperType t, Map<String,TypePlaceholder> tphs) {
        RefTypeOrTPHOrWildcardOrGeneric innerType = convert(t.getSuperedType(), tphs);
        return new SuperWildcardType(innerType, new NullToken());
    }

    public static RefTypeOrTPHOrWildcardOrGeneric convert(ExtendsType t, Map<String,TypePlaceholder> tphs) {
        RefTypeOrTPHOrWildcardOrGeneric innerType = convert(t.getExtendedType(), tphs);
        return new ExtendsWildcardType(innerType, new NullToken());
    }

    public static RefTypeOrTPHOrWildcardOrGeneric convert(PlaceholderType t, Map<String,TypePlaceholder> tphs) {
        TypePlaceholder ret = tphs.get(t.getName());
        if(ret == null){ //Dieser TPH wurde vom Unifikationsalgorithmus erstellt
            ret = TypePlaceholder.fresh(new NullToken());
            tphs.put(t.getName(), ret);
        }
        return ret;
    }

    public static RefTypeOrTPHOrWildcardOrGeneric convert(UnifyType t, Map<String,TypePlaceholder> tphs) {
        if(t instanceof FunNType)return convert((FunNType) t, tphs);
        if(t instanceof ReferenceType)return convert((ReferenceType) t, tphs);
        if(t instanceof SuperType)return convert((SuperType) t, tphs);
        if(t instanceof ExtendsType)return convert((ExtendsType) t, tphs);
        if(t instanceof PlaceholderType)return convert((PlaceholderType) t, tphs);
        throw new NotImplementedException("Der Typ "+t+" kann nicht umgewandelt werden");
    }

    private static List<RefTypeOrTPHOrWildcardOrGeneric> convert(TypeParams typeParams, Map<String,TypePlaceholder> tphs) {
        List<RefTypeOrTPHOrWildcardOrGeneric> ret = new ArrayList<>();
        for(UnifyType uT : typeParams){
            RefTypeOrTPHOrWildcardOrGeneric toAdd = convert(uT, tphs);
            ret.add(toAdd);
        }
        return ret;
    }

}
