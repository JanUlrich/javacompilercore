package de.dhbwstuttgart.syntaxtree.factory;

public class NameGenerator {

    private static String strNextName = "A";
    
    /**
     * Berechnet einen neuen, eindeutigen Namen fÃ¯Â¿Â½r eine neue                          
     * <code>TypePlaceholder</code>. <br>Author: JÃ¯Â¿Â½rg BÃ¯Â¿Â½uerle
     * @return Der Name
     */
    public static String makeNewName()
    {
        // otth: Funktion berechnet einen neuen Namen anhand eines alten gespeicherten
        String strReturn = strNextName;
    
        // nÃ¯Â¿Â½chster Name berechnen und in strNextName speichern        
        inc( strNextName.length() - 1 );
        
        return strReturn;        
    }

    /**
     * Hilfsfunktion zur Berechnung eines neuen Namens
     * <br>Author: JÃ¯Â¿Â½rg BÃ¯Â¿Â½uerle
     * @param i
     */
    private static void inc(int i)
    {
        // otth: Hilfsfunktion zur Berechnung eines neuen Namens
        // otth: ErhÃ¯Â¿Â½hung des Buchstabens an der Stelle i im String strNextName
        // otth: Nach Ã¯Â¿Â½berlauf: rekursiver Aufruf
    
        // falls i = -1 --> neuer Buchstabe vorne anfÃ¯Â¿Â½gen
        if ( i == -1 )
        {
            strNextName = "A" + strNextName;
            return;
        }
    
        char cBuchstabe = (char)(strNextName.charAt( i ));
        cBuchstabe++;
        if ( cBuchstabe - 65 > 25 )
        {
            // aktuelle Stelle: auf A zuruecksetzen
            manipulate( i, 'A' );
            
            // vorherige Stelle erhÃ¯Â¿Â½hen
            inc( i - 1 );
        }
        else
        {
            // aktueller Buchstabe Ã¯Â¿Â½ndern
            manipulate( i, cBuchstabe );
        }
        
    }

    /**
     * Hilfsfunktion zur Berechnung eines neuen Namens.
     * <br>Author: JÃ¯Â¿Â½rg BÃ¯Â¿Â½uerle
     * @param nStelle
     * @param nWert
     */
    private static void manipulate( int nStelle, char nWert )
    {
        // otth: Hilfsfunktion zur Berechnung eines neuen Namens
        // otth: Ersetzt im String 'strNextName' an der Position 'nStelle' den Buchstaben durch 'nWert'
        
        String strTemp = "";
        for( int i = 0; i < strNextName.length(); i++)
        {
            if ( i == nStelle )
                strTemp = strTemp + nWert;
            else
                strTemp = strTemp + strNextName.charAt( i );
        }
        strNextName = strTemp;
    }
    
}
