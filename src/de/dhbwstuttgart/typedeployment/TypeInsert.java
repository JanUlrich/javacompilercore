package de.dhbwstuttgart.typedeployment;

import org.antlr.v4.runtime.Token;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class TypeInsert {
    /**
     * point wird hauptsächlich zur Anzeige einer Annotation im Eclipse-plugin benutzt.
     */
    public final TypeInsertPoint point;
    Set<TypeInsertPoint> inserts;

    public TypeInsert(TypeInsertPoint point, Set<TypeInsertPoint> additionalPoints){
        this.point = point;
        inserts = additionalPoints;
    }

    public String insert(String intoSource){
        List<TypeInsertPoint> offsets = new ArrayList<>();
        String ret = point.insert(intoSource, offsets);
        offsets.add(point);
        for(TypeInsertPoint insertPoint : inserts){
            ret = insertPoint.insert(ret, offsets);
            offsets.add(insertPoint);
        }
        return ret;
    }

    public String getInsertString(){
        return point.getInsertString();
    }
}
