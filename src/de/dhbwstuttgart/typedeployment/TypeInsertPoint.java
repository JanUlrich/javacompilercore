package de.dhbwstuttgart.typedeployment;

import org.antlr.v4.runtime.Token;

import java.util.List;
import java.util.stream.Collectors;

public class TypeInsertPoint {
    public final Token point;
    private String insertString;

    public TypeInsertPoint(Token point, String toInsert){
        this.point = point;
        this.insertString = (toInsert.endsWith(" ")) ? toInsert : toInsert + " " ;
    }

    public String insert(String intoSource, List<TypeInsertPoint> additionalOffset){
        int offset = additionalOffset.stream().filter((token ->
                //token.point.getLine() != point.getLine() && token.point.getCharPositionInLine() <= point.getCharPositionInLine()))
                token.point.getStartIndex() <= point.getStartIndex()))
                .mapToInt((typeInsertPoint -> typeInsertPoint.insertString.length())).sum();
        return new StringBuilder(intoSource).insert(point.getStartIndex()+offset, insertString).toString();
    }

    public String getInsertString() {
        return insertString;
    }
}
