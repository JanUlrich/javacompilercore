package de.dhbwstuttgart.parser.SyntaxTreeGenerator;

import de.dhbwstuttgart.exceptions.NotImplementedException;
import de.dhbwstuttgart.parser.NullToken;
import de.dhbwstuttgart.syntaxtree.AbstractASTWalker;
import de.dhbwstuttgart.syntaxtree.Constructor;
import de.dhbwstuttgart.syntaxtree.statement.*;

import java.util.List;

public class SyntacticSugar {

    public static List<Statement> addTrailingReturn(List<Statement> statements){
        if(statements.size()!=0) {
            Statement lastStmt = statements.get(statements.size() - 1);
            ReturnFinder hasReturn = new ReturnFinder();
            lastStmt.accept(hasReturn);
            if(hasReturn.hasReturn)return statements;
        }
        statements.add(new ReturnVoid(new NullToken()));
        return statements;
    }

    private static class ReturnFinder extends AbstractASTWalker{
        public boolean hasReturn = false;
        @Override
        public void visit(Return aReturn) {
            hasReturn = true;
        }

        @Override
        public void visit(ReturnVoid aReturn) {
            hasReturn = true;
        }
    }

    private static boolean hasReturn(Block block){
        for(Statement s : block.getStatements())
            if(s instanceof Return)return true;
        return false;
    }

}
