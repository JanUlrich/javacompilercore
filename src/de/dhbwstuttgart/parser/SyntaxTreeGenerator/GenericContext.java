package de.dhbwstuttgart.parser.SyntaxTreeGenerator;

import de.dhbwstuttgart.parser.scope.JavaClassName;

public class GenericContext {
    private final String parentMethod;
    private final JavaClassName parentClass;

    public GenericContext(JavaClassName parentClass, String parentMethod) {
        if(parentMethod == null)parentMethod = "";
        this.parentClass = parentClass;
        this.parentMethod = parentMethod;
    }
}
