package de.dhbwstuttgart.parser.SyntaxTreeGenerator;

import de.dhbwstuttgart.syntaxtree.StatementVisitor;
import de.dhbwstuttgart.syntaxtree.statement.AssignLeftSide;
import de.dhbwstuttgart.syntaxtree.statement.Expression;
import de.dhbwstuttgart.syntaxtree.statement.LocalVar;

public class AssignToLocal extends AssignLeftSide {
    public final LocalVar localVar;

    public AssignToLocal(LocalVar leftSide) {
        super(leftSide.getType(), leftSide.getOffset());
        localVar = leftSide;
    }

    @Override
    public void accept(StatementVisitor visitor) {
        visitor.visit(this);
    }
}
