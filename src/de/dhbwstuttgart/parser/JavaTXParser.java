package de.dhbwstuttgart.parser;

import de.dhbwstuttgart.environment.CompilationEnvironment;
import de.dhbwstuttgart.parser.SyntaxTreeGenerator.SyntaxTreeGenerator;
import de.dhbwstuttgart.parser.antlr.Java8Lexer;
import de.dhbwstuttgart.parser.antlr.Java8Parser;
import de.dhbwstuttgart.parser.scope.JavaClassRegistry;
import de.dhbwstuttgart.syntaxtree.SourceFile;

import org.antlr.v4.runtime.ANTLRInputStream;
import org.antlr.v4.runtime.CommonTokenStream;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

public class JavaTXParser {	
    public static Java8Parser.CompilationUnitContext parse(File source) throws IOException, java.lang.ClassNotFoundException {
        InputStream stream = new FileInputStream(source);
        ANTLRInputStream input = new ANTLRInputStream(stream);
        Java8Lexer lexer = new Java8Lexer(input);
        CommonTokenStream tokens = new CommonTokenStream(lexer);
        Java8Parser parser = new Java8Parser(tokens);
        return parser.compilationUnit();
        /*
        SyntaxTreeGenerator generator = new SyntaxTreeGenerator(environment.getRegistry(source));
        return generator.convert(tree);
        */
    }
    
    /* Für das Typsystem ist es notwendig, dass sich der Source in einer Datei befindet:
    public SourceFile parse(String fileContent) throws IOException, java.lang.ClassNotFoundException {
        return this.parse(new ByteArrayInputStream(fileContent.getBytes(StandardCharsets.UTF_8)));
    }
    */
}
