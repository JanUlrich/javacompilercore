package de.dhbwstuttgart.parser.scope;

import de.dhbwstuttgart.exceptions.NotImplementedException;

import java.util.*;

/**
 * Speichert die Klassen f�r einen bestimmten Projektscope
 */
public class JavaClassRegistry {
    final Map<JavaClassName, Integer> existingClasses = new HashMap<>();

    public JavaClassRegistry(Map<String, Integer> initialNames){
        for(String name : initialNames.keySet()){
            existingClasses.put(new JavaClassName(name), initialNames.get(name));
        }
    }

    public JavaClassName getName(String className) {
        for(JavaClassName name : existingClasses.keySet()){
            if(name.equals(new JavaClassName(className)))return name;
        }
        throw new NotImplementedException();
    }

    @Override
    public String toString(){
        return existingClasses.toString();
    }

    public List<JavaClassName> getAllFromPackage(String packageName) {
        List<JavaClassName> ret = new ArrayList<>();
        for(JavaClassName className : this.existingClasses.keySet()){
            JavaClassName toCompare = new JavaClassName(packageName + "." + JavaClassName.stripClassName(className.toString()));
            if(toCompare.toString().equals(className.toString())){
                ret.add(className);
            }
        }
        return ret;
    }

    public boolean contains(String whole) {
        return existingClasses.containsKey(new JavaClassName(whole));
    }

    public int getNumberOfGenerics(String name) {
        return existingClasses.get(new JavaClassName(name));
    }
}
