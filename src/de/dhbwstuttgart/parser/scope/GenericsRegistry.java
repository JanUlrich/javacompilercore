package de.dhbwstuttgart.parser.scope;

import de.dhbwstuttgart.parser.SyntaxTreeGenerator.GenericContext;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class GenericsRegistry  {
    private final List<GenericVariable> registry = new ArrayList<>();
    public final GenericsRegistry globalRegistry;

    public GenericsRegistry(GenericsRegistry globalRegistry){
        this.globalRegistry = globalRegistry;
    }

    public void put(String name, GenericContext genericContext){
        registry.add(new GenericVariable(genericContext,name));
        if(globalRegistry != null)globalRegistry.put(name, genericContext);
    }

    public boolean contains(String name) {
        Optional<Boolean> ret = registry.stream().<Boolean>map(((GenericVariable genericVariable) -> genericVariable.name.equals(name)))
                .reduce(((a, b) -> a || b));
        if(ret.isPresent())
            return ret.get();
        return false;
    }

    public GenericContext get(String name) {
        return registry.stream()
                .filter((genericVariable -> genericVariable.name.equals(name))).findAny().get().context;
    }

    public void putAll(GenericsRegistry generics) {
        for(GenericVariable generic : generics.registry){
            this.put(generic.name, generic.context);
        }
    }
}

class GenericVariable{
    final GenericContext context;
    final String name;

    GenericVariable(GenericContext context, String name){
        this.context = context;
        this.name = name;
    }
}