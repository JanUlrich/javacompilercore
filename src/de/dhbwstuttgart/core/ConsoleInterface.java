package de.dhbwstuttgart.core;

import java.io.File;
import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

public class ConsoleInterface {
	private static final String directory = System.getProperty("user.dir");

	public static void main(String[] args) throws IOException, ClassNotFoundException {
		List<File> input = Arrays.asList(args).stream().map((s -> new File(s))).collect(Collectors.toList());
		JavaTXCompiler compiler = new JavaTXCompiler(input);
		compiler.typeInference();
	}
}
