package de.dhbwstuttgart.core;


import org.antlr.v4.runtime.Token;

public interface IItemWithOffset
{
    Token getOffset();
}
