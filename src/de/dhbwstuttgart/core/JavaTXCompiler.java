package de.dhbwstuttgart.core;


import de.dhbwstuttgart.environment.CompilationEnvironment;
import de.dhbwstuttgart.parser.JavaTXParser;
import de.dhbwstuttgart.parser.scope.GenericsRegistry;
import de.dhbwstuttgart.parser.SyntaxTreeGenerator.SyntaxTreeGenerator;
import de.dhbwstuttgart.parser.antlr.Java8Parser.CompilationUnitContext;
import de.dhbwstuttgart.parser.scope.JavaClassName;
import de.dhbwstuttgart.sat.asp.ASPUnify;
import de.dhbwstuttgart.sat.asp.parser.ASPParser;
import de.dhbwstuttgart.sat.asp.writer.ASPFactory;
import de.dhbwstuttgart.syntaxtree.ClassOrInterface;
import de.dhbwstuttgart.syntaxtree.SourceFile;
import de.dhbwstuttgart.syntaxtree.factory.ASTFactory;
import de.dhbwstuttgart.syntaxtree.factory.UnifyTypeFactory;
import de.dhbwstuttgart.syntaxtree.type.*;
import de.dhbwstuttgart.typeinference.constraints.Constraint;
import de.dhbwstuttgart.typeinference.constraints.ConstraintSet;
import de.dhbwstuttgart.typeinference.constraints.Pair;
import de.dhbwstuttgart.typeinference.result.ResultSet;
import de.dhbwstuttgart.typeinference.typeAlgo.TYPE;
import de.dhbwstuttgart.typeinference.unify.TypeUnify;
import de.dhbwstuttgart.typeinference.unify.model.FiniteClosure;
import de.dhbwstuttgart.typeinference.unify.model.UnifyPair;

import java.io.*;
import java.util.*;
import java.util.stream.Collectors;

public class JavaTXCompiler {

    final CompilationEnvironment environment;
    public final Map<File, SourceFile> sourceFiles = new HashMap<>();

    public JavaTXCompiler(File sourceFile) throws IOException, ClassNotFoundException {
        this(Arrays.asList(sourceFile));
    }

    public JavaTXCompiler(List<File> sources) throws IOException, ClassNotFoundException {
        environment = new CompilationEnvironment(sources);
        for (File s : sources) {
            sourceFiles.put(s, parse(s));
        }
    }

    public ConstraintSet<Pair> getConstraints() throws ClassNotFoundException {
        List<ClassOrInterface> allClasses = new ArrayList<>();//environment.getAllAvailableClasses();
        for (SourceFile sf : sourceFiles.values()) {
            allClasses.addAll(sf.getClasses());
        }
        List<ClassOrInterface> importedClasses = new ArrayList<>();
        //Alle Importierten Klassen in allen geparsten Sourcefiles kommen ins FC
        for (File forSourceFile : sourceFiles.keySet())
            for (JavaClassName name : sourceFiles.get(forSourceFile).getImports()) {
                //TODO: Hier werden imports von eigenen (.jav) Klassen nicht beachtet
                ClassOrInterface importedClass = ASTFactory.createClass(
                        ClassLoader.getSystemClassLoader().loadClass(name.toString()));
                importedClasses.add(importedClass);
            }
        allClasses.addAll(importedClasses);

        return new TYPE(sourceFiles.values(), allClasses).getConstraints();
    }

    public List<ClassOrInterface> getAvailableClasses(SourceFile forSourceFile) throws ClassNotFoundException {
        List<ClassOrInterface> allClasses = new ArrayList<>();//environment.getAllAvailableClasses();
        for (SourceFile sf : sourceFiles.values()) {
            allClasses.addAll(sf.getClasses());
        }
        List<ClassOrInterface> importedClasses = new ArrayList<>();
        for (JavaClassName name : forSourceFile.getImports()) {
            //TODO: Hier werden imports von eigenen (.jav) Klassen nicht beachtet
            ClassOrInterface importedClass = ASTFactory.createClass(
                    ClassLoader.getSystemClassLoader().loadClass(name.toString()));
            importedClasses.add(importedClass);
            allClasses.addAll(importedClasses);
        }
        return allClasses;
    }

    public List<ResultSet> typeInference() throws ClassNotFoundException {
        List<ClassOrInterface> allClasses = new ArrayList<>();//environment.getAllAvailableClasses();
        //Alle Importierten Klassen in allen geparsten Sourcefiles kommen ins FC
        for(SourceFile sf : this.sourceFiles.values()) {
            allClasses.addAll(getAvailableClasses(sf));
            allClasses.addAll(sf.getClasses());
        }

        final ConstraintSet<Pair> cons = getConstraints();

        FiniteClosure finiteClosure = UnifyTypeFactory.generateFC(allClasses);
        System.out.println(finiteClosure);
        ConstraintSet<UnifyPair> unifyCons = UnifyTypeFactory.convert(cons);

        TypeUnify unify = new TypeUnify();
        Set<Set<UnifyPair>> results = new HashSet<>();
        for (List<Constraint<UnifyPair>> xCons : unifyCons.cartesianProduct()) {
            Set<UnifyPair> xConsSet = new HashSet<>();
            for (Constraint<UnifyPair> constraint : xCons) {
                xConsSet.addAll(constraint);
            }

            System.out.println(xConsSet);
            Set<Set<UnifyPair>> result = unify.unify(xConsSet, finiteClosure);
            System.out.println("RESULT: " + result.size());
            results.addAll(result);
        }
        return results.stream().map((unifyPairs ->
                new ResultSet(UnifyTypeFactory.convert(unifyPairs, generateTPHMap(cons))))).collect(Collectors.toList());
    }

    private Map<String, TypePlaceholder> generateTPHMap(ConstraintSet<Pair> constraints) {
        HashMap<String, TypePlaceholder> ret = new HashMap<>();
        constraints.map((Pair p) -> {
            if (p.TA1 instanceof TypePlaceholder) {
                ret.put(((TypePlaceholder) p.TA1).getName(), (TypePlaceholder) p.TA1);
            }
            if (p.TA2 instanceof TypePlaceholder) {
                ret.put(((TypePlaceholder) p.TA2).getName(), (TypePlaceholder) p.TA2);
            }
            return null;
        });
        return ret;
    }

    private SourceFile parse(File sourceFile) throws IOException, java.lang.ClassNotFoundException {
        CompilationUnitContext tree = JavaTXParser.parse(sourceFile);
        SyntaxTreeGenerator generator = new SyntaxTreeGenerator(environment.getRegistry(sourceFile), new GenericsRegistry(null));
        SourceFile ret = generator.convert(tree, environment.packageCrawler);
        return ret;
    }

    public List<ResultSet> aspTypeInference() throws ClassNotFoundException, IOException, InterruptedException {
        Collection<ClassOrInterface> allClasses = new ArrayList<>();//environment.getAllAvailableClasses();
        //Alle Importierten Klassen in allen geparsten Sourcefiles kommen ins FC
        for(SourceFile sf : this.sourceFiles.values()) {
            allClasses.addAll(getAvailableClasses(sf));
            allClasses.addAll(sf.getClasses());
        }
        HashMap<String, ClassOrInterface> classes = new HashMap<>();
        for(ClassOrInterface cl : allClasses){
            classes.put(cl.getClassName().toString(), cl);
        }
        allClasses = classes.values();

        final ConstraintSet<Pair> cons = getConstraints();

        String content = "";
        content = ASPFactory.generateASP(cons, allClasses);
        final String tempDirectory = "/tmp/";
        PrintWriter writer = new PrintWriter(tempDirectory + "test.lp", "UTF-8");
        writer.println(content);
        writer.close();
        ASPUnify clingo = new ASPUnify(Arrays.asList(new File(tempDirectory + "test.lp")));
        String result = clingo.runClingo();
        //System.out.println(result);
        ResultSet resultSet = ASPParser.parse(result, getInvolvedTPHS(cons));
        return Arrays.asList(resultSet);
    }

    private static class TPHExtractor implements TypeVisitor<List<TypePlaceholder>> {
        @Override
        public List<TypePlaceholder> visit(RefType refType) {
            ArrayList<TypePlaceholder> ret = new ArrayList<>();
            for(RefTypeOrTPHOrWildcardOrGeneric param : refType.getParaList()){
                ret.addAll(param.acceptTV(this));
            }
            return ret;
        }

        @Override
        public List<TypePlaceholder> visit(SuperWildcardType superWildcardType) {
            return superWildcardType.getInnerType().acceptTV(this);
        }

        @Override
        public List<TypePlaceholder> visit(TypePlaceholder typePlaceholder) {
            return Arrays.asList(typePlaceholder);
        }

        @Override
        public List<TypePlaceholder> visit(ExtendsWildcardType extendsWildcardType) {
            return extendsWildcardType.getInnerType().acceptTV(this);
        }

        @Override
        public List<TypePlaceholder> visit(GenericRefType genericRefType) {
            return new ArrayList<>();
        }
    }

    protected Collection<TypePlaceholder> getInvolvedTPHS(ConstraintSet<Pair> toTest) {
        List<TypePlaceholder> ret = new ArrayList<>();
        toTest.map((Pair p)-> {
            ret.addAll(p.TA1.acceptTV(new TPHExtractor()));
            ret.addAll(p.TA2.acceptTV(new TPHExtractor()));
            return p;
        });
        return ret;
    }

}