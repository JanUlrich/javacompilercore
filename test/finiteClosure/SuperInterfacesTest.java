package finiteClosure;

import de.dhbwstuttgart.parser.SyntaxTreeGenerator.FCGenerator;
import de.dhbwstuttgart.syntaxtree.ClassOrInterface;
import de.dhbwstuttgart.syntaxtree.factory.ASTFactory;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class SuperInterfacesTest {
    @Test
    public void test() throws ClassNotFoundException {
        Collection<ClassOrInterface> classes = new ArrayList<>();
        classes.add(ASTFactory.createClass(TestClass.class));
        System.out.println(FCGenerator.toFC(classes));
    }

    @Test
    public void testGeneric() throws ClassNotFoundException {
        Collection<ClassOrInterface> classes = new ArrayList<>();
        classes.add(ASTFactory.createClass(TestClassGeneric.class));
        System.out.println(FCGenerator.toFC(classes));
    }
}

class TestClass implements Test2, Test3{

}

class TestClassGeneric<A,B> implements Test4<A>{

}

interface Test2 {

}

interface Test3{

}

interface Test4<A>{

}