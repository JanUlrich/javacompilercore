import java.lang.String;

public class Lambda2
{
   public static void main(List<String> args){
       var listOfStrings = new List<String>();
       var listOfObjects;
       listOfObjects = map(listOfStrings, (a) -> a);
}

public map(a , b){
    b.apply(a);
    return a;
}

/*
public static <I,O> List<O> map(List<I> input, Function<I,O> func) {
  List<O> output;
   output = new List<O>();
    output.add(func.apply(input.get()));
  return output;
}
*/
}

class List<A>{
    A get();
    void add(A);
}

class Function<A,B>{
    B apply(A a);
}