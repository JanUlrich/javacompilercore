class Test{
 methode(param1, param2, param3) {
    return param1.meth(param2.meth(param3));
 }
}

interface Klasse1{
    Klasse1 meth(Klasse1 p);
    Klasse1 meth(Klasse2 p);
}

interface Klasse2{
    Klasse1 meth(Klasse1 p);
    Klasse2 meth(Klasse2 p);
}