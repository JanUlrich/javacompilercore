class Lambda{

methode(){
    return ((f) -> f);
}
}
/*
interface Fun0<A>{
    A apply();
}

interface Fun1<A,B>{
    A apply(B b);
}
*/
interface Fun2<A,B,C>{
    A apply(B b, C c);
}