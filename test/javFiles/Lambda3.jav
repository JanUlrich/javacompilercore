import java.lang.String;

public class Lambda2
{
    /*
    public static <A> List<A> map(List<? extends A> input,
    Function<? super A, ? extends A> func){
      input.add(func.apply(input.get()));
    }
    */
    public map(input,func){
       input.add(func.apply(input.get()));
       return map(new List<String>(), func);
    }
}

class List<A>{
    A get();
    void add(A);
}

class Function<A,B>{
    B apply(A a);
}