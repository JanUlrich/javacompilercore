import java.lang.String;

class Generics<B> {
    //<A extends B> A mt1(A a, B b){
    B mt1(B a, B b){
        return mt1(a, a);
    }
}

class Test {
    methode(String s){
        return new Generics<String>().mt1(s,s);
    }
}
