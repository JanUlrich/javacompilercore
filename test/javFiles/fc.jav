import java.util.List;

class Test{
 methode(param1, param2, param3) {
    param2.add(param3);
    return param1.meth(param2);
 }
}

interface Klasse1{
    Klasse1 meth(List p);
    Klasse1 meth(Klasse2 p);
}

interface Klasse2{
    Klasse1 meth(Klasse1 p);
    Klasse2 meth(Klasse2 p);
}