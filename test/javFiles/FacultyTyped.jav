import java.lang.Integer;

class Faculty {

  Integer mul(Integer x, Integer y) {
  	return x;
   }

  Fun1<java.lang.Integer,java.lang.Integer> m () {
   var fact = (Integer x) -> {
        return mul(x, fact.apply(x));
   };
   return fact;
  }
}

interface Fun1<A,B>{
    B apply(A a);
}