package asp.withWildcards;

import de.dhbwstuttgart.sat.asp.Clingo;
import de.dhbwstuttgart.sat.asp.writer.ASPFactory;
import de.dhbwstuttgart.syntaxtree.ClassOrInterface;
import de.dhbwstuttgart.syntaxtree.factory.ASTFactory;
import org.junit.Test;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;

public class ProduceFC extends ASPFactory{
    public static final String tempDirectory = "/tmp/";

    @Test
    public void testInteger() throws InterruptedException, IOException, ClassNotFoundException {
        Collection<ClassOrInterface> fc = new ArrayList<>();
        fc.add(ASTFactory.createClass(Integer.class));
        System.out.println(run(fc));
    }

    public String run(Collection<ClassOrInterface> fc) throws IOException, InterruptedException, ClassNotFoundException {
        String content = "";
        this.convertFC(fc);

        content = writer.getASPFile();

        PrintWriter writer = new PrintWriter(tempDirectory + "test.lp", "UTF-8");
        writer.println(content);
        writer.close();
        Clingo clingo = new Clingo(
                Arrays.asList(new File(tempDirectory + "test.lp"),
                        new File("/home/janulrich/Sync/HiwiJob/ResearchPapers/MasterarbeitStadelmeier/asp/finiteClosure/fc.pl")));
        String result = clingo.runClingo();

        return result;
    }
}
