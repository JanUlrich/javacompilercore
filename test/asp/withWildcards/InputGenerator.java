package asp.withWildcards;

import de.dhbwstuttgart.core.JavaTXCompiler;
import de.dhbwstuttgart.sat.asp.writer.ASPFactory;
import de.dhbwstuttgart.syntaxtree.ClassOrInterface;
import de.dhbwstuttgart.syntaxtree.SourceFile;
import de.dhbwstuttgart.syntaxtree.visual.ASTPrinter;
import de.dhbwstuttgart.syntaxtree.visual.ASTTypePrinter;
import de.dhbwstuttgart.syntaxtree.visual.ResultSetPrinter;
import de.dhbwstuttgart.typedeployment.TypeInsert;
import de.dhbwstuttgart.typedeployment.TypeInsertFactory;
import de.dhbwstuttgart.typeinference.constraints.ConstraintSet;
import de.dhbwstuttgart.typeinference.constraints.Pair;
import de.dhbwstuttgart.typeinference.result.ResultSet;
import org.junit.Test;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class InputGenerator {

    public static final String rootDirectory = System.getProperty("user.dir")+"/test/javFiles/";

    @Test
    public void finiteClosure() throws IOException, ClassNotFoundException, InterruptedException {
        execute(new File(rootDirectory+"fc.jav"));
    }
    @Test
    public void lambda() throws IOException, ClassNotFoundException, InterruptedException {
        execute(new File(rootDirectory+"Lambda.jav"));
    }
    @Test
    public void lambda2() throws IOException, ClassNotFoundException, InterruptedException {
        execute(new File(rootDirectory+"Lambda2.jav"));
    }
    @Test
    public void lambda3() throws IOException, ClassNotFoundException, InterruptedException {
        execute(new File(rootDirectory+"Lambda3.jav"));
    }
    @Test
    public void mathStruc() throws IOException, ClassNotFoundException, InterruptedException {
        execute(new File(rootDirectory+"mathStruc.jav"));
    }
    @Test
    public void generics() throws IOException, ClassNotFoundException, InterruptedException {
        execute(new File(rootDirectory+"Generics.jav"));
    }
    @Test
    public void genericsMethodCall() throws IOException, ClassNotFoundException, InterruptedException {
        execute(new File(rootDirectory+"MethodCallGenerics.jav"));
        //TODO: Hier sollte der Rückgabetyp der Methode String sein
    }
    @Test
    public void faculty() throws IOException, ClassNotFoundException, InterruptedException {
        execute(new File(rootDirectory+"Faculty.jav"));
    }
    @Test
    public void facultyTyped() throws IOException, ClassNotFoundException, InterruptedException {
        execute(new File(rootDirectory+"FacultyTyped.jav"));
    }
    @Test
    public void matrix() throws IOException, ClassNotFoundException, InterruptedException {
        execute(new File(rootDirectory+"Matrix.jav"));
    }
    @Test
    public void packageTests() throws IOException, ClassNotFoundException, InterruptedException {
        execute(new File(rootDirectory+"Package.jav"));
    }
    @Test
    public void vector() throws IOException, ClassNotFoundException, InterruptedException {
        execute(new File(rootDirectory+"Vector.jav"));
    }
    @Test
    public void lambdaRunnable() throws IOException, ClassNotFoundException, InterruptedException {
        execute(new File(rootDirectory+"LambdaRunnable.jav"));
    }
    @Test
    public void expressions() throws IOException, ClassNotFoundException, InterruptedException {
        execute(new File(rootDirectory+"Expressions.jav"));
    }
    @Test
    public void addLong() throws IOException, ClassNotFoundException, InterruptedException {
        execute(new File(rootDirectory+"AddLong.jav"));
    }

    private static class TestResultSet{

    }

    public void execute(File fileToTest) throws IOException, ClassNotFoundException, InterruptedException {
        JavaTXCompiler compiler = new JavaTXCompiler(fileToTest);

        //List<ResultSet> results = compiler.aspTypeInference();
        List<ClassOrInterface> allClasses = new ArrayList<>();//environment.getAllAvailableClasses();
        //Alle Importierten Klassen in allen geparsten Sourcefiles kommen ins FC
        for(SourceFile sf : compiler.sourceFiles.values()) {
            allClasses.addAll(compiler.getAvailableClasses(sf));
            allClasses.addAll(sf.getClasses());
        }

        final ConstraintSet<Pair> cons = compiler.getConstraints();

        String content = "";
        content = ASPFactory.generateASP(cons, allClasses);
        System.out.println(cons);
        System.out.println(content);
    }

    static String readFile(String path, Charset encoding)
            throws IOException
    {
        byte[] encoded = Files.readAllBytes(Paths.get(path));
        return new String(encoded, encoding);
    }

}

