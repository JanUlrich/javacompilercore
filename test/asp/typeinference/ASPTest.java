package asp.typeinference;

import de.dhbwstuttgart.core.JavaTXCompiler;
import de.dhbwstuttgart.sat.asp.writer.ASPFactory;
import de.dhbwstuttgart.syntaxtree.ClassOrInterface;
import de.dhbwstuttgart.syntaxtree.SourceFile;
import de.dhbwstuttgart.typeinference.constraints.ConstraintSet;
import de.dhbwstuttgart.typeinference.constraints.Pair;
import org.junit.Test;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class ASPTest {

    public static final String rootDirectory = System.getProperty("user.dir")+"/test/javFiles/";
    private static final List<File> filesToTest = new ArrayList<>();
    protected File fileToTest = null;

    public ASPTest(){
    }


    @Test
    public void test() throws IOException, ClassNotFoundException {
        if(fileToTest != null)filesToTest.add(fileToTest);
        else return;
        //filesToTest.add(new File(rootDirectory+"Faculty.jav"));
        //filesToTest.add(new File(rootDirectory+"mathStruc.jav"));
        //filesToTest.add(new File(rootDirectory+"test.jav"));
        filesToTest.add(new File(rootDirectory+"EmptyMethod.jav"));
        //filesToTest.add(new File(rootDirectory+"fc.jav"));
        //filesToTest.add(new File(rootDirectory+"Lambda.jav"));
        //filesToTest.add(new File(rootDirectory+"Lambda2.jav"));
        //filesToTest.add(new File(rootDirectory+"Lambda3.jav"));
        //filesToTest.add(new File(rootDirectory+"Vector.jav"));
        //filesToTest.add(new File(rootDirectory+"Generics.jav"));
        //filesToTest.add(new File(rootDirectory+"MethodsEasy.jav"));
        //filesToTest.add(new File(rootDirectory+"Matrix.jav"));
        //filesToTest.add(new File(rootDirectory+"Import.jav"));
        JavaTXCompiler compiler = new JavaTXCompiler(fileToTest);
        Set<ClassOrInterface> allClasses = new HashSet<>();
        for(SourceFile sf : compiler.sourceFiles.values()) {
            allClasses.addAll(compiler.getAvailableClasses(sf));
        }
        for(SourceFile sf : compiler.sourceFiles.values()) {
            allClasses.addAll(sf.getClasses());
        }

        final ConstraintSet<Pair> cons = compiler.getConstraints();
        String asp = ASPFactory.generateASP(cons, allClasses);
        System.out.println(asp);
    }

    static String readFile(String path, Charset encoding)
            throws IOException
    {
        byte[] encoded = Files.readAllBytes(Paths.get(path));
        return new String(encoded, encoding);
    }

}

