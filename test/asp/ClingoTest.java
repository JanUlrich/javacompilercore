package asp;

import de.dhbwstuttgart.parser.NullToken;
import de.dhbwstuttgart.sat.asp.ASPUnify;
import de.dhbwstuttgart.sat.asp.parser.ASPParser;
import de.dhbwstuttgart.sat.asp.writer.ASPFactory;
import de.dhbwstuttgart.syntaxtree.ClassOrInterface;
import de.dhbwstuttgart.syntaxtree.factory.ASTFactory;
import de.dhbwstuttgart.syntaxtree.type.RefTypeOrTPHOrWildcardOrGeneric;
import de.dhbwstuttgart.syntaxtree.type.TypePlaceholder;
import de.dhbwstuttgart.typeinference.constraints.ConstraintSet;
import de.dhbwstuttgart.typeinference.constraints.Pair;
import de.dhbwstuttgart.typeinference.result.ResultSet;
import de.dhbwstuttgart.typeinference.unify.model.PairOperator;
import org.junit.Test;

import java.io.*;
import java.util.*;

public class ClingoTest {
    public static final String tempDirectory = "/tmp/";
    private final TypePlaceholder testType = TypePlaceholder.fresh(new NullToken());
    @Test
    public void test() throws IOException, InterruptedException, ClassNotFoundException {
        String content = "";
        content = ASPFactory.generateASP(this.getPairs(), this.getFC());
        
        PrintWriter writer = new PrintWriter(tempDirectory + "test.lp", "UTF-8");
        writer.println(content);
        writer.close();

        ASPUnify clingo = new ASPUnify(Arrays.asList(new File(tempDirectory + "test.lp")));
        String result = clingo.runClingo();
        System.out.println(result);
        ResultSet resultSet = ASPParser.parse(result, Arrays.asList(testType));
        RefTypeOrTPHOrWildcardOrGeneric resolvedType = resultSet.resolveType(testType).resolvedType;
        assert resolvedType.toString().equals(ASTFactory.createObjectType().toString());
    }

    public Collection<ClassOrInterface> getFC() {
        Set<ClassOrInterface> ret = new HashSet<>();
        return ret;
    }

    public ConstraintSet<Pair> getPairs() {
        ConstraintSet<Pair> ret = new ConstraintSet<>();
        ret.addUndConstraint(new Pair(testType, ASTFactory.createObjectType(), PairOperator.EQUALSDOT));
        return ret;
    }
}
